﻿import { Action } from '@ngrx/store';
import { Command } from '../modules/shared/models/command.model';

export const CommandActionTypes = {
    RECEIVE_COMMAND: "RECEIVE_COMMAND"
};

export class AddCommandAction implements Action {
    type = CommandActionTypes.RECEIVE_COMMAND;

    constructor(public payload: Command) { }
}

export type CommandActions = AddCommandAction;