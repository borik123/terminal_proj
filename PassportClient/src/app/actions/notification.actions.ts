﻿import { Action } from '@ngrx/store';
import { Notification } from '../modules/shared/models/notification.model';

export const NotificationActionTypes = {
    ADD_NOTIFICATION: "ADD_NOTIFICATION",
    READ_NOTIFICATION: "READ_NOTIFICATION",
    DELETE_NOTIFICATION: "DELETE_NOTIFICATION",
};

export class AddNotificationAction implements Action {
    type = NotificationActionTypes.ADD_NOTIFICATION;

    constructor(public payload: Notification) { }
}

export class ReadNotificationAction implements Action {
    type = NotificationActionTypes.READ_NOTIFICATION;
    payload: number;

    constructor(notificationId: number) {
        this.payload = notificationId;
    }
}

export class DeleteNotificationAction implements Action {
    type = NotificationActionTypes.DELETE_NOTIFICATION;
    payload: number;

    constructor(id: number) {
        this.payload = id;
    }
}

export type NotificationActions = AddNotificationAction | ReadNotificationAction | DeleteNotificationAction;