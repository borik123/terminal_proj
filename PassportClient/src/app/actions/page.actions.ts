﻿import { Action } from '@ngrx/store';

export const PageActionTypes = {
    ADD_PAGE: "ADD_PAGE",
    GO_BACK: "GO_BACK",
    GO_HOME: "GO_HOME",
    CLEAR_HISTORY: "CLEAR_HISTORY"
};

export class AddPageAction implements Action {
    type = PageActionTypes.ADD_PAGE;

    constructor(public payload: any) { }
}

export class GoBackCommandAction implements Action {
    type = PageActionTypes.GO_BACK;

    constructor() { }
}

export class GoHomeCommandAction implements Action {
    type = PageActionTypes.GO_HOME;

    constructor() { }
}

export class ClearHistoryCommandAction implements Action {
    type = PageActionTypes.CLEAR_HISTORY;

    constructor() { }
}

export type CommandActions = AddPageAction | GoBackCommandAction | GoHomeCommandAction | ClearHistoryCommandAction;