﻿import { Action } from '@ngrx/store';
import { Resident, User, CartElem, Medication } from '../modules/shared/models/passportContext.model';

export const PassportContextActionTypes = {
    LOGIN_USER: "LOGIN_USER",
    LOGOUT_USER: "LOGOUT_USER",
    SET_RESIDENT: "SET_RESIDENT",
    CLEAR_RESIDENT: "CLEAR_RESIDENT",
    ADD_TO_CART: "ADD_TO_CART",
    ADD_EXISTING_TO_CART: "ADD_EXISTING_TO_CART",
    REMOVE_FROM_CART: "REMOVE_FROM_CART",
    CLEAR_CONTEXT: "CLEAR_CONTEXT"
};

export class LoginUserAction implements Action {
    type = PassportContextActionTypes.LOGIN_USER;

    constructor(public payload: User) { }
}
export class LogoutUserAction implements Action {
    type = PassportContextActionTypes.LOGOUT_USER;

    constructor() { }
}

export class SetResidentAction implements Action {
    type = PassportContextActionTypes.SET_RESIDENT;

    constructor(public payload: Resident) { }
}
export class ClearResidentAction implements Action {
    type = PassportContextActionTypes.CLEAR_RESIDENT;

    constructor(public payload: Resident) { }
}

export class AddToCartAction implements Action {
    type = PassportContextActionTypes.ADD_TO_CART;

    constructor(public payload: Medication[]) { }
}
export class AddExistingToCartAction implements Action {
    type = PassportContextActionTypes.ADD_EXISTING_TO_CART;

    constructor(public payload: CartElem[]) { }
}
export class RemoveFromCartAction implements Action {
    type = PassportContextActionTypes.REMOVE_FROM_CART;

    constructor(public payload: any) { }
}

export class ClearContextAction implements Action {
    type = PassportContextActionTypes.CLEAR_CONTEXT;
    payload: any
}

export type PassportContextActions = LoginUserAction | LogoutUserAction | SetResidentAction | ClearResidentAction | AddToCartAction | RemoveFromCartAction | ClearContextAction | AddExistingToCartAction;