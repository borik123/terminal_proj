﻿import { Injectable, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { empty } from 'rxjs/observable/empty';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { filter, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { timer } from 'rxjs/observable/timer';

import { PassportContext } from './modules/shared/models/passportContext.model';
import { Notification } from './modules/shared/models/notification.model';
import { Command } from './modules/shared/models/command.model';
import { Event } from './modules/shared/models/event.model';
import { IMedicationMatrix } from './modules/shared/models/medicationMatrix.model';

@Injectable()
export class AppFrameSandbox implements OnDestroy {
    isEditMode = true;
    assetsFolderPath = 'assets/';
    allVmIds: { [key: string]: string } = {
        'LogonVM': 'LOGON',
        'SwitchVM': 'SWITCH',
        'HomeVM': 'HOME',
        'ServicesVM': 'SERVICES',
        'MaintenanceVM': 'MAINTENANCE',
        'PrnVM': 'PRN',
        'DispensionVM': 'DISPENSION',
        'ExitContinueVM': 'EXITCONTINUE',
        'ConfigurationVM': 'CONFIGURATION',
        'ApsVM': 'APS',
        'FacilityVM': 'FACILITY',
        'PharmacyVM': 'PHARMACY',
        'CartrigesVM': 'CARTRIGES',
        'UnauthorizedVM': 'UNAUTHORIZED'
    };
    pageSubject$ = new BehaviorSubject<any>(null);

    page$ = this.pageSubject$.asObservable();
    previousPage$ = of(<any>null);
    context$: Observable<PassportContext> = this.getContext();
    sysNotifications$: Observable<Notification[]> = of([]);
    lastCommand$: Observable<Command> = of(undefined);
    notifications$: Observable<Notification[]> = of([]);
    headerVM$ = fromEvent(window, 'message')
        .pipe(
            filter(ev => this.vmMessageFilter(ev)),
            map((ev: any) => JSON.parse(ev.data).vm),
            filter((vm: any) => vm.ElementKey === 'HeaderVM')
        );
    basketVM$ = fromEvent(window, 'message')
        .pipe(
            filter(ev => this.vmMessageFilter(ev)),
            map((ev: any) => JSON.parse(ev.data).vm),
            filter((vm: any) => vm.ElementKey === 'CartVM')
        );

    medicationMatrix$: Observable<IMedicationMatrix[]> = this.getMedMatrix();
    topMenu$: Observable<any> = empty<any>();
    adminTimes$: Observable<any> = empty<any>();
    stations$: Observable<any> = empty<any>();
    configPageAttributes$: Observable<any[]> = this.getConfigAttributes();
    doorOpened$: Observable<any> = timer(2000);
    doorClosed$: Observable<any> = timer(4000);
    selectedPatient$: Observable<any> = empty<any[]>();
    login$: Observable<any> = empty<Command>();
    gotSolt$: Observable<any> = empty<Command>();

    constructor() {
        // EditMode ONLY
        // post List Of Pages to parent Window
        window.parent.postMessage(JSON.stringify({
            type: 'pages_list',
            pages: Object.keys(this.allVmIds)
        }), '*');

        // subscribe to messages from Parent window
        // inside 'data' attr should be VM
        fromEvent(window, 'message')
            .pipe(
                filter(ev => this.vmMessageFilter(ev)),
                map((ev: any) => {
                    let _vm = JSON.parse(ev.data).vm;
                    _vm.EventId = this.allVmIds[_vm.ElementKey];
                    return _vm;
                }),
                filter((vm: any) => vm.ElementKey !== 'CartVM' && vm.ElementKey !== 'HeaderVM')
            )
            .subscribe(vm => this.pageSubject$.next(vm));
    }

    private vmMessageFilter(event): boolean {
        try {
            const msgData: any = JSON.parse(event.data);
            return msgData.type === 'view_model' && msgData.vm !== undefined;
        } catch (e) {
            return false;
        }
    }
    private getContext(): Observable<PassportContext> {
        const ctx: PassportContext = {
            currentUser: {
                UserID: '9999999999',
                FullName: 'Test User'
            },
            currentResident: {
                UId: '00000000-0000-0000-0000-000000000000',
                PID: '000000',
                FullName: 'Sample Patient',
                RoomName: '-no room-',
                Sex: 70,
                Age: 29,
                Allergies: [],
                LastDose: '',
                BirthDate: new Date(1983, 11, 11)
            },
            cart: [{
                PackageId: '00000000-0000-0000-0000-000000000000',
                Status: 0,
                Patient: {
                    PID: '0000000', Age: 44, Allergies: [], BirthDate: new Date(), FullName: 'Sample Patient',
                    LastDose: '', RoomName: '-no room-', Sex: 77, Status: '',
                    UId: '00000000-0000-0000-0000-000000000000'
                },
                AuthorId: '00000000-0000-0000-0000-000000000000',
                Items: [{
                    ItemId: '00000000-0000-0000-0000-000000000000', MedicationId: '00000000-0000-0000-0000-000000000000',
                    MedicationName: 'Sample Medication', SigId: '000000', Quantity: 1
                }]
            }]
        };
        return of(ctx);
    }
    private getConfigAttributes(): Observable<any[]> {
        return of([
            { 'Key': 'Sample label 1', 'Value': 'value #1' },
            { 'Key': 'Sample label 2', 'Value': 'value #2' },
            { 'Key': 'Sample label 3', 'Value': 'value #3' }
        ]);
    }
    private getMedMatrix(): Observable<IMedicationMatrix[]> {
        return of([
            {
                Location: '2:5',
                ContainerMedication: {
                    MedID: '00000', NDC: '00000', GenericName: 'Sample Name', BrandName: 'SAMPLE',
                    ExpirationDate: new Date(), ImageSrc: 'img_404.png'
                },
                ContainerExpirationDate: new Date(), ContainerFillDate: new Date(), ContainerQuantity: 0,
                ContainerTransNum: 0, ContainerLotNumber: 'SAMPLE01',
                CartridgeMedication: {
                    MedID: '00000', NDC: '00000', GenericName: 'Sample Name', BrandName: 'SAMPLE',
                    ExpirationDate: new Date(), ImageSrc: 'img_404.png'
                },
                CartridgeExpirationDate: new Date(), CartridgeFillDate: new Date(), CartridgeQuantity: 0,
                CartridgeTransNum: 0, CartridgeLotNumber: 'SAMPLE01', Legend: 1
            }
        ]);
    }

    public sendEvent(eventId: string, data: any = null): void {
        let vmKey = '';
        if (eventId === 'BASKET_VM') {
            vmKey = 'CartVM';
        } else if (eventId === 'HEADER_VM') {
            vmKey = 'HeaderVM';
        }

        if (vmKey !== '') {
            window.parent.postMessage(JSON.stringify({
                type: 'give_me_vm',
                page: vmKey
            }), '*');
        }
    }
    public sendEventToServer(ev: Event): void { }
    public sendAsyncEvent(ev: Event): Observable<any[]> {
        if (ev.commandName === 'SEARCH_MEDICATIONS') {
            return of([{
                MedicationId: -1, MedicationName: 'EXAMPLE Medication', SigDescription: 'Q6H',
                Type: 1, RxNumber: 0, MedID: '00', SigTimeCount: 1
            }]);
        } else {
            return empty<any[]>();
        }
    }

    public newCommandReceived(args: IArguments): void { }

    public addPageToHistory(page: any): void { }

    public addToBasket(selection: any[]): void { }
    public removeFromBasket(obj: any): void { }

    public setUser(user: any): void { }
    public setPatientDetails(patient: any): void { }
    public fillBasket(packages: any): void { }
    public clearPassportContext(): void { }

    public addNotification(n: Notification): void { }
    public deleteNotification(notificationId: number): void { }

    ngOnDestroy() {
        this.pageSubject$.complete();
    }
}
