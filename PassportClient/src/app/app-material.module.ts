import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';

@NgModule({
  imports: [
      CommonModule,
      BsDropdownModule.forRoot(),
      ModalModule.forRoot(),
      PopoverModule.forRoot()
    ],
  exports: [BsDropdownModule, ModalModule, PopoverModule ]
})
export class MaterialModule { }
