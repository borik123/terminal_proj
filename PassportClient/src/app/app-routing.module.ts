import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { FullVersionGuard } from './full-version-guard.service';

import { SimpleLayoutComponent } from './modules/shared/layouts/simple.component';
import { MasterLayoutComponent } from './modules/shared/layouts/master.component';

import { IndexComponent } from './modules/pages/pages/index/index.component';
import { IntroComponent } from './modules/pages/pages/intro/intro.component';
import { LogonComponent } from './modules/pages/pages/logon/logon.component';
import { ChangePasswordComponent } from './modules/pages/pages/change-pass/change-pass.component';

import { SearchPageComponent } from './modules/pages/pages/search/search.component';
import { PrnPageComponent } from './modules/pages/pages/prn-page/prn-page.component';
import { PrnDispenseComponent } from './modules/pages/pages/prn-dispense/prn-dispense.component';
import { MaintenanceComponent } from './modules/pages/pages/maintenance/maintenance.component';
import { LoadEnvelopesComponent } from './modules/pages/pages/load-envelopes/load-envelopes.component';
import { MechanicalToolsComponent } from './modules/pages/pages/mechanical-tools/mechanical-tools.component';
import { PrinterRibbonComponent } from './modules/pages/pages/printer-ribbon/printer-ribbon.component';
import { ConfPageComponent } from './modules/pages/pages/conf-page/conf-page.component';
import { FolioComponent } from './modules/pages/pages/folio/folio.component';
import { ProfileComponent } from './modules/pages/pages/profile/profile.component';
import { ResidentScheduleComponent } from './modules/pages/pages/resident-schedule/resident-schedule.component';
import { ButtonsListComponent } from './modules/shared/pages/buttons-list/buttons-list.component';
import { AccessDeniedComponent } from './modules/shared/pages/access-denied/access-denied.component';

/* FULL Version PAGES */
import { LoadCartrigesComponent } from './modules/pages-full/pages/load-cartriges/load-cartriges.component';
import { LoaComponent } from './modules/pages-full/pages/loa/loa.component';
import { LoaPrnComponent } from './modules/pages-full/pages/loa-prn/loa-prn.component';
import { LoaReturnComponent } from './modules/pages-full/pages/loa-return/loa-return.component';
import { AdmissionComponent } from './modules/pages-full/pages/admission/admission.component';
import { ResidentSwitchComponent } from './modules/pages-full/pages/resident-switch/resident-switch.component';
import { ReturnMedsComponent } from './modules/pages-full/pages/return-meds/return-meds.component';
import { StatComponent } from './modules/pages-full/pages/stat/stat.component';
import { MedToteComponent } from './modules/pages-full/pages/med-tote/med-tote.component';
import { VaultRefrigeratorComponent} from './modules/pages-full/pages/vault-refrigerator/vault-refrigerator.component';
import { DrawersLoadComponent } from './modules/pages-full/pages/drawers-load/drawers-load.component';
import { DrawersInventoryComponent } from './modules/pages-full/pages/drawers-inventory/drawers-inventory.component';

const SIMPLE_ROUTES: Route[] = [
    { path: '', pathMatch: 'full', redirectTo: '/index' },
    { path: 'index', component: IndexComponent },
    { path: 'intro', component: IntroComponent },
    { path: 'logon', component: LogonComponent },
    { path: 'change_password', component: ChangePasswordComponent },
    { path: 'unauthorized', component: AccessDeniedComponent }
    // { path: 'tmp', component: TmpComponent, canActivate: [FullVersionGuard] }
];

const MASTER_ROUTES: Route[] = [
    { path: 'home', component: SearchPageComponent, data: { 'breadcrumb': 'PRN' } },
    { path: 'resident_switch', component: ResidentSwitchComponent, data: { 'breadcrumb': 'Resident' } },
    { path: 'loa', component: LoaComponent, data: { 'breadcrumb': 'LOA' } },
    { path: 'loa_prn', component: LoaPrnComponent, data: { 'breadcrumb': 'LOA PRN' } },
    { path: 'loa_return', component: LoaReturnComponent, data: { 'breadcrumb': 'Early return from LOA' } },
    { path: 'prn', component: PrnPageComponent, data: { 'breadcrumb': 'PRN' } },
    { path: 'folio', component: FolioComponent, data: { 'breadcrumb': 'Folio' } },
    { path: 'schedule', component: ResidentScheduleComponent, data: { 'breadcrumb': 'Schedule' } },
    { path: 'profile', component: ProfileComponent, data: { 'breadcrumb': 'Profile' } },
    { path: 'dispension', component: PrnDispenseComponent, data: { 'breadcrumb': 'Dispension' } },
    { path: 'switch', component: ButtonsListComponent, data: { 'breadcrumb': 'Switch' } },
    { path: 'exitcontinue', component: ButtonsListComponent, data: { 'breadcrumb': 'Continue' } },
    { path: 'services', component: ButtonsListComponent, data: { 'breadcrumb': 'MAINtenance' } },
    { path: 'maintenance', component: MaintenanceComponent, data: { 'breadcrumb': 'MAINtenance' } },
    { path: 'cartriges', component: LoadCartrigesComponent, data: { 'breadcrumb': 'Load Cartriges' } },
    { path: 'envelopes', component: LoadEnvelopesComponent, data: { 'breadcrumb': 'Load Envelopes' } },
    { path: 'mechanicaltools', component: MechanicalToolsComponent, data: { 'breadcrumb': 'Mechanical Tools' } },
    { path: 'printer', component: PrinterRibbonComponent, data: { 'breadcrumb': 'Printer Ribbon' } },
    { path: 'configuration', component: ButtonsListComponent, data: { 'breadcrumb': 'Config' } },
    { path: 'aps', component: ConfPageComponent, data: { 'breadcrumb': 'Passport' } },
    { path: 'facility', component: ConfPageComponent, data: { 'breadcrumb': 'Facility' } },
    { path: 'pharmacy', component: ConfPageComponent, data: { 'breadcrumb': 'Pharmacy' } },
    { path: 'admission', component: AdmissionComponent, data: { 'breadcrumb': 'Admission' } },
    { path: 'return_meds', component: ReturnMedsComponent, data: { 'breadcrumb': 'Return Meds' } },
    { path: 'stat', component: StatComponent, data: { 'breadcrumb': 'Stat' } },
    { path: 'med_tote', component: MedToteComponent, data: { 'breadcrumb': 'MedTote' } },
    { path: 'vault_refrigerator', component: VaultRefrigeratorComponent, data: { 'breadcrumb': 'VaultRefrigerator' } },
    { path: 'drawers_load', component: DrawersLoadComponent, data: { 'breadcrumb': 'Load' } },
    { path: 'drawers_inventory', component: DrawersInventoryComponent, data: { 'breadcrumb': 'Inventory' } }
];

const routes: Route[] = [
    { path: '', redirectTo: '/index', pathMatch: 'full' },
    { path: '', component: SimpleLayoutComponent, children: SIMPLE_ROUTES },
    { path: '', component: MasterLayoutComponent, children: MASTER_ROUTES },
    { path: '**', redirectTo: '/index' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
