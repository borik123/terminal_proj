import { Component, OnInit, NgZone } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { tap, filter } from 'rxjs/operators';

import { AppSandbox } from './app.sandbox';

import { Command } from './modules/shared/models/command.model';
import { MessageType } from './modules/shared/models/bound.model';
import { Notification, NotificationPriority } from './modules/shared/models/notification.model';

@Component({
  selector: 'app-root',
  template: `
        <router-outlet></router-outlet>
        <app-system-notifications
                            [@slideNotificationsInOut]="sysNotificationsState"
                            [notifications]="sysNotifications$ | async"
                            (show)="showSystemNotification()"
                            (hide)="hideSystemNotification()">
        </app-system-notifications>
    `,
  host: { 'class': 'h-100 d-block' },
  animations: [
      trigger('slideNotificationsInOut', [
          state('in', style({
              transform: 'translateX(0)'
          })),
          state('out', style({
              transform: 'translateX(-450px)'
          })),
          transition('in => out', animate('500ms ease-in-out')),
          transition('out => in', animate('500ms ease-in-out'))
      ])
  ]
})
export class AppComponent implements OnInit {
    sysNotificationsState = 'out';
    sysNotifications$ = this.sb.sysNotifications$;

    constructor(private sb: AppSandbox, private _ngZone: NgZone, private router: Router, private route: ActivatedRoute) {
        this.sb.lastCommand$
            .pipe(
                filter((command: Command) => command !== null && command !== undefined),
                tap((command: Command) => this.newCommandHandler(command))
            ).subscribe();

        this.sb.page$.pipe(
                filter(vm => vm !== null && vm !== undefined),
                tap(vm => this.newPageHandler(vm))
            ).subscribe();
    }

    showSystemNotification() {
        this.sysNotificationsState = 'in';
    }
    hideSystemNotification() {
        this.sysNotificationsState = 'out';
    }

    private newCommandHandler(command: Command): void {
        if (command.Type === MessageType.ViewModel) {
            if (command.Id !== 'BASKET_VM' && command.Id !== 'HEADER_VM') {
                let _page: any = JSON.parse(command.Message);
                if (_page.EventId === undefined) {
                    _page.EventId = command.Id;
                }
                this.sb.addPageToHistory(_page);
            }
        } else if (command.Type === MessageType.Default && (command.Id === 'NOTIFICATION' || command.Id === 'SYSTEM_NOTIFICATION')) {
            const n: Notification = {
                Id: new Date().getTime(),
                Date: new Date(), Title: '',
                Text: command.Message, IsSystem: command.Id === 'SYSTEM_NOTIFICATION',
                AutoClosed: true, Priority: NotificationPriority.NORMAL
            };
            this.sb.addNotification(n);
        } else if (command.Type === MessageType.Default && command.Id === 'SYSTEM_NOTIFICATION_OBJ') {
            const msg: any = JSON.parse(command.Message);
            const n: Notification = {
                Id: new Date().getTime(), IsSystem: true,
                Date: new Date(), Title: msg.Title,
                Text: msg.Text,
                AutoClosed: msg.AutoClosed, Priority: msg.Level
            };
            this.sb.addNotification(n);
        } else if (command.Type === MessageType.Default && command.Id === 'GET_DISPENSES_BY_USER') {
            this.sb.fillBasket(JSON.parse(command.Message).Packages);
        }
    }
    private newPageHandler(vm: any): any {
        const routeName: string = vm.EventId.toLowerCase();
        const newPath = `/${routeName}`;
        const _title = vm.Title && vm.Title.Value ? vm.Title.Value : '';
        // update router Title
        this.updateRouteTitle(this.router.config, routeName, _title);
        // go to NEW page
        this.router.navigate([newPath]); // , { relativeTo: this.route, skipLocationChange: true });
    }
    private updateRouteTitle(config: Route[], routePath: string, routeTitle: string) {
        if (routeTitle === '') {
            return;
        }

        for (let i = 0; i < config.length; i++) {
            if (config[i].path === routePath && config[i].data) {
                config[i].data['breadcrumb'] = routeTitle;
                break;
            } else if (config[i].children) {
                this.updateRouteTitle(config[i].children, routePath, routeTitle);
            }
        }
    }

    receiveCommand(): void {
        const funcArgs: IArguments = arguments;
        this._ngZone.run(() => this.sb.newCommandReceived(funcArgs));
    }

    ngOnInit() {
        // init global function to call from WPF like window.receiveCommandCallback()
        (<any>window).receiveCommandCallback = this.receiveCommand.bind(this);
    }
}
