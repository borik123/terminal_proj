import {InjectionToken} from '@angular/core';

export interface AppConfig {
    apiEndpoint: string;
    fullVersion: boolean;
}

export const PASSPORT_CLIENT_CONFIG: AppConfig = {
    apiEndpoint: 'http://localhost:8090',
    fullVersion: true
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app_config');
