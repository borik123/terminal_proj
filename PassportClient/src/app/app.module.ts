import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';

import {CoreModule} from './modules/core/core.module';
import {SharedModule} from './modules/shared/shared.module';
import {AppRoutingModule} from './app-routing.module';
import {MaterialModule} from './app-material.module';

import {PagesModule} from './modules/pages/pages.module';
import {PagesFullModule} from './modules/pages-full/pages-full.module';

import {AppComponent} from './app.component';

import {FullVersionGuard} from './full-version-guard.service';

import {APP_CONFIG, PASSPORT_CLIENT_CONFIG} from './app.config';

@NgModule({
  declarations: [
      AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule.forRoot(),
    SharedModule,
    AppRoutingModule,
    PagesModule,
    PagesFullModule,
    MaterialModule
  ],
  providers: [
      FullVersionGuard,
      { provide: APP_CONFIG, useValue: PASSPORT_CLIENT_CONFIG }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
