import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { map, filter, first } from 'rxjs/operators';

import { PassportContext } from './modules/shared/models/passportContext.model';
import { Notification } from './modules/shared/models/notification.model';
import { Command } from './modules/shared/models/command.model';
import { Event } from './modules/shared/models/event.model';
import { SearchResult } from './modules/shared/models/search.model';
import { IMedicationMatrix } from './modules/shared/models/medicationMatrix.model';
import { IBound, MessageType } from './modules/shared/models/bound.model';
import { MockBound } from './mock-items';

import * as pageReducer from './reducers/page.reducer';
import * as contextReducer from './reducers/passportContext.reducer';
import * as notificationReducer from './reducers/notification.reducer';

import * as commandActions from './actions/command.actions';
import * as pageActions from './actions/page.actions';
import * as notifyActions from './actions/notification.actions';
import * as contextActions from './actions/passportContext.actions';

@Injectable()
export class AppSandbox {
    isEditMode = false;
    assetsFolderPath = 'assets/';

    page$ = this.store.select(pageReducer.getLastPage);
    previousPage$ = this.store.select(pageReducer.getPreviousPage);
    context$: Observable<PassportContext> = this.store.select(contextReducer.getPassportContext);
    sysNotifications$: Observable<Notification[]> = this.store.select(notificationReducer.getSystemNotifications);
    lastCommand$: Observable<Command> = this.store.select(pageReducer.getLastCommand);
    notifications$: Observable<Notification[]> = this.store.select(notificationReducer.getNotifications);
    // vaultRecords$: Observable<any> = this.sendAsyncEvent({ commandName: 'GET_VAULT_REFRIGERATOR_RECORDS' });

  vaultRecords$: Observable<any> = this.lastCommand$
    .pipe(
      filter((cmd: Command) => cmd !== undefined && cmd.Id === 'GET_VAULT_REFRIGERATOR_RECORDS' && cmd.Type === MessageType.Default),
      map((cmd: Command) => (JSON.parse(cmd.Message) as SearchResult).Records)
    );
  inventoryRecords$: Observable<any> = this.lastCommand$
    .pipe(
      filter((cmd: Command) => cmd !== undefined && cmd.Id === 'GET_DRAWERS_INVENTORY_RECORDS' && cmd.Type === MessageType.Default),
      map((cmd: Command) => (JSON.parse(cmd.Message) as SearchResult).Records)
    );

    medicationMatrix$: Observable<IMedicationMatrix[]> = this.lastCommand$
        .pipe(
            filter((cmd: Command) => cmd !== undefined && cmd.Id === 'GET_MEDICATION_MATRIX' && cmd.Type === MessageType.Default),
            map((cmd: Command) => (JSON.parse(cmd.Message) as SearchResult).Records)
        );
    topMenu$: Observable<any> = this.sendAsyncEvent({ commandName: 'GET_TOP_MENU' });
    adminTimes$: Observable<any> = this.sendAsyncEvent({ commandName: 'GET_ADMIN_TIMES' });
    stations$: Observable<any> = this.sendAsyncEvent({ commandName: 'GET_STATIONS' });
    configPageAttributes$: Observable<any[]> = this.lastCommand$
        .pipe(
            first((cmd: Command) => cmd !== undefined && cmd.Type === MessageType.Default &&
                (cmd.Id === 'GET_APS_INFO' || cmd.Id === 'GET_FACILITY_INFO' || cmd.Id === 'GET_PHARMACY_INFO')),
            map((cmd: Command) => (JSON.parse(cmd.Message) as SearchResult).Records)
        );
    doorOpened$: Observable<any> = this.lastCommand$
        .pipe(
            first((cmd: Command) => cmd !== undefined && cmd.Type === MessageType.Default && cmd.Id === 'CONTROLLER_DOOR_OPENED')
        );
    doorClosed$: Observable<any> = this.lastCommand$
        .pipe(
            first((cmd: Command) => cmd !== undefined && cmd.Type === MessageType.Default && cmd.Id === 'CONTROLLER_DOOR_CLOSED')
        );
    selectedPatient$: Observable<any> = this.lastCommand$
        .pipe(
            filter((cmd: Command) => cmd !== undefined && cmd.Type === MessageType.Default && cmd.Id === 'GET_FULL_PATIENT'),
            map((cmd: Command) => JSON.parse(cmd.Message, function(k, v) {
                if (typeof v === 'string' && v !== '' &&
                    (k === 'BirthDate' || k === 'Discharged' || k === 'LoaStart' || k === 'LoaEnd' || k === 'AdmitDate')) {
                    return new Date(v);
                }
                return v;
            }))
        );
    login$: Observable<Command> = this.lastCommand$
        .pipe(
            filter((cmd: Command) => cmd !== undefined && cmd.Type === MessageType.Default && cmd.Id === 'LOGIN')
        );
    gotSolt$: Observable<Command> = this.lastCommand$
        .pipe(
            filter((cmd: Command) => cmd !== undefined && cmd.Type === MessageType.Default && cmd.Id === 'GOT_SALT')
        );
    headerVM$: Observable<any> = this.store
        .pipe(
            pageReducer.getLastHeaderVmCommand,
            map((cmd: Command) => JSON.parse(cmd.Message))
        );
    basketVM$: Observable<any> = this.store
        .pipe(
            pageReducer.getLastBasketVmCommand,
            map((cmd: Command) => JSON.parse(cmd.Message))
        );

    constructor(private store: Store<any>) { }

    public sendEvent(eventId: string, data: any = null): void {
        const ev: Event = { commandName: eventId, type: '', ID: null, data: (data ? JSON.stringify(data) : null) };
        this.sendEventToServer(ev);
    }
    public sendEventToServer(ev: Event): void {
        if ((<any>window).bound === undefined) {
            // DEBUG MODE
            (<any>window).bound = new MockBound();
        }
        ((<any>window).bound as IBound).sendEvent(ev);
    }
    public sendAsyncEvent(ev: Event): Observable<any[]> {

        this.sendEventToServer(ev);

        return this.store
            .pipe(
                pageReducer.getLastSearchCommand,
                filter((command: Command) => command.Id === ev.commandName),
                map((command: Command) => (JSON.parse(command.Message) as SearchResult).Records)
            );
    }

    public newCommandReceived(args: IArguments): void {
        if (args.length < 4) {
            throw new Error('Function "newCommandReceived" receive less than 4 arguments');
        }

        const lastcmd: Command = {
            Id: args[0],
            Message: JSON.stringify(args[1]),
            Details: args[2],
            Type: args[3]
        };

        this.store.dispatch(new commandActions.AddCommandAction(lastcmd));
    }

    public addPageToHistory(page: any): void {
        this.store.dispatch(new pageActions.AddPageAction(page));
    }

    public addToBasket(selection: any[]): void {
        this.store.dispatch(new contextActions.AddToCartAction(selection));
    }
    public removeFromBasket(obj: any): void {
        this.store.dispatch(new contextActions.RemoveFromCartAction(obj));
    }

    public setUser(user: any): void {
        this.store.dispatch(new contextActions.LoginUserAction(user));
    }
    public setPatientDetails(patient: any): void {
        this.store.dispatch(new contextActions.SetResidentAction(patient));
    }
    public fillBasket(packages: any): void {
        this.store.dispatch(new contextActions.AddExistingToCartAction(packages));
    }
    public clearPassportContext(): void {
        this.store.dispatch(new contextActions.ClearContextAction());
    }

    public addNotification(n: Notification): void {
        this.store.dispatch(new notifyActions.AddNotificationAction(n));
    }
    public deleteNotification(notificationId: number): void {
        this.store.dispatch(new notifyActions.DeleteNotificationAction(notificationId));
    }
}
