import { Injectable, isDevMode, Inject } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AppConfig, APP_CONFIG } from './app.config';

@Injectable()
export class FullVersionGuard implements CanActivate {

    constructor( @Inject(APP_CONFIG) private appConfig: AppConfig) { }

    canActivate() {

        return this.appConfig.fullVersion;
    }
}
