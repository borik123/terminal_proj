import { IBound, MessageType } from './modules/shared/models/bound.model';
import { IResidentDetails } from './modules/shared/VMs/IResidentDetails.model';
import { CartElem } from './modules/shared/models/passportContext.model';
import { SearchRequest, SearchResult, AuthenticationResult } from './modules/shared/models/search.model';

import { timer } from 'rxjs/observable/timer';

const USER_DETAILS: IResidentDetails = {
    Icon: { Value: 'ICONCONTACT.png', Key: '' },
    Discharged: { Value: 'DISCHARGED', Key: '' },
    LoaStart: { Value: 'LOA START', Key: '' },
    LoaEnd: { Value: 'LOA END', Key: '' },
    ResidentId: { Value: 'Resident ID', Key: '' },
    Room: { Value: 'Room', Key: '' },
    Sex: { Value: 'Sex', Key: '' },
    Age: { Value: 'DOB', Key: '' },
    Allergies: { Value: 'Allergies', Key: '' },
    LastDose: { Value: 'Last dose given', Key: '' },
    LastName: { Value: 'Last Name', Key: '' },
    FirstName: { Value: 'First Name', Key: '' },
    MedicalRecNo: { Value: 'Medical Rec. No', Key: '' },
    AdmitDate: { Value: 'Admit Date', Key: '' },
    Station: { Value: 'Nursuring St.', Key: '' },
    Bed: { Value: 'Bed', Key: '' },
    Male: { Value: 'Male', Key: '' },
    Female: { Value: 'Female', Key: '' },
    ToggleDetails: {
        Text: { Value: 'More Details', Key: '' }, UncheckedText: { Value: 'Less Details', Key: '' }, Action: ''
    },
    Profile: {
        Action: 'PROFILE', Text: { Value: 'Profile', Key: '' }
    },
    Schedule: {
        Action: 'SCHEDULE', Text: { Value: 'Schedule', Key: '' }
    },
    SearchField: {
        Text: { Value: '', Key: '' },
        Placeholder: { Value: 'Search', Key: '' },
        Image: { Value: 'SCREEN4_PRN_SEARCH_ICON.png', Key: '' }
    }
};
const LOA_PICKER = {
    Icon: { Value: 'ResidentsLOA_clock.png' },
    EditIcon: { Value: 'ResidentsLOA_edit.png' },
    Select: { Value: 'Select'},
    Change: { Value: 'CHANGE'},
    Date: { Value: 'Date'},
    Or: { Value: 'OR'},
    TimeStamp: { Value: 'By Time Stamp:'},
    MedPass: { Value: 'By Med Pass:'},
    Arrow: { Value: 'arrow133.png' },
    Ok: {
        Action: 'UPSERT_LOA_DATA',
        Text: { Value: 'OK' },
        Image: { Value: 'B_BKG_1.png' }
    }
};
const MEDICATION_LIST = {
    Icon: { Value: 'SCREEN4_PRN_LIST_ICON.png' },
    PrnTabTitle: { Value: 'PRN' },
    RoutineMedsTabTitle: { Value: 'Routine Meds' },
    UnselectPrn: { Value: 'Unselect PRN' },
    UnselectRoutine: { Value: 'Unselect Routine' },
    SearchField: {
        Placeholder: { Value: 'Search Medication' },
        Image: { Value: 'SCREEN4_PRN_SEARCH_ICON.png' }
    },
    Legend: [
        {Text: { Value: 'Scheduled during requested Med Run' },
         Image: { Value: 'LEGEND_Scheduled_Med_Run.png' }},
        {Text: { Value: 'Not scheduled during requested Med Run' },
         Image: { Value: 'LEGEND_Not_scheduled_d_Med_Run.png' }},
        {Text: { Value: 'Resident on Leave of Absence' },
         Image: { Value: 'LEGEND_Resident_on_Leave_of_Absence.png' }},
        {Text: { Value: 'Med is not in APS' },
         Image: { Value: 'LEGEND_Med_is_not_currently_found_in_APS.png' }},
        {Text: { Value: 'Med Tagged NOT to be crushed' },
         Image: { Value: 'LEGEND_Med_has_been_Tagged_to_crush.png' }},
        {Text: { Value: 'Med in Right Bulk Drawer' },
         Image: { Value: 'LEGEND_Med_has_been_found_in_the_Right_Bulk_Drawer.png' }},
        {Text: { Value: 'Med in Left Bulk Drawer' },
         Image: { Value: 'LEGEND_Med_has_been_found_in_the_Left_Bulk_Drawer.png' }},
        {Text: { Value: 'Med in Lower Vault' },
         Image: { Value: 'LEGEND_Med_has_been_found_in_the_Bulk_Drawer.png' }},
        {Text: { Value: 'Med in Upper Vault' },
         Image: { Value: 'LEGEND_Med_has_been_found_in_the_Bulk_Drawer_copy.png' }},
        {Text: { Value: 'Med in Refrigerator - Right Drawer' },
         Image: { Value: 'LEGEND_Med_found_in_the_Right_Bulk_Drawer_Refrigerator.png' }},
        {Text: { Value: 'Med in Refrigerator - Left Drawer' },
         Image: { Value: 'LEGEND_Med_found_in_the_Left_Bulk_Drawer_Refrigerator.png' }},
        {Text: { Value: 'Med in Refrigerator' },
         Image: { Value: 'LEGEND_Med_found_in_the_Bulk_Drawer_Refrigerator.png' }},
        {Text: { Value: 'Inventory not available' },
         Image: { Value: 'LEGEND_Inventory_not_available_for_APS_Med.png' }},
        {Text: { Value: 'APS Med on stop package' },
         Image: { Value: 'LEGEND_APS_med_is_not_to_be_packaged_for_this_order.png' }}
    ],
    Select: {
        Image: { Value: 'SCREEN4_PRN_V_ICON_HIGHLIGHT.png' }, UncheckedImage: { Value: 'SCREEN4_PRN_V_ICON.png' }
    },
    PrnButton: {
        Text: { Value: 'PRN' }, Image: { Value: 'ResidentsLOA_prnButton.png' }
    },
    ClearSelected: {
        Text: { Value: 'Clear Selected' }, Image: { Value: 'SCREEN4_PRN_X_ICON.png' }
    },
    ColumnNamesAsArray: [
        '', 'PIC', 'QTY', 'MED', 'SIG', '::{SCREEN4_PRN_INFO_ICON.png}::', 'MORE'
    ],
    ColumnNames: ';#PIC;#QTY;#MED;#SIG;#::{SCREEN4_PRN_INFO_ICON.png}::;#MORE'
};
const MEDICATIONS = [
    { MedicationId: 1, MedicationName: '1 dose of Codeine TAB 150MG', SigDescription: 'ACB', Type: 0, RxNumber: 143223, MedID: '040404', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', SigDescription: 'Q6H', Type: 1, RxNumber: 153324, State: 'AboutToExpire', MedID: '010101', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', SigDescription: 'CM', Type: 0, RxNumber: 123456, MedID: '010202', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 4, ImageSrc: 'med4.png', MedicationName: '1 dose of Prilosec TAB 40MG', SigDescription: 'BID', Type: 1, RxNumber: 234111, State: 'FutureOrder', MedID: '010101', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 5, ImageSrc: 'med5.png', MedicationName: '3 dose of Lexarpo TAB 50MG', SigDescription: 'QD', Type: 0, RxNumber: 121212, SigTimeCount: 1, MedID: '030301', Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 6, ImageSrc: 'med1.png', MedicationName: '2 dose of Lexarpo TAB 60MG', SigDescription: 'QD', Type: 1, RxNumber: 121212, State: 'AllDispensed', MedID: '040401', SigTimeCount: 0, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 7, ImageSrc: 'med2.png', MedicationName: '1 dose of Acetamin TAB 70MG', SigDescription: 'ACB', Type: 2, RxNumber: 121212, MedID: '050501', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 8, ImageSrc: 'med3.png', MedicationName: '3 dose of Prilosec TAB 80MG', SigDescription: 'QD', Type: 0, RxNumber: 121212, MedID: '070101', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 9, ImageSrc: 'med4.png', MedicationName: '1 dose of Codeine TAB 90MG', SigDescription: 'BID', Type: 1, RxNumber: 121212, MedID: '060101', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 10, ImageSrc: 'med1.png', MedicationName: '2 dose of Codeine TAB 10MG', SigDescription: 'BID', Type: 2, RxNumber: 121212, MedID: '080101', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 11, ImageSrc: 'med2.png', MedicationName: '3 dose of Codeine TAB 11MG', SigDescription: 'BID', Type: 1, RxNumber: 121212, MedID: '090101', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 12, ImageSrc: 'med3.png', MedicationName: '1 dose of Codeine TAB 12MG', SigDescription: 'BID', Type: 0, RxNumber: 121212, MedID: '010708', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 13, ImageSrc: 'med4.png', MedicationName: '3 dose of Codeine TAB 13MG', SigDescription: 'BID', Type: 2, RxNumber: 121212, MedID: '010807', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 14, ImageSrc: 'med1.png', MedicationName: '2 dose of Codeine TAB 14MG', SigDescription: 'BID', Type: 1, RxNumber: 121212, MedID: '010906', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 15, ImageSrc: 'med3.png', MedicationName: '3 dose of Codeine TAB 15MG', SigDescription: 'BID', Type: 0, RxNumber: 121212, MedID: '010205', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 16, ImageSrc: 'med2.png', MedicationName: '1 dose of Codeine TAB 16MG', SigDescription: 'BID', Type: 0, RxNumber: 121212, MedID: '010302', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 },
    { MedicationId: 17, ImageSrc: 'med1.png', MedicationName: '2 dose of Codeine TAB 17MG', SigDescription: 'BID', Type: 1, RxNumber: 121212, MedID: '010403', SigTimeCount: 1, Quantity: 1, RoNumber: 12345, TQW: 1, TQD: 1 }
];

const VAULT_REFRIGERATOR_RECORDS = [
  { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', Quantity: 1,
    RecordType: 'OPEN_UPPER_VAULT'},
  { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', Quantity: 2,
    RecordType: 'OPEN_UPPER_VAULT'},
  { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG',  Quantity: 3,
    RecordType: 'OPEN_UPPER_VAULT'},
  { MedicationId: 4, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', Quantity: 7,
    RecordType: 'OPEN_LOWER_VAULT'},
  { MedicationId: 5, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 60MG', Quantity: 9,
    RecordType: 'OPEN_LOWER_VAULT'},
  { MedicationId: 6, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG',  Quantity: 4,
    RecordType: 'OPEN_LOWER_VAULT'},
  { MedicationId: 7, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', Quantity: 2,
    RecordType: 'OPEN_REFRIGERATOR'},
  { MedicationId: 8, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 40MG', Quantity: 3,
    RecordType: 'OPEN_REFRIGERATOR'},
  { MedicationId: 9, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG',  Quantity: 1,
    RecordType: 'OPEN_REFRIGERATOR'}
];

const DRAWERS_INVENTORY_RECORDS = [
  {
    InventoryType: 'Upper Vault',
    InventoryItems: [
      { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3},
      { MedicationId: 4, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3},
      { MedicationId: 4, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3},
      { MedicationId: 4, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3},
      { MedicationId: 4, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3},
      { MedicationId: 4, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
    ]
  },
  {
    InventoryType: 'Lower Vault',
    InventoryItems: [
      { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3}
    ]
  },
  {
    InventoryType: 'Refrigerator',
    InventoryItems: [
      { MedicationId: 1, ImageSrc: 'med1.png',  MedicationName: '1 dose of Codeine TAB 150MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', ScanQuantity: 1,
        Form: '1 bottle', Count: 3, Discrepancy: 1},
      { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', ScanQuantity: 4,
        Form: '1 bottle', Count: 1, Discrepancy: 3}
    ]
  },
]

const VIEW_MODEL_COMMANDS = [

    {
        EventId: 'INTRO',
        Title: {
            Value: 'TOUCH SCREEN TO <h3>START</h3>'
        },
        Language: {
            LanguageID: 1,
            Key: 'English',
            Direction: 0
        }
    },
    {
        EventId: 'LOGON',
        Logo: {
            Value: 'PRN_03.png'
        },
        Background: {
            Value: 'BKG.jpg'
        },
        ErrorIcon: {
            Value: 'login_error_icon.png'
        },
        Login: {
            Action: 'LOGIN', Text: { Value: 'Login' }, Image: { Value: 'button.png' }
        },
        UserNameField: {
            Placeholder: { Value: 'Username' }, ValidationPattern: { Value: '' }, ErrorText: { Value: 'Incorrect Username / Password' },
            Image: { Value: 'logon-user-icon-rev.png' }
        },
        PasswordField: {
            Placeholder: { Value: 'Password' }, ValidationPattern: { Value: '' }, ErrorText: { Value: 'Incorrect Username / Password' },
            Image: { Value: 'logon-password-icon-rev.png' }
        },
        FingerprintField: {
            ErrorText: { Value: 'Fingerprint Not Detected - Try Again' }, Image: { Value: 'PRN_146.png' }
        }
    },
    {
        EventId: 'CHANGE_PASSWORD',
        Title: { Value: 'Change Password'},
        Logo: {
            Value: 'PRN_03.png'
        },
        Background: {
            Value: 'BKG.jpg'
        },
        ErrorIcon: {
            Value: 'login_error_icon.png'
        },
        Reset: {
            Action: 'RESET_PASSWORD', Text: { Value: 'Reset' }, Image: { Value: 'button.png' }
        },
        OldPasswordField: {
            Placeholder: { Value: 'Old Password' }, ValidationPattern: { Value: '' },
            ErrorText: { Value: 'Old Password is incorrect' },
            Image: { Value: 'logon-password-icon-rev.png' }
        },
        NewPasswordField: {
            Placeholder: { Value: 'New Password' }, ValidationPattern: { Value: '' },
            ErrorText: { Value: 'New Password is incorrect' },
            Image: { Value: 'logon-password-icon-rev.png' }
        },
        RepeatPasswordField: {
            Placeholder: { Value: 'Repeat Password' }, ValidationPattern: { Value: '' },
            ErrorText: { Value: 'Repeat Password is incorrect' },
            Image: { Value: 'logon-password-icon-rev.png' }
        }
    },
    {
        EventId: 'SWITCH',
        Title: { Value: 'Switch' },
        Buttons: [
            {
                Action: 'HOME', Text: { Value: 'RESIDENTS' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            },
            {
                Action: 'MED_TOTE', Text: { Value: 'MED TOTES' },
                Image: { Value: 'PRN_30.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_30.png' }
            },
            {
                Action: 'SERVICES', Text: { Value: 'SERVICES' },
                Image: { Value: 'PRN_31.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_31.png' }
            }
        ]
    },
    {
        EventId: 'SERVICES',
        Title: { Value: 'Services' },
        Buttons: [
            {
                Action: 'MAINTENANCE', Text: { Value: 'Maintenance' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            },
            {
                Action: 'MECHANICALTOOLS', Text: { Value: 'Mechanical<br/>Tools' },
                Image: { Value: 'PRN_30.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_30.png' }
            },
            {
                Action: 'CONFIGURATION', Text: { Value: 'Configuration' },
                Image: { Value: 'PRN_31.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_31.png' }
            }
        ]
    },
    {
        EventId: 'MAINTENANCE',
        Title: { Value: 'Maintenance' },
        Buttons: [
            {
                Action: 'ENVELOPES', Text: { Value: 'Load<br/>Envelopes' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            },
            {
                Action: 'PRINTER', Text: { Value: 'Printer<br/>Ink' },
                Image: { Value: 'PRN_30.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_30.png' }
            },
            {
                Action: 'ARRAYACCESS', Text: { Value: 'Array<br/>Access' },
                Image: { Value: 'PRN_31.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_31.png' }
            },
            {
                Action: 'CARTRIGES', Text: { Value: 'Load<br/>Cartridges' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            }
        ],
        OpenDoorModal: {
            Header: { Value: 'Please follow the steps below:' },
            Button: {
                Action: '', Text: { Value: 'Open Door' }, Image: { Value: 'OPEN_DOOR_ICON.png' }
            },
            Background: { Value: 'POPUP_BKG.png' }
        },
        Modals: [
            {
                Header: { Value: 'Please follow the steps below:' },
                Button: { Action: 'ENVELOPES', Text: { Value: 'Load Envelopes' }, Image: { Value: 'LOAD_ICON.png' } },
                Background: { Value: 'POPUP_BKG.png' }
            },
            {
                Header: { Value: 'Please follow the steps below:' },
                Button: { Action: 'PRINTER', Text: { Value: 'Replace Ink' }, Image: { Value: 'LOAD_ICON.png' } },
                Background: { Value: 'POPUP_BKG.png' }
            },
            {
                Header: { Value: 'Please follow the steps below:' },
                Button: { Action: 'ARRAYACCESS', Text: { Value: 'Service Array' }, Image: { Value: 'LOAD_ICON.png' } },
                Background: { Value: 'POPUP_BKG.png' }
            }
        ],
        DoneModal: {
            Header: { Value: 'DONE' },
            Button: {
                Action: '', Text: { Value: 'Thank You' }, Image: { Value: 'DONE_ICON.png' }
            },
            Background: { Value: 'POPUP_BKG.png' }
        }
    },
    {
        EventId: 'RESIDENT_SWITCH',
        Title: { Value: 'Resident' },
        Subheader: { Value: 'Please Select' },
        UserDetails: USER_DETAILS,
        Buttons: [
            {
                Action: 'PRN', Text: { Value: 'PRN' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            },
            {
                Action: 'STAT', Text: { Value: 'STAT' },
                Image: { Value: 'PRN_30.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_30.png' }
            },
            {
                Action: 'RETURN_MEDS', Text: { Value: 'RETURN<br/>MEDS' },
                Image: { Value: 'PRN_31.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_31.png' }
            },
            {
                Action: 'LOA', Text: { Value: 'LOA' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            }
        ]
    },
    {
        EventId: 'HOME',
        Title: { Value: 'Residents' },
        PatientField: {
            Placeholder: { Value: 'Search Residents' },
            Image: { Value: 'resizeimage_home.png' }
        }
    },
    {
        EventId: 'STAT',
        Title: { Value: 'Stat' },
        UserDetails: USER_DETAILS,
        DoctorField: {
            Placeholder: { Value: 'Search Doctor' },
            Image: { Value: 'ResidentsStat_zoom.png' }
        },
        MedicationField: {
            Placeholder: { Value: 'SEARCH FOR MEDICATION' },
            Image: { Value: 'ResidentsStat_zoom.png' }
        },
        AddRow: { Value: 'ReturnMeds_AddPlus.png' },
        RemoveRow: { Value: 'ReturnMeds_RemoveMinus.png' },
        DoctorIcon: { Value: 'ResidentsStat_doctor.png' },
        PillIcon: { Value: 'ResidentsStat_pill.png' },
        BinIcon: { Value: 'ResidentsStat_bin.png' },
        Quantity: { Value: 'Quantity' },
        SelectedMedIcon: { Value: 'ResidentsStat_selected.png' },
        AddToPackage: {
            Action: 'DISPENSION',
            Text: { Value: 'Add To Package' },
            Image: { Value: 'Addtopackage-button.png' }
        }
    },
    {
        EventId: 'LOA',
        Title: { Value: 'LOA' },
        UserDetails: USER_DETAILS,
        LoaStartLabel: { Value: 'LOA START'},
        LoaEndLabel: { Value: 'LOA END'},
        LoaPicker: LOA_PICKER,
        Continue: {
            Action: 'UPSERT_LOA_DATA',
            Text: { Value: 'Continue' },
            Image: { Value: 'B_BKG_1.png' }
        }
    },
    {
        EventId: 'LOA_PRN',
        Title: { Value: 'LOA Medications' },
        UserDetails: USER_DETAILS,
        LoaStartLabel: { Value: 'LOA START'},
        LoaEndLabel: { Value: 'LOA END'},
        LoaPicker: LOA_PICKER,
        AddToPackage: {
            Action: 'DISPENSION',
            Text: { Value: 'Add To Package' },
            Image: { Value: 'Addtopackage-button.png' }
        },
        MedicationList: MEDICATION_LIST
    },
    {
        EventId: 'LOA_RETURN',
        Title: { Value: 'Early return from LOA' },
        UserDetails: USER_DETAILS,
        LoaStartLabel: { Value: 'LOA START'},
        LoaEndLabel: { Value: 'LOA END'},
        LoaPicker: LOA_PICKER,
        Question: { Value: 'Return Medication?'},
        QuestionDescription: { Value: 'Once selected you will be redirected to the Return Medication Page'},
        ReturnMedicationToggle: {
            Action: 'RETURN_MEDS',
            Text: { Value: 'YES' }, UncheckedText: { Value: 'NO' },
            Image: { Value: 'ResidentsLOA_checkOn.png'}, UncheckedImage: { Value: 'ResidentsLOA_checkOff.png'}
        },
        Return: {
            Action: 'RETURN_FROM_LOA',
            Text: { Value: 'Early return from LOA' },
            Image: { Value: 'ResidentsLOA_returnBtn.png' }
        }
    },
    {
        EventId: 'CARTRIGES',
        Title: { Value: 'Load Cartriges' },
        SearchField: {
            Placeholder: { Value: 'Search Med' },
            Image: { Value: 'SCREEN4_PRN_SEARCH_ICON.png' }
        },
        ArrayToggleButton: {
            Image: { Value: 'L_R_B_L.png' }, ActiveImage: { Value: 'L_R_B_R.png' }
        },
        ActionButtons: [
            {
                Action: 'SELECT_MODE', Text: { Value: 'Select' }, Image: { Value: 'B_BKG_1.png' }
            },
            {
                Action: 'SELECT_LOW_MODE', Text: { Value: 'Select low level' }, Image: { Value: 'B_BKG_1.png' }
            },
            {
                Action: 'CLEAR_ALL', Text: { Value: 'Clear All' }, Image: { Value: 'B_BKG_1.png' }
            },
            {
                Action: 'RESCAN', Text: { Value: 'Rescan' }, Image: { Value: 'B_BKG_1.png' }
            },
            {
                Action: 'LOAD_PREVIEW', Text: { Value: 'Load Preview' }, Image: { Value: 'B_BKG_1.png' }
            }
        ],
        StartLoad: {
            Action: 'START_LOAD_CARTRIGES', Text: { Value: 'Start Load' }, Image: { Value: 'START_LOAD_B.png' }
        },
        MedicationDetails: {
            WorkingContainerHeader: { Value: 'Working Container' },
            RefillCartridgeHeader: { Value: 'Refill Cartridge' },
            MedId: { Value: 'Med ID' },
            LotNumber: { Value: 'Lot Number' },
            ManufactureExpirationDate: { Value: 'Manufacture Expiration Date' },
            LoadExpirationDate: { Value: 'Load Expiration Date' },
            ManufactureNDC: { Value: 'Manufacture NDC' },
            Quantity: { Value: 'Quantity' },
            UseDate: { Value: 'Use Date' },
            TransID: { Value: 'Trans ID' },
            Imprint: { Value: 'Imprint' }
        },
        Legend: {
            LinkTitle: { Value: 'Legend' },
            Items: [
                { Color: { Value: '#ffffff' }, Text: { Value: 'Number in the box is the quantity remaining in the Working Container' } },
                { Color: { Value: '#64B3C9' }, Text: { Value: 'Selected to be Loaded' } },
                { Color: { Value: '#99CC32' }, Text: { Value: 'There is a full and unlocked Backup Cartridge on the Container' } },
                { Color: { Value: '#209F40' }, Text: { Value: 'Refill Cartridge is full and locked. Continue packaging even if count is 0' } },
                { Color: { Value: '#FEF694' }, Text: { Value: 'Working Container is above Par Level and the Backup Cartridge is empty' } },
                { Color: { Value: '#FFBD00' }, Text: { Value: 'Working Container is above Par Level and the Backup Cartridge is full, but locked' } },
                { Color: { Value: '#E11B1D' }, Text: { Value: 'Working Container is below Par Level and the Backup Cartridge is empty' } },
                { Color: { Value: '#9A0B0D' }, Text: { Value: 'Working Container is below Par Level and the Backup Cartridge is full, but locked' } },
                { Color: { Value: '#E7E7E7' }, Text: { Value: 'If there is a number in this box then this location has been locked. If blank, then it is an unusued location' } },
                { Color: { Value: '#979B9D' }, Text: { Value: 'The Backup Cartridge on this new Container is locked' } }
            ]
        },
        OpenDoorModal: {
            Header: { Value: 'Please follow the steps below:' },
            Button: {
                Action: '', Text: { Value: 'Open Door' }, Image: { Value: 'OPEN_DOOR_ICON.png' }
            },
            Background: { Value: 'POPUP_BKG.png' }
        },
        ReplaceModal: {
            Header: { Value: 'Please follow the steps below:' },
            Button: {
                Action: '', Text: { Value: 'Replace Cartriges' }, Image: { Value: 'LOAD_ICON.png' }
            },
            Background: { Value: 'POPUP_BKG.png' }
        }
    },
    {
        EventId: 'MECHANICALTOOLS',
        Title: { Value: 'Mechanical Tools' }
    },
    {
        EventId: 'PRN',
        Title: { Value: 'PRN' },
        Subtitle: { Value: 'PRN' },
        UserDetails: USER_DETAILS,
        MedicationList: MEDICATION_LIST,
        AddToPackage: {
            Action: 'DISPENSION', Text: { Value: 'Add To Package' }, Image: { Value: 'Addtopackage-button.png' }
        }
    },
    {
        EventId: 'FOLIO',
        Title: { Value: 'Folio' },
        UserDetails: USER_DETAILS,
        Notice: { Value: 'NOTICE' },
        Resident: { Value: 'Resident' },
        ResidentOn: { Value: 'Resident On' },
        Buttons: [
            { Action: 'PRN', Text: { Value: 'PRN' } },
            { Action: 'LOA_RETURN', Text: { Value: 'Return from LOA' } },
            { Action: 'RETURN_MEDS', Text: { Value: 'Return Meds' } },
            { Action: 'ADMISSION', Text: { Value: 'Readmit' } }
        ]
    },
    {
        EventId: 'SCHEDULE',
        Title: { Value: 'Schedule' },
        History: { Value: 'History' },
        Scheduled: { Value: 'Scheduled' },
        UserDetails: USER_DETAILS,
        Ok: {
            Action: 'FOLIO', Text: { Value: 'OK' }, Image: { Value: 'YES-NObutton.png' }
        }
    },
    {
        EventId: 'PROFILE',
        Title: { Value: 'Profile' },
        UserDetails: USER_DETAILS,
        Ok: {
            Action: 'FOLIO', Text: { Value: 'OK' }, Image: { Value: 'YES-NObutton.png' }
        }
    },
    {
        EventId: 'ADMISSION',
        Title: { Value: 'Admission' },
        Header: { Value: 'Resident Information' },
        NewResidentPrefix: 'TMP',
        UserDetails: USER_DETAILS,
        SearchField: {
            Placeholder: { Value: 'Search Resident' },
            Image: { Value: 'SCREEN4_PRN_SEARCH_ICON.png' }
        },
        AddResident: { Text: { Value: 'Add Resident' }, Image: { Value: 'YES-NObutton.png' } },
        EditResident: { Text: { Value: 'Edit Resident' }, Image: { Value: 'YES-NObutton.png' } },
        Redmit: { Text: { Value: 'Redmit' }, Image: { Value: 'YES-NObutton.png' } },
        Discharge: { Text: { Value: 'Discharge Resident' }, Image: { Value: 'YES-NObutton.png' } },
        Save: { Action: 'SAVE_RESIDENT', Text: { Value: 'Save' }, Image: { Value: 'YES-NObutton.png' } },
        Cancel: { Text: { Value: 'Cancel' }, Image: { Value: 'YES-NObutton.png' } },
        DatePickerModal: {
            SelectDate: { Value: 'SELECT DATE' },
            Save: { Text: { Value: 'Save' }, Image: { Value: 'B_BKG_1.png' } },
            Cancel: { Text: { Value: 'Cancel' }, Image: { Value: 'B_BKG_1.png' } }
        }
    },
    {
        EventId: 'RETURN_MEDS',
        Title: { Value: 'Return Meds' },
        Header: { Value: 'Scan Returned Envelope<br/>or Select Med Administration Date' },
        AdminTimeLabel: { Value: 'ADMIN TIME' },
        PackagedAt: { Value: 'Packaged at' },
        PlaceMedsInControlled: { Value: 'Place Meds In Controlled Bin' },
        PlaceMedsInReturn: { Value: 'Place Meds In Return Bin' },
        UserDetails: USER_DETAILS,
        DatePickerModal: {
            SelectDate: { Value: 'SELECT DATE' },
            Save: { Text: { Value: 'Save' }, Image: { Value: 'B_BKG_1.png' } },
            Cancel: { Text: { Value: 'Cancel' }, Image: { Value: 'B_BKG_1.png' } }
        },
        AddRow: { Value: 'ReturnMeds_AddPlus.png' },
        RemoveRow: { Value: 'ReturnMeds_RemoveMinus.png' },
        ControlledMedIcon: { Value: 'ReturnMeds_ControlledMed.png' },
        Next: { Text: { Value: 'Next' }, Image: { Value: 'B_BKG_1.png' } },
        SearchMeds: {
            Placeholder: { Value: 'SEARCH MEDICATION' }
        },
        SelectReason: {
            Placeholder: { Value: 'REASON' }
        },
        Buttons: [
            {
                Action: '0d', Text: { Value: 'TODAY' },
                Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            },
            {
                Action: '1d', Text: { Value: 'YESTERDAY' },
                Image: { Value: 'PRN_30.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_30.png' }
            }
        ],
        SelectDate: { Text: { Value: 'SELECT DATE' } },
        ReturnToBin: {
            Action: 'RETURN_MEDS_TO_BIN',
            Text: { Value: 'Return To Bin' },
            Image: { Value: 'YES-NObutton.png' }
        },
        ConfirmSelectionIcon: {
            Value: 'ReturnMeds_Selected.png'
        }
    },
    {
        EventId: 'MED_TOTE',
        Title: { Value: 'MedTote' },
        Subtitle: { Value: 'SELECT A TOTE' },
        Icon: {
            Value: 'MedTote_icon.png'
        },
        StationLabel: { Value: 'Station' },
        NAText: { Value: 'N/A' },
        Modal: {
            Icon: {
                Value: 'MedTote_icon.png'
            },
            Button: {
                Action: '', Text: { Value: 'Continue' }, Image: { Value: 'B_BKG_1.png' }
            },
            EmptyText: { Value: 'EMPTY' },
            PackagingDateLabel: { Value: 'Packaging Date' },
            Background: { Value: 'POPUP_BKG.png' }
        },
        Continue: {
            Action: 'HOME', Text: { Value: 'Continue' }, Image: { Value: 'Addtopackage-button.png' }
        }
    },
    {
        EventId: 'DISPENSION',
        Title: { Value: 'Dispense' },
        Subtitle: { Value: 'PRN' },
        Dispense: {
            Action: 'EXITCONTINUE', Text: { Value: 'DISPENSE' }, Image: { Value: 'DISPENSE_BUTTON.png' }
        },
        TimeoutInSec: 1,
        CartEmptyMessage: { Value: 'Cart is empty!' },
        AddMore: {
            Action: 'PRN', Text: { Value: 'Add More >' }
        },
        Prepare: {
            Text: { Value: 'Preparing Package...' }, Image: { Value: 'loading-3.gif' }
        }
    },
    {
        EventId: 'EXITCONTINUE',
        Title: { Value: 'Continue' },
        Buttons: [
            {
                Action: 'LOGON', Text: { Value: 'Log Out' },
                Image: { Value: 'PRN_NO_TEXT_55.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_57.png' }
            },
            {
                Action: 'SWITCH', Text: { Value: 'Continue' },
                Image: { Value: 'PRN_NO_TEXT_56.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_58.png' }
            }
        ]
    },
    {
        EventId: 'CONFIGURATION',
        Title: { Value: 'Configuration' },
        Buttons: [
            {
                Action: 'APS', Text: { Value: 'APS' }, Image: { Value: 'PRN_29.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_29.png' }
            },
            {
                Action: 'FACILITY', Text: { Value: 'Facility' },
                Image: { Value: 'PRN_30.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_30.png' }
            },
            {
                Action: 'PHARMACY', Text: { Value: 'Pharmacy' },
                Image: { Value: 'PRN_31.png' }, ActiveImage: { Value: 'PRN_NO_TEXT_31.png' }
            }
        ]
    },
    {
        EventId: 'APS',
        Title: { Value: 'Passport' },
        Subtitle: { Value: 'Passport Information' },
        More: {
            Action: 'CONFIGURATION', Text: { Value: 'More System Configurations' }, Image: { Value: 'LINK_B_W_ICON.png' }
        },
        SubPageEventId: 'GET_APS_INFO'
    },
    {
        EventId: 'FACILITY',
        Title: { Value: 'Facility' },
        Subtitle: { Value: 'Facility Information' },
        More: {
            Action: 'CONFIGURATION', Text: { Value: 'More System Configurations' }, Image: { Value: 'LINK_B_W_ICON.png' }
        },
        SubPageEventId: 'GET_FACILITY_INFO'
    },
    {
        EventId: 'PHARMACY',
        Title: { Value: 'Pharmacy' },
        Subtitle: { Value: 'Pharmacy Information' },
        More: {
            Action: 'CONFIGURATION', Text: { Value: 'More System Configurations' }, Image: { Value: 'LINK_B_W_ICON.png' }
        },
        SubPageEventId: 'GET_PHARMACY_INFO'
    },
    {
        EventId: 'UNAUTHORIZED',
        Title: { Value: 'Access Denied' },
        SubHeader: { Value: 'You are not authorized to view this page' },
        GoHome: {
            Action: 'SWITCH', Text: { Value: 'Go to Home page' }, Image: { Value: 'B_BKG_1.png' }
        },
        Icon: { Value: 'no_access.png' }
    },
    {
        EventId: 'BASKET_VM',
        Icon: { Value: 'ICONBAG.png' },
        Remove: { Value: 'SIDEBAR_X_ICON.png' },
        ResidentIcon: { Value: 'SIDEBAR_ICON1.png' },
        MedicationIcon: { Value: 'SIDEBAR_ICON2.png' },
        RemoveMedicationIcon: { Value: 'SIDEBAR_ICON3.png' },
        InQueue: { Value: 'PRN_36.png' },
        InBasket: { Value: 'PRN_37.png' },
        InPackaging: { Value: 'PRN_38.png' },
        Prepare: {
            Action: '', Text: { Value: 'Preparing Package...' }, Image: { Value: 'loading-3.gif' }
        },
        Dispense: {
            Action: 'EXITCONTINUE', Text: { Value: 'Dispense' }
        }
    },
    {
        EventId: 'HEADER_VM',
        Logo: { Value: 'PRN_03.png' },
        GoHome: {
            Action: 'SWITCH', Image: { Value: 'SCREEN1_08.png' }
        },
        RefreshPage: {
            Image: { Value: 'SCREEN1_10.png' }
        },
        Logout: {
            Action: 'LOGON', Image: { Value: 'SCREEN1_12.png' }
        },
        Notifications: {
            Image: { Value: 'MASSAGES_BLUE_ICON2.png' }
        },
        DeleteNotification: {
            Image: { Value: 'MASSAGES_BLUE_ICON1.png' }
        },
        Help: {
            Image: { Value: 'SCREEN1_29.png' }
        },
        SystemMessage: {
            Value: 'MASSAGE_RED_ICON1.png'
        },
        LogoutDialog: {
            Icon: { Value: 'LOGOUT_MASSAGE_ICON.png' },
            Confirmation: { Value: 'Are you sure you want to logout?' },
            BasketNotification: { Value: 'You have [count] item(s) in the basket - They will be deleted!' },
            Yes: {
                Text: { Value: 'Yes' }, Image: { Value: 'YES-NObutton.png' }
            },
            No: {
                Text: { Value: 'No' }, Image: { Value: 'YES-NObutton.png' }
            }
        }
    },
    {
        EventId: 'SEARCH_USERS',
        Records: [{ "UserID": 1, "FullName": "Stephen Walsh" }, { "UserID": 2, "FullName": "Merritt Schultz" }, { "UserID": 3, "FullName": "Grady Hubbard" }, { "UserID": 4, "FullName": "Tiger Stevens" }, { "UserID": 5, "FullName": "Gannon Bradshaw" }, { "UserID": 6, "FullName": "Ulric Dickerson" }, { "UserID": 7, "FullName": "Shad Chan" }, { "UserID": 8, "FullName": "Amos Hanson" }, { "UserID": 9, "FullName": "Boris Mcknight" }, { "UserID": 10, "FullName": "Hayes Horton" }, { "UserID": 11, "FullName": "Gareth Macias" }, { "UserID": 12, "FullName": "Ali West" }, { "UserID": 13, "FullName": "Dieter Cummings" }, { "UserID": 14, "FullName": "Brenden Massey" }, { "UserID": 15, "FullName": "Zane Pennington" }, { "UserID": 16, "FullName": "Jordan Madden" }, { "UserID": 17, "FullName": "Emmanuel Burt" }, { "UserID": 18, "FullName": "Guy Luna" }, { "UserID": 19, "FullName": "Raphael Mccall" }, { "UserID": 20, "FullName": "Hall Avery" }, { "UserID": 21, "FullName": "Jordan Meadows" }, { "UserID": 22, "FullName": "Wylie Shelton" }, { "UserID": 23, "FullName": "Lucas Kane" }, { "UserID": 24, "FullName": "Buckminster Silva" }, { "UserID": 25, "FullName": "Kadeem Moses" }, { "UserID": 26, "FullName": "Troy Ayala" }, { "UserID": 27, "FullName": "Ryan Lewis" }, { "UserID": 28, "FullName": "Marsden Cantu" }, { "UserID": 29, "FullName": "Wing Barrett" }, { "UserID": 30, "FullName": "Bert Sampson" }, { "UserID": 31, "FullName": "Clarke Barr" }, { "UserID": 32, "FullName": "Raymond Sloan" }, { "UserID": 33, "FullName": "Dieter Simpson" }, { "UserID": 34, "FullName": "Lars Calderon" }, { "UserID": 35, "FullName": "John Myers" }, { "UserID": 36, "FullName": "Bruce Buckley" }, { "UserID": 37, "FullName": "Kennedy Lester" }, { "UserID": 38, "FullName": "Blake Nixon" }, { "UserID": 39, "FullName": "Jameson Huffman" }, { "UserID": 40, "FullName": "Matthew Tucker" }, { "UserID": 41, "FullName": "Driscoll Osborn" }, { "UserID": 42, "FullName": "Magee Ewing" }, { "UserID": 43, "FullName": "Jakeem Boyer" }, { "UserID": 44, "FullName": "Hammett Clements" }, { "UserID": 45, "FullName": "Victor Nelson" }, { "UserID": 46, "FullName": "Herrod Coffey" }, { "UserID": 47, "FullName": "Jonah Hudson" }, { "UserID": 48, "FullName": "Erich Brown" }, { "UserID": 49, "FullName": "Matthew England" }, { "UserID": 50, "FullName": "Yoshio Battle" }, { "UserID": 51, "FullName": "Reed Estrada" }, { "UserID": 52, "FullName": "Zane Hayes" }, { "UserID": 53, "FullName": "Kennan Hahn" }, { "UserID": 54, "FullName": "Garth Mcconnell" }, { "UserID": 55, "FullName": "Fletcher Hester" }, { "UserID": 56, "FullName": "Perry Kaufman" }, { "UserID": 57, "FullName": "Price Kelley" }, { "UserID": 58, "FullName": "Allen Lee" }, { "UserID": 59, "FullName": "Bruce Powers" }, { "UserID": 60, "FullName": "Tate Pope" }, { "UserID": 61, "FullName": "Linus Petty" }, { "UserID": 62, "FullName": "Hamilton Cash" }, { "UserID": 63, "FullName": "Lester Campos" }, { "UserID": 64, "FullName": "Dylan Noel" }, { "UserID": 65, "FullName": "Kamal Brock" }, { "UserID": 66, "FullName": "Preston Mcintosh" }, { "UserID": 67, "FullName": "Xander Cabrera" }, { "UserID": 68, "FullName": "Randall Sargent" }, { "UserID": 69, "FullName": "Gil Mayo" }, { "UserID": 70, "FullName": "Josiah Jones" }, { "UserID": 71, "FullName": "Ali Decker" }, { "UserID": 72, "FullName": "Ciaran Pearson" }, { "UserID": 73, "FullName": "Eagan Hardy" }, { "UserID": 74, "FullName": "Keith Peck" }, { "UserID": 75, "FullName": "Abbot Woodward" }, { "UserID": 76, "FullName": "Ryan Harrell" }, { "UserID": 77, "FullName": "Perry Benson" }, { "UserID": 78, "FullName": "Garrett Bush" }, { "UserID": 79, "FullName": "Lucas Cervantes" }, { "UserID": 80, "FullName": "Marshall Pate" }, { "UserID": 81, "FullName": "Brady Mcdonald" }, { "UserID": 82, "FullName": "Dexter Walker" }, { "UserID": 83, "FullName": "Brendan Holman" }, { "UserID": 84, "FullName": "Duncan Mitchell" }, { "UserID": 85, "FullName": "Dolan Sherman" }, { "UserID": 86, "FullName": "Adrian Cortez" }, { "UserID": 87, "FullName": "Clark Richmond" }, { "UserID": 88, "FullName": "Rigel Williamson" }, { "UserID": 89, "FullName": "Wade Salinas" }, { "UserID": 90, "FullName": "Charles Espinoza" }, { "UserID": 91, "FullName": "Reese Dillard" }, { "UserID": 92, "FullName": "Craig Montoya" }, { "UserID": 93, "FullName": "Akeem Huff" }, { "UserID": 94, "FullName": "Stuart Oneill" }, { "UserID": 95, "FullName": "Carlos Chaney" }, { "UserID": 96, "FullName": "Joshua Lang" }, { "UserID": 97, "FullName": "Wyatt Henson" }, { "UserID": 98, "FullName": "Shad Norton" }, { "UserID": 99, "FullName": "Keegan Mcleod" }, { "UserID": 100, "FullName": "Daniel Dunn" }]
    },
    {
        EventId: 'SEARCH_PATIENTS',
        Records: [
            { "Status": 'Loa', "LOA": {"LoaStart": new Date(), "LoaEnd": new Date()}, "PID": "0004", "UId": '00000002', "FullName": "Tallulah Hurst", "Sex": 77, "Age": 43, "Allergies": ["Heparin", "Cephalexin", "Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago", "StationID": '0001', "StationName": 'Station 1', "RoomID": '01', "RoomName": "101", "BedID": '005', "BedName": '11' },
            { "Status": 'Discharged', "Discharged": new Date(), "PID": "0004", "UId": '2', "FullName": "Alvin Cardenas", "RoomName": 281, "Sex": 77, "Age": 40, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '3', "FullName": "Hu Pruitt", "RoomName": 497, "Sex": 70, "Age": 61, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": 'Bed Hold', "PID": "0004", "UId": '4', "FullName": "Fay Carrillo", "RoomName": 270, "Sex": 77, "Age": 60, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '5', "FullName": "Illiana Glenn", "RoomName": 422, "Sex": 70, "Age": 21, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '6', "FullName": "Alexa Mclaughlin", "RoomName": 171, "Sex": 77, "Age": 45, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '7', "FullName": "Hammett Witt", "RoomName": 256, "Sex": 77, "Age": 46, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '8', "FullName": "Eve Delaney", "RoomName": 187, "Sex": 77, "Age": 52, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '9', "FullName": "Harlan Stark", "RoomName": 338, "Sex": 77, "Age": 25, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '10', "FullName": "Forrest Sawyer", "RoomName": 379, "Sex": 77, "Age": 48, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '11', "FullName": "Emmanuel Golden", "RoomName": 460, "Sex": 77, "Age": 43, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '12', "FullName": "Dexter Bauer", "RoomName": 318, "Sex": 77, "Age": 41, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '13', "FullName": "Luke Cummings", "RoomName": 204, "Sex": 70, "Age": 58, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '14', "FullName": "Juliet Hopper", "RoomName": 490, "Sex": 77, "Age": 49, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '15', "FullName": "Jessica Prince", "RoomName": 440, "Sex": 77, "Age": 22, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '16', "FullName": "Nell Kim", "RoomName": 401, "Sex": 70, "Age": 39, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" },
            { "Status": '', "PID": "0004", "UId": '17', "FullName": "Boris Clemons", "RoomName": 292, "Sex": 70, "Age": 38, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 18, "FullName": "Morgan Bradley", "RoomName": 550, "Sex": 77, "Age": 56, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 19, "FullName": "Sybil Horton", "RoomName": 519, "Sex": 77, "Age": 33, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 20, "FullName": "Amity Finley", "RoomName": 467, "Sex": 70, "Age": 32, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 21, "FullName": "Cole Nixon", "RoomName": 131, "Sex": 77, "Age": 50, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 22, "FullName": "Jade Hartman", "RoomName": 424, "Sex": 77, "Age": 60, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 23, "FullName": "Elijah Owens", "RoomName": 281, "Sex": 77, "Age": 39, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 24, "FullName": "Dean Singleton", "RoomName": 458, "Sex": 77, "Age": 44, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 25, "FullName": "Imogene Preston", "RoomName": 260, "Sex": 70, "Age": 26, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 26, "FullName": "Merrill Dickson", "RoomName": 507, "Sex": 70, "Age": 20, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 27, "FullName": "Quynn Bentley", "RoomName": 513, "Sex": 77, "Age": 45, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 28, "FullName": "Brielle George", "RoomName": 523, "Sex": 77, "Age": 42, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 29, "FullName": "Forrest Hogan", "RoomName": 369, "Sex": 70, "Age": 31, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 30, "FullName": "Zena Haley", "RoomName": 485, "Sex": 77, "Age": 47, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 31, "FullName": "Lydia Cherry", "RoomName": 470, "Sex": 77, "Age": 48, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 32, "FullName": "Jakeem Hayden", "RoomName": 550, "Sex": 70, "Age": 46, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 33, "FullName": "Flynn Sanders", "RoomName": 191, "Sex": 70, "Age": 53, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 34, "FullName": "Mechelle Rosa", "RoomName": 476, "Sex": 70, "Age": 48, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 35, "FullName": "Olga Sanders", "RoomName": 562, "Sex": 70, "Age": 23, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 36, "FullName": "Piper Savage", "RoomName": 310, "Sex": 70, "Age": 25, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 37, "FullName": "Omar Cabrera", "RoomName": 250, "Sex": 70, "Age": 51, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 38, "FullName": "Audrey Aguirre", "RoomName": 508, "Sex": 70, "Age": 37, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 39, "FullName": "Thane Golden", "RoomName": 595, "Sex": 70, "Age": 43, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 40, "FullName": "Curran Pruitt", "RoomName": 582, "Sex": 70, "Age": 54, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 41, "FullName": "Althea Juarez", "RoomName": 369, "Sex": 77, "Age": 46, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 42, "FullName": "Ariana Tanner", "RoomName": 551, "Sex": 70, "Age": 44, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 43, "FullName": "Joseph Santos", "RoomName": 516, "Sex": 77, "Age": 38, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 44, "FullName": "Yvonne Jefferson", "RoomName": 499, "Sex": 77, "Age": 56, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 45, "FullName": "Vivian Fuller", "RoomName": 494, "Sex": 77, "Age": 61, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 46, "FullName": "Kuame Bowen", "RoomName": 284, "Sex": 77, "Age": 54, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 47, "FullName": "Casey Lane", "RoomName": 122, "Sex": 70, "Age": 55, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 48, "FullName": "Ezra Randall", "RoomName": 523, "Sex": 77, "Age": 53, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 49, "FullName": "Sheila Bauer", "RoomName": 365, "Sex": 70, "Age": 51, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 50, "FullName": "Damon Newton", "RoomName": 577, "Sex": 77, "Age": 59, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 51, "FullName": "Medge Dennis", "RoomName": 201, "Sex": 70, "Age": 55, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 52, "FullName": "Chelsea Macias", "RoomName": 567, "Sex": 70, "Age": 21, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 53, "FullName": "Mari Witt", "RoomName": 353, "Sex": 70, "Age": 33, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 54, "FullName": "Cameran Woodard", "RoomName": 503, "Sex": 77, "Age": 52, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 55, "FullName": "Quemby Beasley", "RoomName": 557, "Sex": 77, "Age": 33, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 56, "FullName": "Shay Guthrie", "RoomName": 597, "Sex": 77, "Age": 47, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 57, "FullName": "Cassidy Douglas", "RoomName": 445, "Sex": 77, "Age": 41, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 58, "FullName": "Ina Burch", "RoomName": 576, "Sex": 70, "Age": 23, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 59, "FullName": "Craig Patton", "RoomName": 271, "Sex": 77, "Age": 58, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 60, "FullName": "Hope Campos", "RoomName": 252, "Sex": 70, "Age": 36, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 61, "FullName": "Patricia Valentine", "RoomName": 180, "Sex": 77, "Age": 20, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 62, "FullName": "Joseph Contreras", "RoomName": 471, "Sex": 77, "Age": 35, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 63, "FullName": "Noel Weber", "RoomName": 380, "Sex": 77, "Age": 28, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 64, "FullName": "Henry Bolton", "RoomName": 573, "Sex": 77, "Age": 57, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 65, "FullName": "Joseph Mcclure", "RoomName": 143, "Sex": 77, "Age": 23, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 66, "FullName": "Ramona Henson", "RoomName": 435, "Sex": 70, "Age": 40, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 67, "FullName": "Kelly Cain", "RoomName": 180, "Sex": 70, "Age": 35, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 68, "FullName": "Hermione Roberson", "RoomName": 583, "Sex": 77, "Age": 24, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 69, "FullName": "Brandon Dawson", "RoomName": 529, "Sex": 70, "Age": 48, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 70, "FullName": "Jolene Jordan", "RoomName": 549, "Sex": 70, "Age": 59, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 71, "FullName": "Ifeoma Hampton", "RoomName": 577, "Sex": 77, "Age": 54, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 72, "FullName": "Gwendolyn Woodard", "RoomName": 243, "Sex": 77, "Age": 33, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 73, "FullName": "Kareem Aguilar", "RoomName": 179, "Sex": 77, "Age": 25, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 74, "FullName": "Kyla Hendricks", "RoomName": 411, "Sex": 70, "Age": 38, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 75, "FullName": "Autumn Anthony", "RoomName": 295, "Sex": 77, "Age": 26, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 76, "FullName": "Hashim Blair", "RoomName": 115, "Sex": 77, "Age": 35, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 77, "FullName": "Zeph Boone", "RoomName": 106, "Sex": 70, "Age": 25, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 78, "FullName": "Morgan Alford", "RoomName": 442, "Sex": 70, "Age": 44, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 79, "FullName": "Finn Landry", "RoomName": 280, "Sex": 70, "Age": 24, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 80, "FullName": "Fritz Mcfarland", "RoomName": 435, "Sex": 77, "Age": 20, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 81, "FullName": "Talon Douglas", "RoomName": 527, "Sex": 77, "Age": 44, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 82, "FullName": "Melyssa Mcgee", "RoomName": 468, "Sex": 77, "Age": 37, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 83, "FullName": "Jaden Buck", "RoomName": 483, "Sex": 70, "Age": 32, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 84, "FullName": "Garth Bennett", "RoomName": 452, "Sex": 77, "Age": 51, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 85, "FullName": "Venus Scott", "RoomName": 479, "Sex": 70, "Age": 58, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 86, "FullName": "Ross Marquez", "RoomName": 129, "Sex": 77, "Age": 57, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 87, "FullName": "Kitra Wade", "RoomName": 312, "Sex": 77, "Age": 44, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 88, "FullName": "Vladimir Mitchell", "RoomName": 536, "Sex": 77, "Age": 32, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 89, "FullName": "Moana Page", "RoomName": 173, "Sex": 70, "Age": 41, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 90, "FullName": "Barbara Anthony", "RoomName": 462, "Sex": 77, "Age": 43, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 91, "FullName": "Amelia Hawkins", "RoomName": 367, "Sex": 70, "Age": 36, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 92, "FullName": "Shafira Joseph", "RoomName": 217, "Sex": 77, "Age": 57, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 93, "FullName": "Cally Moody", "RoomName": 496, "Sex": 77, "Age": 40, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 94, "FullName": "Christian Kennedy", "RoomName": 556, "Sex": 70, "Age": 30, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 95, "FullName": "Shad Wiley", "RoomName": 442, "Sex": 77, "Age": 57, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 96, "FullName": "Mariam Mosley", "RoomName": 392, "Sex": 77, "Age": 54, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 97, "FullName": "Jillian Henson", "RoomName": 177, "Sex": 77, "Age": 31, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 98, "FullName": "Vivian Fields", "RoomName": 422, "Sex": 77, "Age": 59, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 99, "FullName": "Cassidy Sampson", "RoomName": 437, "Sex": 77, "Age": 38, "Allergies": ["Heparin, Cephalexin and Cephalosparin"], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }, { "Status": '', "PID": "0004", "UId": 100, "FullName": "Hashim Hardin", "RoomName": 280, "Sex": 77, "Age": 24, "Allergies": [], "BirthDate": '1983-12-09T00:00:00Z', "LastDose": "Ondasetron 4MG, 4 hours ago" }]
    },
    {
        EventId: 'SEARCH_MEDICATIONS',
        Records: MEDICATIONS
    },
    {
        EventId: 'SEARCH_SCHEDULE',
        Records: MEDICATIONS
    },
    {
        EventId: 'SEARCH_PROFILE_MEDS',
        Records: MEDICATIONS
    },
    {
        EventId: 'SEARCH_STAT_MEDICATIONS',
        Records: MEDICATIONS
    },
    {
        EventId: 'GET_APS_INFO',
        Records: [{ "Key": "Passport Name", "Value": "PASS004" }, { "Key": "Passport ID", "Value": "333444" }, { "Key": "State", "Value": "Active" }]
    },
    {
        EventId: 'GET_FACILITY_INFO',
        Records: [
            { "Key": "Facility Name", "Value": "PH084" }, { "Key": "Facility ID", "Value": "55535" }, { "Key": "Location", "Value": "England" },
            { "Key": "Address", "Value": "1695 Lepade Pt" }, { "Key": "State", "Value": "PA" }, { "Key": "City", "Value": "West Chester" },
            { "Key": "Zip", "Value": "19382" }, { "Key": "Phone", "Value": "(281)-309-897" }, { "Key": "Contact", "Value": "Ron White" }
        ]
    },
    {
        EventId: 'GET_PHARMACY_INFO',
        Records: [{ "Key": "Pharmacy Name", "Value": "PH084" }, { "Key": "Pharmacy ID", "Value": "55535" }, { "Key": "Region Name", "Value": "West Chester" }]
    },
    {
        EventId: 'GET_MEDICATION_MATRIX',
        Records: [
            {
                Location: '0101',
                ContainerMedication: { MedID: '12279', NDC: '12345678923', GenericName: 'FLODEXETINE CAP 40 MG Cap (LH1)', BrandName: 'A1', ExpirationDate: new Date(), ImageSrc: 'med1.png' },
                ContainerExpirationDate: new Date(), ContainerFillDate: new Date(), ContainerQuantity: 19,
                ContainerTransNum: 61515, ContainerLotNumber: 'HB4942',
                CartridgeMedication: { MedID: '12279', NDC: '12345678924', GenericName: 'FLODEXETINE CAP 40 MG Cap (LH1)', BrandName: 'A1', ExpirationDate: new Date(), ImageSrc: 'med1.png' },
                CartridgeExpirationDate: new Date(), CartridgeFillDate: new Date(), CartridgeQuantity: 170,
                CartridgeTransNum: 61515, CartridgeLotNumber: 'HB4942', Legend: 1
            },
            {
                Location: '0202',
                ContainerMedication: { MedID: '12333', NDC: '12355555923', GenericName: 'FLODEXETINE CAP 140 MG Cap (LH1)', BrandName: 'A2', ExpirationDate: new Date(), ImageSrc: 'med2.png' },
                ContainerExpirationDate: new Date(), ContainerFillDate: new Date(), ContainerQuantity: 11,
                ContainerTransNum: 61515, ContainerLotNumber: 'HB1111',
                CartridgeMedication: { MedID: '12333', NDC: '12355555923', GenericName: 'FLODEXETINE CAP 140 MG Cap (LH1)', BrandName: 'A2', ExpirationDate: new Date(), ImageSrc: 'med2.png' },
                CartridgeExpirationDate: new Date(), CartridgeFillDate: new Date(), CartridgeQuantity: 110,
                CartridgeTransNum: 61515, CartridgeLotNumber: 'HB1111', Legend: 5
            },
            {
                Location: '0303',
                ContainerMedication: { MedID: '13567', NDC: '44445678923', GenericName: 'FLODEXETINE CAP 400 MG Cap (LH1)', BrandName: 'A3', ExpirationDate: new Date(), ImageSrc: 'med3.png' },
                ContainerExpirationDate: new Date(), ContainerFillDate: new Date(), ContainerQuantity: 9,
                ContainerTransNum: 61515, ContainerLotNumber: 'HB3333',
                CartridgeMedication: { MedID: '13567', NDC: '44445678923', GenericName: 'FLODEXETINE CAP 400 MG Cap (LH1)', BrandName: 'A3', ExpirationDate: new Date(), ImageSrc: 'med3.png' },
                CartridgeExpirationDate: new Date(), CartridgeFillDate: new Date(), CartridgeQuantity: 70,
                CartridgeTransNum: 61515, CartridgeLotNumber: 'HB3333', Legend: 6
            }
        ]
    },
    {
        EventId: 'GET_TOP_MENU',
        Records: [
            {
                Title: 'Residents', Action: 'SWITCH',
                Children: [{ Title: 'PRN', Action: 'HOME', Children: [] }]
            },
            {
                Title: 'Services', Action: 'SERVICES',
                Children: [{
                    Title: 'Maintenance', Action: 'MAINTENANCE',
                    Children: [
                        { Title: 'Printer Ribbon', Action: 'MAINTENANCE', Children: [] },
                        { Title: 'Load Envelopes', Action: 'MAINTENANCE', Children: [] },
                        { Title: 'Array Access', Action: 'MAINTENANCE', Children: [] },
                        { Title: 'Load Cartridge', Action: 'CARTRIGES', Children: [] }
                    ]
                }]
            },
            {
                Title: 'Configuration', Action: 'CONFIGURATION',
                Children: [
                    { Title: 'APS', Action: 'APS', Children: [] },
                    { Title: 'Facility', Action: 'FACILITY', Children: [] },
                    { Title: 'Pharmacy', Action: 'PHARMACY', Children: [] }
                ]
            }
        ]
    },
    {
        EventId: 'GET_ADMIN_TIMES',
        Records: [
            { UId: '00000001-0000-0000-0000-000000000000', Name: 'Day' },
            { UId: '00000002-0000-0000-0000-000000000000', Name: 'Evening' },
            { UId: '00000003-0000-0000-0000-000000000000', Name: 'Night' }
        ]
    },
    {
        EventId: 'GET_STATIONS',
        Records: [
            {
                Name: 'Station 1',
                UId: '0001',
                Rooms: [
                    { UId: '01', Name: '101', Beds: [{ UId: '001', Name: '12' }, { UId: '005', Name: '11' }] },
                    { UId: '02', Name: '102', Beds: [{ UId: '002', Name: '1' }] },
                    { UId: '03', Name: '103', Beds: [{ UId: '003', Name: '2' }] },
                    { UId: '04', Name: '104', Beds: [{ UId: '004', Name: '3' }] }
                ]
            },
            {
                Name: 'Station 2',
                UId: '0002',
                Rooms: [
                    { UId: '21', Name: '201A', Beds: [{ UId: '006', Name: 'P6' }] }, { UId: '22', Name: '202', Beds: [] },
                    { UId: '23', Name: '203B', Beds: [] }, { UId: '24', Name: '204', Beds: [] }
                ]
            },
            {
                Name: 'Station 3',
                UId: '0003',
                Rooms: [
                    { UId: '31', Name: '305C', Beds: [] }, { UId: '32', Name: '306', Beds: [] },
                    { UId: '33', Name: '3038', Beds: [] }, { UId: '34', Name: '311-2', Beds: [] }
                ]
            },
            {
                Name: 'Station 4',
                UId: '0004',
                Rooms: [
                    { UId: '41', Name: '444', Beds: [] }, { UId: '42', Name: '455', Beds: [] },
                    { UId: '43', Name: '465A', Beds: [] }, { UId: '44', Name: '499', Beds: [] }
                ]
            }
        ]
    },
    {
        EventId: 'GET_DOCTORS',
        Records: [
            { UId: '001', FullName: 'James Smith' },
            { UId: '002', FullName: 'Michael Smith' },
            { UId: '003', FullName: 'Robert Smith' },
            { UId: '004', FullName: 'Maria Garcia' },
            { UId: '005', FullName: 'David Smith' },
            { UId: '006', FullName: 'Maria Rodriguez' },
            { UId: '007', FullName: 'Mary Smith' },
            { UId: '008', FullName: 'Maria Hernandez' },
            { UId: '009', FullName: 'Bethel Robert' }
        ]
    },
    {
        EventId: 'GET_MED_TOTES',
        Records: [
            { UId: '001', StationNo: 'W15', StationUId: '01', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 10, 2),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Ready', EnvelopesPackaged: 30, EnvelopesTotal: 30 },
            { UId: '002', StationNo: 'W18', StationUId: '02', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 11, 8),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Packaging', EnvelopesPackaged: 'X', EnvelopesTotal: 30 },
            { UId: '003', StationNo: 'W17', StationUId: '03', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 12, 12),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Reserved', EnvelopesPackaged: 0, EnvelopesTotal: 30 },
            { UId: '004', StationNo: 'W23', StationUId: '04', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 11, 2),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Available', EnvelopesPackaged: 0, EnvelopesTotal: 0 },
            { UId: '005', StationNo: 'W24', StationUId: '05', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 11, 12),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Ready', EnvelopesPackaged: 30, EnvelopesTotal: 30 },
            { UId: '006', StationNo: 'W27', StationUId: '06', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 10, 22),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Ready', EnvelopesPackaged: 30, EnvelopesTotal: 30 },
            { UId: '007', StationNo: '', StationUId: '10', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 10, 23),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Ready', EnvelopesPackaged: 30, EnvelopesTotal: 30 },
            { UId: '008', StationNo: 'W28', StationUId: '07', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 10, 24),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Reserved', EnvelopesPackaged: 0, EnvelopesTotal: 30 },
            { UId: '009', StationNo: 'W15', StationUId: '01', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 11, 25),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Empty', EnvelopesPackaged: '-', EnvelopesTotal: '-' },
            { UId: '010', StationNo: 'W22', StationUId: '09', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 11, 5),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Empty', EnvelopesPackaged: '-', EnvelopesTotal: '-' },
            { UId: '011', StationNo: '', StationUId: '11', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 11, 9),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Ready', EnvelopesPackaged: 30, EnvelopesTotal: 30 },
            { UId: '012', StationNo: 'W21', StationUId: '08', StationName: 'JONES NORTH Clava Victoria', Packaged: new Date(2018, 10, 25),
                ScheduledBy: 'Kevin Bradley', AdminTime: 'Tue Evening', State: 'Ready', EnvelopesPackaged: 30, EnvelopesTotal: 30 }
        ]
    },
    {
        EventId: 'SEARCH_SCHEDULE',
        Records: [
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 500MG', Quantity: 1, RoNumber: 12345, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 600MG', Quantity: 1, RoNumber: 12346, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 700MG', Quantity: 1, RoNumber: 12347, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 800MG', Quantity: 1, RoNumber: 12348, AdminBy: 'Andrey B', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 900MG', Quantity: 1, RoNumber: 12349, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 100MG', Quantity: 1, RoNumber: 12350, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 200MG', Quantity: 1, RoNumber: 12351, AdminBy: 'Andrey F', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 300MG', Quantity: 1, RoNumber: 12352, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 400MG', Quantity: 1, RoNumber: 12353, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 500MG', Quantity: 1, RoNumber: 12354, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 600MG', Quantity: 1, RoNumber: 12355, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 700MG', Quantity: 1, RoNumber: 12356, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 800MG', Quantity: 1, RoNumber: 12357, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 900MG', Quantity: 1, RoNumber: 12358, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 100MG', Quantity: 1, RoNumber: 12359, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' },
            { Date: new Date(), AdminTime: new Date(), MedicationName: 'ACETAMIN TAB 200MG', Quantity: 1, RoNumber: 12360, AdminBy: 'Tal T', PackagedTime: new Date(), PackagedBy: 'Tal T', RemovedTime: new Date(), RemovedBy: 'Tal T', ReturnedTime: new Date(), ReturnedQuantity: 1, ReturnedBy: 'Tal T', ReturnedReason: 'Pending...' }
        ]
    },
    {
    EventId: 'VAULT_REFRIGERATOR',
    Title: { Value: 'Vault/Refrigerator' },
    UserDetails: USER_DETAILS,
      ActionButtons: [
        {
          Action: 'OPEN_UPPER_VAULT', Text: { Value: 'Open Upper Vault' }, Image: { Value: 'B_BKG_1.png' }
        },
        {
          Action: 'OPEN_LOWER_VAULT', Text: { Value: 'Open Lower Vault' }, Image: { Value: 'B_BKG_1.png' }
        },
        {
          Action: 'OPEN_REFRIGERATOR', Text: { Value: 'Open Refrigerator' }, Image: { Value: 'B_BKG_1.png' }
        }
      ],
      ColumnNamesAsArray: [
        '', 'QTY', 'MED', ''
      ],
      GridItems: [
        {
          Header: 'Upper Vault', Message: 'Door open - Close door to continue', Key: 'OPEN_UPPER_VAULT'
        },
        {
          Header: 'Lower Vault', Message: 'Door open - Close door to continue', Key: 'OPEN_LOWER_VAULT'
        },
        {
          Header: 'Refrigerator', Message: 'Door open - Close door to continue', Key: 'OPEN_REFRIGERATOR'
        }
      ],
      Select: {
        Image: { Value: 'SCREEN4_PRN_V_ICON_HIGHLIGHT.png' }, UncheckedImage: { Value: 'SCREEN4_PRN_V_ICON.png' }
      },
      Cross: { Value: 'SIDEBAR_X_ICON.png' }
  },
  {
    EventId: 'GET_VAULT_REFRIGERATOR_RECORDS',
    Records: VAULT_REFRIGERATOR_RECORDS
  },
  {
    EventId: 'DRAWERS_LOAD',
    Title: { Value: 'Drawers/Load' },
    ActionButtons: [
      {
        Action: 'OPEN_UPPER_VAULT', Text: { Value: 'Open Upper Vault' }, Image: { Value: 'B_BKG_1.png' }
      },
      {
        Action: 'OPEN_LOWER_VAULT', Text: { Value: 'Open Lower Vault' }, Image: { Value: 'B_BKG_1.png' }
      },
      {
        Action: 'OPEN_REFRIGERATOR', Text: { Value: 'Open Refrigerator' }, Image: { Value: 'B_BKG_1.png' }
      },
      {
        Action: 'CLOSE', Text: { Value: 'Close' }, Image: { Value: 'B_BKG_1.png' }
      },
    ],
    ColumnNamesAsArray: [
      '', 'MED', 'DISPENSE FORM', 'SCAN QTY', 'COUNT', 'DISCREPANCY', ''
    ],
    DoorOpenMessage: 'Door open - Close door to continue',
    InventoryReport: {
      Action: 'INVENTORY_REPORT',
      Text: { Value: 'See Inventory Report' },
      Image: { Value: 'INVENTORY_REPORT.png' }
    },
    Select: {
      Image: { Value: 'SCREEN4_PRN_V_ICON_HIGHLIGHT.png' }, UncheckedImage: { Value: 'SCREEN4_PRN_V_ICON.png' }
    },
    SectionSelect: {
      Image: { Value: 'FULL_Services_Maintenance_HIGHLIGHT.png' }, UncheckedImage: { Value: 'FULL_Services_Maintenance.png' }
    },
    Cross: { Value: 'SIDEBAR_X_ICON.png' }
  },
  {
    EventId: 'DRAWERS_INVENTORY',
    Title: { Value: 'Drawers/Inventory' },
    ActionButtons: [
      {
        Action: 'OPEN_DOOR', Text: { Value: '' }, Image: { Value: 'B_BKG_1.png' }
      },
      {
        Action: 'RESCAN', Text: { Value: 'Rescan' }, Image: { Value: 'B_BKG_1.png' }
      },
      {
        Action: 'CANCEL', Text: { Value: 'Cancel' }, Image: { Value: 'B_BKG_1.png' }
      },
      {
        Action: 'RECONCILE', Text: { Value: 'Reconcile' }, Image: { Value: 'B_BKG_1.png' }
      }
    ],
    DoorOpenMessage: 'Door open - Close door to continue',
    ColumnNamesAsArray: [
      'MED', 'DISPENSE FORM', 'SCAN QTY', 'COUNT', 'DISCREPANCY', ''
    ],
    InventoryReport: {
      Action: 'INVENTORY_REPORT',
      Text: { Value: 'See Inventory Report' },
      Image: { Value: 'INVENTORY_REPORT.png' }
    },
    Select: {
      Image: { Value: 'SCREEN4_PRN_V_ICON_HIGHLIGHT.png' }, UncheckedImage: { Value: 'SCREEN4_PRN_V_ICON.png' }
    },
    Cross: { Value: 'SIDEBAR_X_ICON.png' }
  },
  {
    EventId: 'GET_DRAWERS_INVENTORY_RECORDS',
    Records: DRAWERS_INVENTORY_RECORDS
  }
];

export class MockBound implements IBound {

    messageId: string;
    messageContent: string;
    messageDetails: string;
    messageType: MessageType;

    constructor() { }

    sendEvent(eventData: any): void {

        let cmdName = eventData.commandName;
        let vmCommands = VIEW_MODEL_COMMANDS.filter(p => p.EventId.toLowerCase() === cmdName.toLowerCase());
        if (cmdName === 'LOGIN') {

            const loginInfo = JSON.parse(eventData.data);
            let _res: AuthenticationResult;
            if (loginInfo.password == '0000') {
                _res = AuthenticationResult.InvalidFingerprint;

            } else if (loginInfo.password != '1234') {
                _res = AuthenticationResult.InvalidPassword;

            } else {
                _res = AuthenticationResult.Success;

                // go to Home page after success Login
                vmCommands = VIEW_MODEL_COMMANDS.filter(p => p.EventId.toLowerCase() === 'switch');
            }
            this.messageContent = JSON.stringify(_res);
            this.messageId = 'LOGIN';
            this.messageType = MessageType.Default;
            (<any>window).receiveCommandCallback('LOGIN', _res, null, MessageType.Default);
        }
        if (cmdName === 'RESET_PASSWORD') {
            vmCommands = VIEW_MODEL_COMMANDS.filter(p => p.EventId.toLowerCase() === 'switch');
        }
        if (cmdName === 'GET_DISPENSES_BY_USER') {

            timer(1000).subscribe(_ => {

                const inProgressPackages: CartElem[] = [
                    {
                        PackageId: '61685afb-7462-4be9-98d6-e95bd472d2f0',
                        Status: 0,
                        Patient: {
                            PID: '0000010', Age: 54, Allergies: [], BirthDate: new Date(), FullName: 'Machamed Bashar',
                            LastDose: '', RoomName: '202A', Sex: 70, Status: 'LOA',
                            UId: '00000001-7462-4be9-98d6-e95bd472d2f0'
                        },
                        AuthorId: '7c2fa339-cc50-4fb8-b135-685ab6a7928f',
                        Items: [{
                            ItemId: 'ca21b74a-85e0-43cf-b888-ac00c66d5144', MedicationId: '7534d919-bdb7-4041-ade4-4f1f793e4a0a',
                            MedicationName: 'My Tots 50MG', SigId: '010101', Quantity: 1
                        }]
                    },
                    {
                        PackageId: '66666666-7777-4444-9999-e95bd472d2f0',
                        Status: 7,
                        Patient: {
                            PID: '0000011', Age: 34, Allergies: [], BirthDate: new Date(), FullName: 'Andrey Fedotov',
                            LastDose: '', RoomName: '202A', Sex: 77, Status: 'LOA',
                            UId: '00000002-7462-4be9-98d6-e95bd472d2f0'
                        },
                        AuthorId: '7c2fa339-cc50-4fb8-b135-685ab6a7928f',
                        Items: [{
                            ItemId: 'ca21b74a-85e0-43cf-b888-ac00c66d5144', MedicationId: '7534d919-bdb7-4041-ade4-4f1f793e4a0a',
                            MedicationName: 'My Meds 150MG', SigId: '010101', Quantity: 1
                        }]
                    },
                    {
                        PackageId: '77777777-7462-4be9-98d6-e95bd472d2f0',
                        Status: 0,
                        Patient: {
                            PID: '0000010', Age: 54, Allergies: [], BirthDate: new Date(), FullName: 'Machamed Bashar',
                            LastDose: '', RoomName: '202A', Sex: 70, Status: 'LOA',
                            UId: '00000001-7462-4be9-98d6-e95bd472d2f0'
                        },
                        AuthorId: '7c2fa339-cc50-4fb8-b135-685ab6a7928f',
                        Items: [{
                            ItemId: 'ca21b74a-85e0-43cf-b888-ac00c66d5144', MedicationId: '7534d919-bdb7-4041-ade4-4f1f793e4a0a',
                            MedicationName: 'My Drugs 222MG', SigId: '010141', Quantity: 1
                        }]
                    }
                ];

                this.messageContent = JSON.stringify({ Packages: inProgressPackages });
                this.messageId = 'GET_DISPENSES_BY_USER';
                this.messageType = MessageType.Default;

                (<any>window).receiveCommandCallback('GET_DISPENSES_BY_USER', { Packages: inProgressPackages }, null, MessageType.Default);
            });
        }
        if (cmdName == 'GET_FULL_PATIENT') {

            timer(100).subscribe(_ => {

                const resList = (<any>VIEW_MODEL_COMMANDS.filter(p => p.EventId.toLowerCase() === 'search_patients')[0])
                    .Records.filter(n => n.UId == JSON.parse(eventData.data));

                this.messageContent = JSON.stringify(resList[0]);
                this.messageId = 'GET_FULL_PATIENT';
                this.messageType = MessageType.Default;

                (<any>window).receiveCommandCallback('GET_FULL_PATIENT', resList[0], null, MessageType.Default);
            });
        }
        if (cmdName == 'CONTROLLER_OPEN_DOOR') {

            timer(2500).subscribe(_ => {

                this.messageContent = JSON.stringify({});
                this.messageId = 'CONTROLLER_DOOR_OPENED';
                this.messageType = MessageType.Default;

                (<any>window).receiveCommandCallback('CONTROLLER_DOOR_OPENED', {}, null, MessageType.Default);
            });
            timer(5000).subscribe(_ => {

                this.messageContent = JSON.stringify({});
                this.messageId = 'CONTROLLER_DOOR_CLOSED';
                this.messageType = MessageType.Default;

                (<any>window).receiveCommandCallback('CONTROLLER_DOOR_CLOSED', {}, null, MessageType.Default);
            });
        }
        if (cmdName == 'UPSERT_LOA_DATA') {
            vmCommands = VIEW_MODEL_COMMANDS.filter(p => p.EventId.toLowerCase() === 'loa_prn');
        }

        if (vmCommands.length) {

            let msgContent: any = {};
            let msgType: MessageType;
            if (cmdName == 'SEARCH_USERS') {
                const request = JSON.parse(eventData.data) as SearchRequest;
                msgContent = {
                    Records: (<any>vmCommands[0]).Records.filter(n => n['FullName'].toLowerCase().indexOf(request.Queries['FullName'].toLowerCase()) >= 0)
                };
                msgType = MessageType.Default;
                if (request.PageSize > 0) {
                    msgContent.Records = msgContent.Records.slice(request.PageSize * request.PageNumber, (request.PageSize * request.PageNumber) + request.PageSize);
                }
            } else if (cmdName == 'SEARCH_PATIENTS') {
                const request = JSON.parse(eventData.data) as SearchRequest;
                msgContent = {
                    Records: (<any>vmCommands[0]).Records.filter(n =>
                        n['FullName'].toLowerCase().indexOf(request.Queries['Name'].toLowerCase()) >= 0 ||
                        n['RoomName'].toString().indexOf(request.Queries['Name'].toLowerCase()) >= 0 ||
                        n['PID'].toLowerCase().indexOf(request.Queries['Name'].toLowerCase()) >= 0)
                };
                msgType = MessageType.Default;
                if (request.PageSize > 0) {
                    msgContent.Records = msgContent.Records.slice(request.PageSize * request.PageNumber, (request.PageSize * request.PageNumber) + request.PageSize);
                }
            } else if (cmdName === 'SEARCH_MEDICATIONS' || cmdName === 'SEARCH_SCHEDULE' ||
                       cmdName === 'SEARCH_PROFILE_MEDS' || cmdName === 'SEARCH_STAT_MEDICATIONS') {
                const request = JSON.parse(eventData.data) as SearchRequest;
                msgContent = {
                    Records: request.Queries['MedicationName'] == '' || request.Queries['MedicationName'] == undefined
                        ? (<any>vmCommands[0]).Records
                        : (<any>vmCommands[0]).Records.filter(n => n['MedicationName'].toLowerCase().indexOf(request.Queries['MedicationName'].toLowerCase()) >= 0)
                };
                msgType = MessageType.Default;
                if (request.PageSize > 0) {
                    msgContent.Records = msgContent.Records.slice(request.PageSize * request.PageNumber, (request.PageSize * request.PageNumber) + request.PageSize);
                }
            } else if (cmdName === 'GET_APS_INFO' || cmdName === 'GET_FACILITY_INFO' || cmdName === 'GET_PHARMACY_INFO' ||
                       cmdName === 'GET_MEDICATION_MATRIX' || cmdName === 'GET_TOP_MENU' || cmdName === 'GET_STATIONS' ||
                       cmdName === 'GET_ADMIN_TIMES' || cmdName === 'GET_DOCTORS' || cmdName === 'GET_MED_TOTES' || cmdName === 'GET_VAULT_REFRIGERATOR_RECORDS' ||
            cmdName === 'GET_DRAWERS_INVENTORY_RECORDS') {
                msgType = MessageType.Default;
                msgContent = {
                    Records: (<any>vmCommands[0]).Records
                };
            } else {
                msgContent = vmCommands[0];
                msgType = MessageType.ViewModel;
            }

            this.messageContent = JSON.stringify(msgContent);
            this.messageId = vmCommands[0].EventId;
            this.messageType = msgType;

            setTimeout(function () {
                (<any>window).receiveCommandCallback(vmCommands[0].EventId, msgContent, null, msgType);
            }, 300);
        }
    }

    sendPeripheralCommand(id: string, content: string): void {

        this.messageContent = content;
        this.messageId = id;
        this.messageType = MessageType.Peripheral;

        (<any>window).receiveCommandCallback(id, JSON.parse(content), null, MessageType.Peripheral);
    }

    savePageVM(modelAsJson: string) {

        console.log(modelAsJson);
    }
}
