import {CommonModule} from '@angular/common';
import {ModuleWithProviders, Optional, SkipSelf} from '@angular/core';
import {NgModule, ErrorHandler} from '@angular/core';
import {StoreModule, Store} from '@ngrx/store';
import {Ng4LoadingSpinnerModule} from 'ng4-loading-spinner';

/* REDUCERS*/
import {commandReducer} from '../../reducers/command.reducer';
import {pageReducer} from '../../reducers/page.reducer';
import {notificationReducer} from '../../reducers/notification.reducer';
import {passportContextReducer} from '../../reducers/passportContext.reducer';

/* SERVICES */
import {AppSandbox} from '../../app.sandbox';
import {AppFrameSandbox} from '../../app-frame.sandbox';
import {CustomErrorHandler} from './customErrorHandler';

@NgModule({
  imports: [
    CommonModule,
    Ng4LoadingSpinnerModule.forRoot(),
    StoreModule.forRoot({
        lastCommand: commandReducer,
        navHistory: pageReducer,
        notifications: notificationReducer,
        passportContext: passportContextReducer
    })
  ]
})
export class CoreModule {

    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                {
                    provide: AppSandbox,
                    useFactory: sandboxFactory,
                    deps: [Store]
                },
                {
                    provide: ErrorHandler,
                    useClass: CustomErrorHandler,
                }
            ]
        };
    }
}

export function sandboxFactory(store: Store<any>) {
    if (window.frameElement) {
        // in frame
        return new AppFrameSandbox();
    } else {
        // not in frame
        return new AppSandbox(store);
    }
}
