import { ErrorHandler, Injectable, Injector} from '@angular/core';

import { AppSandbox } from '../../app.sandbox';

@Injectable()
export class CustomErrorHandler extends ErrorHandler {

    constructor(private injector: Injector) {

        super();
    }

    handleError(error: Error) {

        const ev = {
            commandName: 'WRITE_TO_LOG',
            type: 'log',
            ID: null,
            data: error.message
        };

        try {
            // custom handler
            const sb = this.injector.get(AppSandbox);
            sb.sendEventToServer(ev);
        } catch (e) {
            console.log('Cannot send error to Client. Exception: ' + e.message);
        } finally {
            // default handler
            super.handleError(error);
        }
    }
}
