/* tslint:disable:member-ordering */
import { Directive, ElementRef, HostListener, Input, Output, Renderer2, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs/Subscription';

import * as _ from 'lodash';

import { IEditorOptions, IEditorType } from '../models/editorOptions.model';
import { TextEditorComponent } from '../editors/text-editor/text-editor.component';
import { PlaylistEditorComponent } from '../editors/playlist-editor/playlist-editor.component';
import { ButtonEditorComponent } from '../editors/button-editor/button-editor.component';
import { BaseItemEditorComponent } from '../editors/base-item-editor/base-item-editor.component';
import { ImageEditorComponent } from '../editors/image-editor/image-editor.component';
import { InputEditorComponent } from '../editors/input-editor/input-editor.component';

@Directive({
    selector: '[appInEdit]',
    // outputs: ['updateModel']
})
export class InlineEditDirective {
    @Input('appInEdit') options: IEditorOptions;
    // @Output() updEdit = new EventEmitter();

    modalSubscription: Subscription = new Subscription();

    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private modalService: BsModalService) { }

    @HostListener('mouseenter') onMouseEnter() {

        if (this.options.enabled) {
            this.renderer.addClass(this.el.nativeElement, 'edit-mode-' + (this.options.pos || 'rt'));
        }
    }

    @HostListener('mouseleave') onMouseLeave() {

        if (this.options.enabled) {
            this.renderer.removeClass(this.el.nativeElement, 'edit-mode-' + (this.options.pos || 'rt'));
        }
    }

    @HostListener('click', ['$event']) onMouseClick(event: any) {
        if (!this.options.enabled) {
            return;
        }

        const zoneSize = 40;
        const isEditIconClicked =
            ((this.options.pos === 'rt' || this.options.pos == undefined) && event.offsetY < zoneSize && (event.target.clientWidth - event.offsetX) <= zoneSize) ||
            (this.options.pos === 'lt' && event.offsetY <= zoneSize && event.offsetX <= zoneSize) ||
            (this.options.pos === 'rb' && (event.target.clientWidth - event.offsetX) <= zoneSize && (event.target.clientHeight - event.offsetY) <= zoneSize) ||
            (this.options.pos === 'lb' && event.offsetX <= zoneSize && (event.target.clientHeight - event.offsetY) <= zoneSize);

        if (isEditIconClicked) {
            this.showEditorDialog();

            // HACK to NOT got to the Next page
            throw new Error('error');
        }
    }

    showEditorDialog(): void {
        let _clone = _.cloneDeep(this.options.ctrl);
        let _prop = this.options.path === undefined ? _clone : _.get(_clone, this.options.path);

        // show Modal dialog
        const dialogRef = this.modalService.show(this.editorComponent, {
            initialState: {
                control: _prop
            }
        });

        // subscribe to modal Close event
        this.modalSubscription = this.modalService.onHidden.subscribe(() => {
            const { control, needSaveChanges } = dialogRef.content;

            if (needSaveChanges) {
                // save changes to page VM
                if (this.options.path === undefined) {
                    _.merge(this.options.ctrl, control);
                } else {
                    _.set(this.options.ctrl, this.options.path, control);
                }

                // send changes to Server
                window.parent.postMessage({
                    type: 'update_vm',
                    propType: this.options.type,
                    prop: control,
                }, '*');
                // this.updEdit.emit(control);
                //'::UPDATE_VM_PROP::' + JSON.stringify(control)
            }

            // unsubscribe
            this.modalSubscription.unsubscribe();
        });
    }

    get editorComponent(): any {

        switch (this.options.type) {

            case IEditorType.Text:
                return TextEditorComponent;
            case IEditorType.Playlist:
                return PlaylistEditorComponent;
            case IEditorType.Button:
                return ButtonEditorComponent;
            case IEditorType.BaseItem:
                return BaseItemEditorComponent;
            case IEditorType.Image:
                return ImageEditorComponent;
            case IEditorType.BaseInput:
                return InputEditorComponent;
            default:
                return null;
        }
    }
}
