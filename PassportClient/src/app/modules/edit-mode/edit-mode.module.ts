import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

/*DIRECTIVES*/
import { InlineEditDirective } from './directives/inline-edit.directive';

/*CONTROLS*/
import { BaseEditorComponent } from './editors/base-editor.component';
import { ActionsBlockComponent } from './editors/actions-block/actions-block.component';
import { TextEditorComponent } from './editors/text-editor/text-editor.component';
import { PlaylistEditorComponent } from './editors/playlist-editor/playlist-editor.component';
import { ButtonEditorComponent } from './editors/button-editor/button-editor.component';
import { BaseItemEditorComponent } from './editors/base-item-editor/base-item-editor.component';
import { ImageEditorComponent } from './editors/image-editor/image-editor.component';
import { InputEditorComponent } from './editors/input-editor/input-editor.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        InlineEditDirective,
        BaseEditorComponent,
        ActionsBlockComponent,
        TextEditorComponent,
        PlaylistEditorComponent,
        ButtonEditorComponent,
        BaseItemEditorComponent,
        ImageEditorComponent,
        InputEditorComponent
    ],
    exports: [
        InlineEditDirective
    ],
    entryComponents: [
        TextEditorComponent,
        PlaylistEditorComponent,
        ButtonEditorComponent,
        BaseItemEditorComponent,
        ImageEditorComponent,
        InputEditorComponent
    ]
})
export class InlineEditorModule { }
