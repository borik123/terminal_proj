import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-actions-block',
    templateUrl: './actions-block.template.html',
    styleUrls: ['./actions-block.component.less']
})
export class ActionsBlockComponent {
    @Output() saveForm = new EventEmitter();
    @Output() cancel = new EventEmitter();

}
