import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-base-editor',
    template: ''
})
export class BaseEditorComponent {
    needSaveChanges = false;
    control: any;

    constructor(public bsModalRef: BsModalRef) { }

    saveForm(): void {

        this.needSaveChanges = true;
        this.closeModalDialog();
    }
    cancel(): void {

        this.needSaveChanges = false;
        this.closeModalDialog();
    }
    closeModalDialog(): void {

        // close modal dialog
        this.bsModalRef.hide();
        this.bsModalRef = null;
    }
}
