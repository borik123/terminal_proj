import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-base-item-editor',
    templateUrl: './base-item-editor.template.html',
    styleUrls: ['./base-item-editor.component.less']
})
export class BaseItemEditorComponent extends BaseEditorComponent {

}
