import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-button-editor',
    templateUrl: './button-editor.template.html',
    styleUrls: ['./button-editor.component.less']
})
export class ButtonEditorComponent extends BaseEditorComponent {

}
