import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-image-editor',
    templateUrl: './image-editor.template.html',
    styleUrls: ['./image-editor.component.less']
})
export class ImageEditorComponent extends BaseEditorComponent {

}
