import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-input-editor',
    templateUrl: './input-editor.template.html',
    styleUrls: ['./input-editor.component.less']
})
export class InputEditorComponent extends BaseEditorComponent {

}
