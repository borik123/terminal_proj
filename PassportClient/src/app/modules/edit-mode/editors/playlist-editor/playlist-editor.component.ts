import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-playlist-editor',
    templateUrl: './playlist-editor.template.html',
    styleUrls: ['./playlist-editor.component.less']
})
export class PlaylistEditorComponent extends BaseEditorComponent {

}
