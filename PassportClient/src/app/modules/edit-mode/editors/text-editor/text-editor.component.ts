import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseEditorComponent } from '../base-editor.component';

@Component({
    selector: 'app-text-editor',
    templateUrl: './text-editor.template.html',
    styleUrls: ['./text-editor.component.less']
})
export class TextEditorComponent extends BaseEditorComponent {

}
