﻿export enum IEditorType {
    Text = 0,
    Button = 1,
    ToggleButton = 2,
    Image = 3,
    GroupedButton = 4,
    CheckBox = 5,
    BaseItem = 6,
    BaseInput = 7,
    Audio = 8,
    Playlist = 9
}

export interface IEditorOptions {
    ctrl: any;
    path: string;
    enabled: boolean;
    pos: string; // lt - LeftTop, rt - RightTop, lb - LeftBottom, rb - RightBottom
    type: IEditorType;
}
