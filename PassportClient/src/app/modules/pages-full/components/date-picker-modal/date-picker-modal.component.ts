﻿import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-date-picker-modal',
    templateUrl: './date-picker-modal.template.html',
    styleUrls: ['./date-picker-modal.component.less']
})
export class DatePickerModalComponent {
    selectedDate: Date = null;
    datePickerModalVM: any;
    assetsFolderPath: string;

    constructor(public bsModalRef: BsModalRef) { }

    saveSelectedDate(): void {
        if (this.selectedDate !== null) {
            this.closeModalDialog();
        }
    }
    cancelDateSelection(): void {

        this.selectedDate = null;
        this.closeModalDialog();
    }

    closeModalDialog(): void {

        // close modal dialog
        this.bsModalRef.hide();
        this.bsModalRef = null;
    }
}
