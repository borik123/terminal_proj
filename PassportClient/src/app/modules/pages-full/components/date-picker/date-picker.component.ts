﻿import { Component, ViewChild, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import * as moment from 'moment';

@Component({
    selector: 'app-date-picker',
    templateUrl: './date-picker.template.html',
    styleUrls: ['./date-picker.component.less'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => DatePickerComponent),
        multi: true
    }]
})
export class DatePickerComponent implements ControlValueAccessor {
    @ViewChild('popM') popM: any;
    @ViewChild('popD') popD: any;
    @ViewChild('popY') popY: any;

    months: string[] = moment.months();
    years: number[] = [2017, 2018, 2019, 2020, 2021, 2022, 2023];
    days: number[] = [];

    disabled = false;
    selectedMonth: number = moment().month();
    selectedMonthAsStr: string = this.months[this.selectedMonth];
    selectedDay: number = moment().date();
    selectedYear: number = moment().year();

    getDays = (year: number, month: number): number[] =>
        Array.from(new Array(moment({year: year, month: month}).daysInMonth()), (val, index) => index + 1)

    selectMonth(val: number): void {
        this.writeValue(new Date(this.selectedYear, val, this.selectedDay));
        this.popM._popover.hide();
    }
    selectDay(val: number): void {
        this.writeValue(new Date(this.selectedYear, this.selectedMonth, val));
        this.popD._popover.hide();
    }
    selectYear(val: number): void {
        this.writeValue(new Date(val, this.selectedMonth, this.selectedDay));
        this.popY._popover.hide();
    }

    onChange = (date: Date) => {};
    onTouched = () => {};
    writeValue(date: Date): void {
        const newValue: Date = (date === undefined || date === null)
            ? new Date() // default value = NOW
            : date;
        // update inner variables
        this.selectedYear = newValue.getFullYear();
        this.selectedMonth = newValue.getMonth();
        this.selectedMonthAsStr = this.months[this.selectedMonth];
        this.selectedDay = newValue.getDate();
        this.days = this.getDays(this.selectedYear, this.selectedMonth);
        // clear time
        newValue.setHours(0, 0, 0, 0);
        // update input from parent control
        this.onChange(this.selectedDay <= this.days.length ? newValue : null);
    }
    registerOnChange(fn: (date: Date) => void): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}
