﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LoaPickerValue } from '../loa-picker/loa-picker.component';
import { AppSandbox } from '../../../../app.sandbox';

@Component({
    selector: 'app-loa-picker-popover',
    templateUrl: './loa-picker-popover.template.html',
    styleUrls: ['./loa-picker-popover.component.less']
})
export class LoaPickerPopoverComponent implements OnInit {
    adminTimes: Observable<any[]> = this.sb.adminTimes$;

    @Input() pickerVM: any;
    @Input() isChange = false;
    @Input() loaDateValue: LoaPickerValue;
    @Output() updateLoa = new EventEmitter();

    constructor(public sb: AppSandbox) { }

    ngOnInit() {
        this.sb.sendEvent('GET_ADMIN_TIMES');
    }
}
