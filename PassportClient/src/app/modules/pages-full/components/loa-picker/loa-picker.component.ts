﻿import { Component, Input, forwardRef } from '@angular/core';
import { FormBuilder, FormGroup, RequiredValidator, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs/Subscription';
import { TimePickerComponent } from '../time-picker/time-picker.component';

export interface LoaPickerValue {
    date: Date;
    time: Date;
    adminTime: string;
}
const INITIAL_VALUE: LoaPickerValue = {
    date: new Date(),
    time: new Date(1970, 1, 1, 0, 0, 0, 0),
    adminTime: ''
};

@Component({
    selector: 'app-loa-picker',
    templateUrl: './loa-picker.template.html',
    styleUrls: ['./loa-picker.component.less'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => LoaPickerComponent),
        multi: true
    }]
})
export class LoaPickerComponent implements ControlValueAccessor {
    pickerForm: FormGroup;

    @Input() pickerVM: any;
    @Input() isChange = false;
    @Input() adminTimes: any[] = [];

    constructor(private modalService: BsModalService, private readonly fb: FormBuilder) {
        this.pickerForm = this.fb.group({
            date: ['', RequiredValidator],
            time: '',
            adminTime: ''
        });

        this.pickerForm.valueChanges.subscribe(v => {
            this.onChange(this.pickerForm.valid ? v : null);
        });
    }

    openTimePicker(): void {
        const _time: Date = this.pickerForm.controls['time'].value;
        const dateDialogRef = this.modalService.show(TimePickerComponent, {
            initialState: {
                selectedHour: _time.getHours().toString().padStart(2, '0'),
                selectedMinute:
                    (_time.getMinutes() - _time.getMinutes() % 5).toString().padStart(2, '0')
            },
            ignoreBackdropClick: false,
            class: 'date-picker-dialog'
        });

        // subscribe to modal Close event
        const modalSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
            if (dateDialogRef.content.selectedTime) {
                this.pickerForm.patchValue({time: dateDialogRef.content.selectedTime});
            }
            modalSubscription.unsubscribe();
        });
    }

    onChange = (val: LoaPickerValue) => {};
    onTouched = () => {};
    writeValue(val: LoaPickerValue): void {
        const newVal = val || INITIAL_VALUE;
        this.pickerForm.setValue(newVal);
        // this.onChange(newVal);
    }
    registerOnChange(fn: (val: LoaPickerValue) => void): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled: boolean): void { }
}
