﻿import { Component } from '@angular/core';
import { ButtonElement } from '../../../shared/models/contentElems.model';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

interface TimeObj {
    class: string;
    value: string;
}

@Component({
    selector: 'app-time-picker',
    templateUrl: './time-picker.template.html',
    styleUrls: ['./time-picker.component.less']
})
export class TimePickerComponent {
    selectedHour: string;
    selectedMinute: string;
    selectedTime: Date;

    hoursAM: TimeObj[] = [
        { class: 'twelve', value: '12'}, { class: 'one', value: '01'}, { class: 'two', value: '02'},
        { class: 'three', value: '03'}, { class: 'four', value: '04'}, { class: 'five', value: '05'},
        { class: 'six', value: '06'}, { class: 'seven', value: '07'}, { class: 'eight', value: '08'},
        { class: 'nine', value: '09'}, { class: 'ten', value: '10'}, { class: 'eleven', value: '11'}
    ];
    hoursPM: TimeObj[] = [
        { class: 'zero', value: '00'}, { class: 'thirteen', value: '13'}, { class: 'fourteen', value: '14'},
        { class: 'fifteen', value: '15'}, { class: 'sixteen', value: '16'}, { class: 'seventeen', value: '17'},
        { class: 'eighteen', value: '18'}, { class: 'nineteen', value: '19'}, { class: 'twenty', value: '20'},
        { class: 'twenty-one', value: '21'}, { class: 'twenty-two', value: '22'}, { class: 'twenty-three', value: '23'}
    ];
    minutes: TimeObj[] = [
        { class: 'twelve', value: '00'}, { class: 'one', value: '05'}, { class: 'two', value: '10'},
        { class: 'three', value: '15'}, { class: 'four', value: '20'}, { class: 'five', value: '25'},
        { class: 'six', value: '30'}, { class: 'seven', value: '35'}, { class: 'eight', value: '40'},
        { class: 'nine', value: '45'}, { class: 'ten', value: '50'}, { class: 'eleven', value: '55'}
    ];

    okBtn: ButtonElement = {
        Action: '', Text: { Value: 'OK', Key: '' }, Image: { Value: 'B_BKG_1.png', Key: '' }
    };

    constructor(public bsModalRef: BsModalRef) { }

    selectTime(): void {
        this.selectedTime = new Date(1970, 1, 1, parseInt(this.selectedHour, 10), parseInt(this.selectedMinute, 10));

        // close modal dialog
        this.bsModalRef.hide();
        this.bsModalRef = null;
    }

    getHourHandDeg(): string {
        // [style.transform]="'rotate(' + ((selectedHour%12)*30) + 'deg)'"
        return parseInt(this.selectedHour, 10) % 12 * 30 + 'deg';
    }
    getMinuteHandDeg(): string {
        // [style.transform]="'rotate(' + (((selectedMinute/5)%12)*30) + 'deg)'"
        return (parseInt(this.selectedMinute, 10) / 5) % 12 * 30 + 'deg';
    }
}
