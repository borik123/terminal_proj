﻿import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { ToggleButtonElement } from '../../../shared/models/contentElems.model';

@Component({
    selector: 'app-yes-no-ctrl',
    templateUrl: './yes-no-ctrl.template.html',
    styleUrls: ['./yes-no-ctrl.component.less'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => YesNoCtrlComponent),
        multi: true
    }]
})
export class YesNoCtrlComponent implements ControlValueAccessor {
    @Input() toggle: ToggleButtonElement;
    @Input() assetsFolderPath: string;

    selectedValue = false;

    updateSelection(newValue: boolean): void {
        this.selectedValue = newValue;
        this.onChange(newValue);
    }

    onChange = (val: boolean) => {};
    onTouched = () => {};
    writeValue(val: boolean): void {
        this.selectedValue = val;
    }
    registerOnChange(fn: (val: boolean) => void): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }
    setDisabledState(isDisabled: boolean): void { }
}
