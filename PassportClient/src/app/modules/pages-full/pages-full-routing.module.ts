import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { SimpleLayoutComponent } from '../../modules/shared/layouts/simple.component';
import { MasterLayoutComponent } from '../../modules/shared/layouts/master.component';

const SIMPLE_ROUTES: Route[] = [
];

const MASTER_ROUTES: Route[] = [
];

const routes: Route[] = [
    { path: '', component: SimpleLayoutComponent, children: SIMPLE_ROUTES },
    { path: '', component: MasterLayoutComponent, children: MASTER_ROUTES }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PagesFullRoutingModule { }
