import {NgModule} from '@angular/core';
import {SharedModule} from '../../modules/shared/shared.module';

/* COMPONENTS */
import {LoaPickerComponent} from './components/loa-picker/loa-picker.component';
import {LoaPickerPopoverComponent} from './components/loa-picker-popover/loa-picker-popover.component';
import {TimePickerComponent} from './components/time-picker/time-picker.component';
import {DatePickerComponent} from './components/date-picker/date-picker.component';
import {DatePickerModalComponent} from './components/date-picker-modal/date-picker-modal.component';
import {YesNoCtrlComponent} from './components/yes-no-ctrl/yes-no-ctrl.component';

/* PAGES */
import {LoadCartrigesComponent} from './pages/load-cartriges/load-cartriges.component';
import {LoaComponent} from './pages/loa/loa.component';
import {LoaPrnComponent} from './pages/loa-prn/loa-prn.component';
import {LoaReturnComponent} from './pages/loa-return/loa-return.component';
import {AdmissionComponent} from './pages/admission/admission.component';
import {ResidentSwitchComponent} from './pages/resident-switch/resident-switch.component';
import {ReturnMedsComponent} from './pages/return-meds/return-meds.component';
import {StatComponent} from './pages/stat/stat.component';
import {MedToteComponent} from './pages/med-tote/med-tote.component';
import {VaultRefrigeratorComponent} from './pages/vault-refrigerator/vault-refrigerator.component';
import {DrawersLoadComponent} from './pages/drawers-load/drawers-load.component';
import {DrawersInventoryComponent} from './pages/drawers-inventory/drawers-inventory.component';

@NgModule({
    declarations: [
        LoadCartrigesComponent,
        LoaComponent,
        LoaPrnComponent,
        LoaReturnComponent,
        AdmissionComponent,
        ResidentSwitchComponent,
        LoaPickerComponent,
        LoaPickerPopoverComponent,
        ReturnMedsComponent,
        StatComponent,
        MedToteComponent,
        TimePickerComponent,
        DatePickerComponent,
        DatePickerModalComponent,
        YesNoCtrlComponent,
        VaultRefrigeratorComponent,
        DrawersLoadComponent,
        DrawersInventoryComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        LoadCartrigesComponent,
        LoaComponent,
        LoaPrnComponent,
        LoaReturnComponent,
        AdmissionComponent,
        ResidentSwitchComponent,
        ReturnMedsComponent,
        StatComponent,
        MedToteComponent,
        VaultRefrigeratorComponent,
        DrawersLoadComponent,
        DrawersInventoryComponent
    ],
    entryComponents: [DatePickerModalComponent, TimePickerComponent]
})
export class PagesFullModule {
}
