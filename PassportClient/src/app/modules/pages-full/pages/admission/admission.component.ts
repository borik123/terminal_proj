﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { filter } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Resident } from '../../../shared/models/passportContext.model';
import { IStation, IRoom, IBed } from '../../../shared/models/station.model';

import { DatePickerModalComponent } from '../../components/date-picker-modal/date-picker-modal.component';

interface IData {
    pid: string;
    lastName: string;
    firstName: string;
    dob: string;
    gender: string;
    medRecNo: string;
    admitDate: string;
    station: string;
    room: string;
    bed: string;
}

@Component({
    selector: 'app-admission',
    templateUrl: './admission.template.html',
    styleUrls: ['./admission.component.less']
})
export class AdmissionComponent extends BasicPageComponent implements OnInit, OnDestroy {
    private modalSubscription: Subscription = new Subscription();

    formMode = 'Display';
    selectedResident: Resident;
    searchForm: FormGroup;
    dataForm: FormGroup;
    stations: IStation[] = [];
    btnStyle: any = {'font-size': '24px', 'color': '#5C5C5C'};
    genders: any[] = [];
    SAVE_PATIENT_EVENT_ID = 'SAVE_RESIDENT';
    private localSubscription: Subscription = new Subscription();

    constructor(public sb: AppSandbox, private readonly _fb: FormBuilder, private modalService: BsModalService) {
        super(sb);

        this.searchForm = this._fb.group({
            resident: ['', Validators.required]
        });
        this.dataForm = this._fb.group({
            pid: ['', Validators.required],
            lastName: ['', Validators.required],
            firstName: ['', Validators.required],
            dob: ['', Validators.required],
            gender: ['', Validators.required],
            medRecNo: ['', Validators.required],
            admitDate: ['', Validators.required],
            station: ['', Validators.required],
            room: ['', Validators.required],
            bed: ['', Validators.required]
        });

        this.searchForm.valueChanges
            .pipe(
                filter((term: any) => term.resident && term.resident.FullName.length > 0)
            )
            .subscribe(val => {
                this.sendClickEvent('GET_FULL_PATIENT', val.resident.UId);
            });

        this.genders = [
            {k: '77', v: this.page.UserDetails.Male.Value}, {k: '70', v: this.page.UserDetails.Female.Value}
        ];
    }

    ngOnInit() {
        this.localSubscription.add(
            this.sb.selectedPatient$.subscribe(patient => this.initDataForm(patient))
        );

        this.localSubscription.add(
            this.context$.subscribe(r => this.initDataForm(r.currentResident))
        );

        this.localSubscription.add(
            this.sb.stations$.subscribe(st => this.stations = st)
        );

        this.loadStations();
    }

    goToNewMode(): void {
        this.formMode = 'New';
        const newId: number = Math.floor(Math.random() * 100000) + 1;

        this.dataForm.setValue({
            pid: `${this.page.NewResidentPrefix}${newId.toString().padStart(5, '0')}`,
            lastName: '',
            firstName: '',
            dob: '',
            gender: '70', // Female
            medRecNo: '',
            admitDate: this.formatDate(new Date()),
            station: '',
            room: '',
            bed: ''
        } as IData);
    }
    goToEditMode(): void {
        this.formMode = 'Edit';

        if (this.selectedResident) {
            this.dataForm.setValue({
                pid: this.selectedResident.PID,
                lastName: this.selectedResident.FullName.split(' ')[1],
                firstName: this.selectedResident.FullName.split(' ')[0],
                dob: this.formatDate(this.selectedResident.BirthDate),
                gender: this.selectedResident.Sex.toString(),
                medRecNo: this.selectedResident.MedicalRecNo || '',
                admitDate: this.selectedResident.AdmitDate
                    ? this.formatDate(this.selectedResident.AdmitDate)
                    : '',
                station: this.selectedResident.StationID || '',
                room: this.selectedResident.RoomID || '',
                bed: this.selectedResident.BedID || ''
            } as IData);
        }
    }
    redmit(): void {
        this.selectedResident.Status = 'Admitted';
        this.selectedResident.AdmitDate = new Date();
        this.sendClickEvent(this.SAVE_PATIENT_EVENT_ID, this.selectedResident);
    }
    discharge(): void {
        this.selectedResident.Status = 'Discharged';
        this.selectedResident.Discharged = new Date();
        this.sendClickEvent(this.SAVE_PATIENT_EVENT_ID, this.selectedResident);
    }
    save(): void {
        if (!this.isDataFormValid()) {
            return;
        }

        // New Mode fields
        if (this.isNewMode()) {
            this.selectedResident.UId = '00000000-0000-0000-0000-000000000000';
            this.selectedResident.PID = this.dataForm.get('pid').value;
            this.selectedResident.FullName = this.dataForm.get('firstName').value + ' ' + this.dataForm.get('lastName').value;
            this.selectedResident.FirstName = this.dataForm.get('firstName').value;
            this.selectedResident.LastName = this.dataForm.get('lastName').value;
            this.selectedResident.BirthDate = new Date(Date.parse(this.dataForm.get('dob').value));
            this.selectedResident.Sex = parseInt(this.dataForm.get('gender').value, 10);
            this.selectedResident.MedicalRecNo = this.dataForm.get('medRecNo').value;
            this.selectedResident.AdmitDate = new Date(Date.parse(this.dataForm.get('admitDate').value));
            this.selectedResident.Discharged = null;
            this.selectedResident.Status = null;
        }
        // Edit Mode fields
        this.selectedResident.StationID = this.dataForm.get('station').value;
        this.selectedResident.RoomID = this.dataForm.get('room').value;
        this.selectedResident.BedID = this.dataForm.get('bed').value;
        this.selectedResident.StationName =
            this.findStationById(this.selectedResident.StationID).Name;
        this.selectedResident.RoomName =
            this.findRoomById(this.selectedResident.StationID, this.selectedResident.RoomID).Name;
        this.selectedResident.BedName =
            this.findBedById(this.selectedResident.StationID, this.selectedResident.RoomID, this.selectedResident.BedID).Name;

        // send data to Server
        this.sendClickEvent(this.SAVE_PATIENT_EVENT_ID, this.selectedResident);
        this.formMode = 'Display';
    }
    cancel(): void {
        this.formMode = 'Display';
    }

    initDataForm(resident: Resident): void {
        this.selectedResident = resident;

        // this.searchForm.setValue({resident: ''}); // clear Autocomplete control
        this.formMode = 'Display';
    }
    isDisplayMode(): boolean {
        return this.formMode === 'Display';
    }
    isEditMode(): boolean {
        return this.formMode === 'Edit';
    }
    isNewMode(): boolean {
        return this.formMode === 'New';
    }
    isDataFormValid(): boolean {
        if (this.isEditMode() &&
            !(this.dataForm.get('station').valid && this.dataForm.get('room').valid && this.dataForm.get('bed').valid)) {
            return false;
        }
        if (this.isNewMode() && !this.dataForm.valid) {
            return false;
        }

        return true;
    }

    loadStations(): void {
        this.sendClickEvent('GET_STATIONS');
    }
    getGenderAsLongString(genderKey: number): string {
        return this.genders.filter(g => g.k === genderKey.toString())[0].v;
    }
    get rooms(): IRoom[] {
        const selectedStationId: string = this.dataForm.controls['station'].value;
        const selectedStation: IStation = this.findStationById(selectedStationId);
        return selectedStation !== null ? selectedStation.Rooms : [];
    }
    get beds(): IBed[] {
        const selectedStationId: string = this.dataForm.controls['station'].value;
        const selectedRoomId: string = this.dataForm.controls['room'].value;
        const selectedRoom: IRoom = this.findRoomById(selectedStationId, selectedRoomId);
        return selectedRoom !== null && selectedRoom.Beds !== null
            ? selectedRoom.Beds
            : [];
    }
    stationChanged(selectedStationId: string): void {
        const selectedStation: IStation = this.findStationById(selectedStationId);
        if (selectedStation !== null && selectedStation.Rooms !== null && selectedStation.Rooms.length) {
            const roomId: string = selectedStation.Rooms[0].UId;
            this.dataForm.patchValue({room: roomId});

            this.roomChanged(roomId);
        } else {
            this.dataForm.patchValue({room: '', bed: ''});
        }
    }
    roomChanged(selectedRoomId: string): void {
        const selectedStationId: string = this.dataForm.controls['station'].value;
        const selectedRoom: IRoom = this.findRoomById(selectedStationId, selectedRoomId);
        if (selectedRoom !== null && selectedRoom.Beds !== null && selectedRoom.Beds.length) {
            this.dataForm.patchValue({bed: selectedRoom.Beds[0].UId});
        } else {
            this.dataForm.patchValue({bed: ''});
        }
    }

    private findStationById(stationId: string): IStation {
        const stations: IStation[] = this.stations.filter(s => s.UId === stationId);
        return stations.length ? stations[0] : null;
    }
    private findRoomById(stationId: string, roomId: string): IRoom {
        const selectedStation: IStation = this.findStationById(stationId);
        if (selectedStation !== null && selectedStation.Rooms != null) {
            const rooms: IRoom[] = selectedStation.Rooms.filter(r => r.UId === roomId);
            return rooms.length ? rooms[0] : null;
        } else {
            return null;
        }
    }
    private findBedById(stationId: string, roomId: string, bedId: string): IBed {
        const selectedRoom: IRoom = this.findRoomById(stationId, roomId);
        if (selectedRoom !== null && selectedRoom.Beds != null) {
            const beds: IBed[] = selectedRoom.Beds.filter(b => b.UId === bedId);
            return beds.length ? beds[0] : null;
        } else {
            return null;
        }
    }

    private formatDate(dt: Date): string {
        return dt.toLocaleString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' }).substr(0, 10);
    }

    openDatePickerModal(): void {
        // Open Modal Dialog
        const dateDialogRef = this.modalService.show(DatePickerModalComponent, {
            initialState: {
                datePickerModalVM: this.page.DatePickerModal,
                assetsFolderPath: this.assetsFolderPath
            },
            ignoreBackdropClick: false,
            class: 'date-picker-dialog'
        });

        // subscribe to modal Close event
        // Update BirthDate on Close
        this.modalSubscription = this.modalService.onHidden.subscribe(() => {
            if (dateDialogRef.content.selectedDate) {
                this.dataForm.patchValue({dob: this.formatDate(dateDialogRef.content.selectedDate)});
            }
            this.modalSubscription.unsubscribe();
        });
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
