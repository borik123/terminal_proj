import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ToggleButtonElement } from '../../../shared/models/contentElems.model';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
@Component({
  selector: 'app-drawers-inventory',
  templateUrl: './drawers-inventory.template.html',
  styleUrls: ['./drawers-inventory.component.less']
})
export class DrawersInventoryComponent extends BasicPageComponent implements OnInit, OnDestroy {
  @Input() toggle: ToggleButtonElement;

  private localSubscription: Subscription = new Subscription();
  public gridItems: any;
  public tabItems: any;
  public selectedTab: any;
  public actionButtons: any;
  public doorOpen: boolean;

  constructor(public sb: AppSandbox) {
    super(sb);

    this.toggle = this.page.Select;
    this.actionButtons = this.page.ActionButtons;
    this.gridItems = this.page.GridItems;

    this.localSubscription.add(
      this.sb.inventoryRecords$.subscribe(ir => {
        this.tabItems = ir;
        if (this.tabItems.length > 0) {
          this.tabItems[0].Selected = true;
          this.selectedTab = this.tabItems[0];
          this.actionButtons[0].Text.Value = 'Open ' + this.selectedTab.InventoryType + ' door';
        }
      }),
    );
  }

  ngOnInit() {
    this.getRecords();
  }

  ngOnDestroy() {
    if (this.localSubscription) {
      this.localSubscription.unsubscribe();
    }
    super.ngOnDestroy();
  }

  private getRecords(): void {
    this.sb.sendEvent('GET_DRAWERS_INVENTORY_RECORDS');
  }

  actionBtnClicked(btn): void {
    btn.Clicked = !btn.Clicked;

    if (btn.Action === 'OPEN_DOOR') {
      this.doorOpen = btn.Clicked;
    }
  }

  selectItem(record): void {
    if (!this.doorOpen) {
      record.Selected = !record.Selected;
    }
  }

  selectTab(tab): void {
    if (this.doorOpen) {
      return;
    }

    this.tabItems.forEach(function (tabItem) {
      tabItem.Selected = false;
    });

    tab.Selected = true;
    this.selectedTab = tab;
    this.actionButtons[0].Text.Value = 'Open ' + this.selectedTab.InventoryType + ' door';
  }

  removeItem(recordToRemove): void {
    if (!this.doorOpen) {
      this.selectedTab.InventoryItems = this.selectedTab.InventoryItems.filter(item => item !== recordToRemove);
    }
  }
}
