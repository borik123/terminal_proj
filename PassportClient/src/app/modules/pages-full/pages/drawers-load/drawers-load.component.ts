import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ToggleButtonElement } from '../../../shared/models/contentElems.model';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
import {forEach} from '@angular/router/src/utils/collection';
@Component({
  selector: 'app-drawers-load',
  templateUrl: './drawers-load.template.html',
  styleUrls: ['./drawers-load.component.less']
})
export class DrawersLoadComponent extends BasicPageComponent implements OnInit, OnDestroy {
  @Input() toggle: ToggleButtonElement;
  @Input() toggleSection: ToggleButtonElement;

  private localSubscription: Subscription = new Subscription();
  public sectionItems: any;
  public actionButtons: any;

  constructor(public sb: AppSandbox) {
    super(sb);

    this.toggle = this.page.Select;
    this.toggleSection = this.page.SectionSelect;

    this.actionButtons = this.page.ActionButtons;

    this.localSubscription.add(
      this.sb.inventoryRecords$.subscribe(ir => {
        this.sectionItems = ir;
        this.sectionItems.forEach(function (item) {
          item.Selected = true;
        });
      }),
    );
  }

  ngOnInit() {
    this.getRecords();
  }

  ngOnDestroy() {
    if (this.localSubscription) {
      this.localSubscription.unsubscribe();
    }
    super.ngOnDestroy();
  }

  private getRecords(): void {
    this.sb.sendEvent('GET_DRAWERS_INVENTORY_RECORDS');
  }

  actionBtnClicked(btn, index): void {
    btn.Clicked = !btn.Clicked;
    this.sectionItems[index].Blocked = btn.Clicked;
  }

  selectItem(record, index): void {
    if (!this.sectionItems[index].Blocked) {
      record.Selected = !record.Selected;
    }
  }

  clickHeader(header): void {
    if (!header.Blocked) {
      header.Selected = !header.Selected;
    }
  }

  removeItem(recordToRemove, index): void {
    if (!this.sectionItems[index].Blocked) {
      this.sectionItems[index].InventoryItems = this.sectionItems[index].InventoryItems.filter(item => item !== recordToRemove);
    }
  }
}
