﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Resident } from '../../../shared/models/passportContext.model';
import { LoaPickerValue } from '../../components/loa-picker/loa-picker.component';
import { SearchRequest, IDictionary } from '../../../shared/models/search.model';
import { Event } from '../../../shared/models/event.model';

@Component({
    selector: 'app-loa-prn',
    templateUrl: './loa-prn.template.html',
    styleUrls: ['./loa-prn.component.less']
})
export class LoaPrnComponent extends BasicPageComponent implements OnInit, OnDestroy {
    loaDateInEdit = '';
    meds$: Observable<any[]>;

    constructor(public sb: AppSandbox) {
        super(sb);
    }

    ngOnInit() {
        const _queries: IDictionary = {};
        _queries['PatientId'] = this.patient.UId;
        _queries['MedicationName'] = ''; // empty name will return ALL meds

        const searchRequest: SearchRequest = {
            Queries: _queries, PageSize: 0, PageNumber: 0
        };

        const asyncEvent: Event = {
            commandName: 'SEARCH_MEDICATIONS',
            data: JSON.stringify(searchRequest),
            type: '', ID: null
        };
        this.meds$ = this.sb.sendAsyncEvent(asyncEvent);
    }

    addToPackage(selection: any[]): void {
        this.sb.addToBasket(selection);
    }

    updateLoa(loa: LoaPickerValue, patient: Resident): void {
        let newLoaValue;
        if (this.loaDateInEdit === 'loaStart') {
            newLoaValue = {
                LoaStart:
                    new Date(loa.date.getTime() + loa.time.getHours() * 60 * 60000 + loa.time.getMinutes() * 60000),
                LoaStartAdminTimeUId: loa.adminTime || null,
                LoaEnd: patient.LOA.LoaEnd,
                LoaEndAdminTimeUId: patient.LOA.LoaEndAdminTimeUId
            };
        } else if (this.loaDateInEdit === 'loaEnd') {
            newLoaValue = {
                LoaStart: patient.LOA.LoaStart,
                LoaStartAdminTimeUId: patient.LOA.LoaStartAdminTimeUId,
                LoaEnd:
                    new Date(loa.date.getTime() + loa.time.getHours() * 60 * 60000 + loa.time.getMinutes() * 60000),
                LoaEndAdminTimeUId: loa.adminTime || null
            };
        }

        if (newLoaValue.LoaStart > newLoaValue.LoaEnd) {
            this.sb.addNotification({
                Id: new Date().getTime(), Date: null, AutoClosed: true, Priority: 1, IsSystem: true,
                Text: 'The LoaEnd date occurs before the LoaStart date', Title: ''
            });
        } else {
            // update current Resident
            this.sb.setPatientDetails(Object.assign(
                {},
                patient,
                { LOA: newLoaValue }
            ));
            // hide popover
            this.onPopoverHidden();
        }
    }
    getLoaStart(loa: any): {value: LoaPickerValue} {
        return {
            value: {
                date: new Date(loa.LoaStart.getTime()),
                time: new Date(loa.LoaStart.getTime()),
                adminTime: loa.LoaStartAdminTimeUId || ''
            }
        };
    }
    getLoaEnd(loa: any): {value: LoaPickerValue} {
        return {
            value: {
                date: new Date(loa.LoaEnd.getTime()),
                time: new Date(loa.LoaEnd.getTime()),
                adminTime: loa.LoaEndAdminTimeUId || ''
            }
        };
    }
    onPopoverHidden(): void {
        this.loaDateInEdit = '';
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }
}
