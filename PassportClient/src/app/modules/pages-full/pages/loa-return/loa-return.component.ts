﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Resident } from '../../../shared/models/passportContext.model';
import { LoaPickerValue } from '../../components/loa-picker/loa-picker.component';

@Component({
    selector: 'app-loa-return',
    templateUrl: './loa-return.template.html',
    styleUrls: ['./loa-return.component.less']
})
export class LoaReturnComponent extends BasicPageComponent implements OnInit, OnDestroy {
    private localSubscription: Subscription = new Subscription();
    adminTimes: Observable<any[]> = this.sb.adminTimes$;
    returnForm: FormGroup;
    currentPatient: Resident;

    constructor(public sb: AppSandbox, private readonly fb: FormBuilder) {
        super(sb);

        this.returnForm = this.fb.group({
            loaEnd: {
                date: '',
                time: '',
                adminTime: ''
            },
            returnMeds: false
        }, {validator: this.formValidator()});

        this.localSubscription.add(
            this.sb.context$.subscribe(ctx => this.getLoaDataFromResidentEntity(ctx.currentResident))
        );
    }

    formValidator() {
        return (group: FormGroup): { [key: string]: string } => {
            if (this.currentPatient === undefined) {
                return { 'currentPatient': 'Current Patient is undefined' };
            }
            const _start = this.currentPatient.LOA ? this.currentPatient.LOA.LoaStart : null;
            const _end = group.controls['loaEnd'].value;
            return _start !== null && _end !== null && _start < _end.date
                ? null
                : { 'dates': 'The LoaEnd date occurs before the LoaStart date'};
        };
    }

    private getLoaDataFromResidentEntity(r: Resident): void {
        this.currentPatient = r;
        this.returnForm.patchValue({
            loaEnd: {
                date: new Date(r.LOA.LoaEnd.getTime()),
                time: new Date(r.LOA.LoaEnd.getTime()),
                adminTime: r.LOA.LoaEndAdminTimeUId || ''
            }
        });
    }

    ngOnInit() {
        this.sb.sendEvent('GET_ADMIN_TIMES');
    }

    returnFromLoa(action: string): void {
        if (this.returnForm.valid) {
            const loaEnd: LoaPickerValue = this.returnForm.value.loaEnd;
            // send to server
            const loaReturnData = {
                PatientUId: this.currentPatient.UId,
                StartDateTime: this.currentPatient.LOA.LoaStart,
                EndDateTime:
                    new Date(loaEnd.date.getTime() + loaEnd.time.getHours() * 60 * 60000 + loaEnd.time.getMinutes() * 60000),
                StartAdminTimeUId: this.currentPatient.LOA.LoaStartAdminTimeUId || null,
                EndAdminTimeUId: loaEnd.adminTime || null
            };

            // TEMP
            this.sb.sendEvent(this.returnForm.value.returnMeds ? 'RETURN_MEDS' : 'HOME');

            // TO-DO: send updated data to server
            // this.sb.sendEvent(action, loaReturnData);

            // update current Resident
            this.sb.setPatientDetails(Object.assign(
                {},
                this.currentPatient,
                {
                    LoaEnd: loaReturnData.EndDateTime,
                    LoaEndAdminTimeUId: loaReturnData.EndAdminTimeUId
                }
            ));
        }
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
