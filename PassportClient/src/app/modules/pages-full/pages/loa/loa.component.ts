﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Resident } from '../../../shared/models/passportContext.model';
import { LoaPickerValue } from '../../components/loa-picker/loa-picker.component';

@Component({
    selector: 'app-loa',
    templateUrl: './loa.template.html',
    styleUrls: ['./loa.component.less']
})
export class LoaComponent extends BasicPageComponent implements OnInit {
    adminTimes: Observable<any[]> = this.sb.adminTimes$;
    loaForm: FormGroup;

    constructor(public sb: AppSandbox, private readonly fb: FormBuilder) {
        super(sb);

        this.loaForm = this.fb.group({
            loaStart: {
                date: new Date(), // today
                time: new Date(1970, 1, 1, 0, 0, 0, 0),
                adminTime: ''
            },
            loaEnd: {
                date: new Date(new Date().getTime() + 86400000), // tomorrow
                time: new Date(1970, 1, 1, 0, 0, 0, 0),
                adminTime: ''
            },
        }, {validator: this.formValidator()});
    }

    formValidator() {
        return (group: FormGroup): {[key: string]: string} => {
            const _start = group.controls['loaStart'].value;
            const _end = group.controls['loaEnd'].value;
            return _start !== null && _end !== null && _start.date < _end.date
                ? null
                : { 'dates': 'The LoaEnd date occurs before the LoaStart date'};
        };
    }

    ngOnInit() {
        this.sb.sendEvent('GET_ADMIN_TIMES');
    }

    continue(action: string, patient: Resident): void {
        if (this.loaForm.valid) {
            const loaStart: LoaPickerValue = this.loaForm.value.loaStart;
            const loaEnd: LoaPickerValue = this.loaForm.value.loaEnd;
            // send to server
            const loaData = {
                PatientUId: patient.UId,
                StartDateTime:
                    new Date(loaStart.date.getTime() + loaStart.time.getHours() * 60 * 60000 + loaStart.time.getMinutes() * 60000),
                EndDateTime:
                    new Date(loaEnd.date.getTime() + loaEnd.time.getHours() * 60 * 60000 + loaEnd.time.getMinutes() * 60000),
                StartAdminTimeUId: loaStart.adminTime || null,
                EndAdminTimeUId: loaEnd.adminTime || null
            };
            this.sb.sendEvent(action, loaData);

            // update current Resident
            this.sb.setPatientDetails(Object.assign(
                {},
                patient,
                {
                    LOA: {
                        LoaStart: loaData.StartDateTime,
                        LoaStartAdminTimeUId: loaData.StartAdminTimeUId,
                        LoaEnd: loaData.EndDateTime,
                        LoaEndAdminTimeUId: loaData.EndAdminTimeUId
                    }
                }
            ));
        }
    }
}
