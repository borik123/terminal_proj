﻿import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { timer } from 'rxjs/observable/timer';

import { AppSandbox } from '../../../../app.sandbox';

import { IMedicationMatrix, Legend } from '../../../shared/models/medicationMatrix.model';
import { MaintenanceDialog } from '../../../shared/models/maintenanceDialog.model';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-load-cartriges',
    templateUrl: './load-cartriges.template.html',
    styleUrls: ['./load-cartriges.component.less']
})
export class LoadCartrigesComponent extends BasicPageComponent implements OnInit, OnDestroy {
    private localSubscription: Subscription = new Subscription();
    dialogRef: BsModalRef = null;
    currentDialog: MaintenanceDialog = null;
    searchForm: FormGroup;

    selectedBlockPos: string[] = [];
    selectionMode: string = null;
    grid: IMedicationMatrix[] = [];
    lastSelectedMedication: IMedicationMatrix = null;
    columns: number[] = Array(11).fill(1);
    rows: number[] = Array(10).fill(1);
    private collNames: string[] = [
        ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ];

    constructor(public sb: AppSandbox, private readonly _fb: FormBuilder, private modalService: BsModalService) {
        super(sb);

        this.searchForm = this._fb.group({
            query: ['']
        });

        this.localSubscription.add(
            this.sb.medicationMatrix$.subscribe(mm => this.grid = mm)
        );
    }

    ngOnInit() {
        this.getMedMatrix(); // get data from server
    }

    private getMedMatrix(): void {
        this.sb.sendEvent('GET_MEDICATION_MATRIX');
    }

    startLoad(template: TemplateRef<any>) {
        if (this.selectedBlockPos.length === 0) {
            return;
        }

        // open 'Open Door' dialog
        this.currentDialog = this.page.OpenDoorModal;

        this.dialogRef = this.modalService.show(template, {
            ignoreBackdropClick: true,
            class: 'maintenance-loading-dialog'
        });

        timer(1400).subscribe(x => {

            this.currentDialog = this.page.ReplaceModal;
        });

        timer(3600).subscribe(x => {

            // close popup dialog
            if (this.dialogRef != null) {
                this.dialogRef.hide();
                this.dialogRef = null;
            }

            // change Mode
            this.selectionMode = 'LOAD_PREVIEW';
        });

        timer(5000).subscribe(x => {

            // clear all
            this.selectionMode = null;
            this.selectedBlockPos = [];
            this.lastSelectedMedication = null;
        });
    }
    actionBtnClicked(action?: string) {

        if (action === 'CLEAR_ALL' && this.selectionMode === 'SELECT_MODE') {

            this.selectionMode = null;
            const lowSelection =
                this.grid.filter((x: IMedicationMatrix) =>
                    (x.Legend === Legend.CONTAINER_BELOW_CARTRIDGE_EMPTY || x.Legend === Legend.CONTAINER_BELOW_CARTRIDGE_FULL)
                    && this.isLocationSelected(x.Location));

            lowSelection.forEach((x: IMedicationMatrix) => this.selectLocation(x.Location)); // this will UNselect
        } else if (action === 'LOAD_PREVIEW' && this.selectionMode === 'LOAD_PREVIEW') {

            this.selectionMode = null; // TO-DO :: set previous value?
        } else if (action === 'SELECT_MODE' || action === 'LOAD_PREVIEW') {

            this.selectionMode = action;
        } else if (action === 'SELECT_LOW_MODE' && this.selectionMode === 'SELECT_MODE') {

            this.selectionMode = null;
            const lowSelection =
                this.grid.filter((x: IMedicationMatrix) =>
                    x.Legend === Legend.CONTAINER_BELOW_CARTRIDGE_EMPTY || x.Legend === Legend.CONTAINER_BELOW_CARTRIDGE_FULL);

            lowSelection.forEach((x: IMedicationMatrix) => this.selectLocation(x.Location));
        } else if (action === 'RESCAN') {

            this.sb.sendEvent('CONTROLLER_SCAN');
        }
    }

    get MatrixColumnTitles(): string[] {
        return this.collNames.slice(0, this.columns.length);
    }

    private get isPreviewMode(): boolean {
        return this.selectionMode === 'LOAD_PREVIEW';
    }

    private _getLocation = (row: number, col: number): string => `${row.toString().padStart(2, '0')}${col.toString().padStart(2, '0')}`;

    public selectBlock(row: number, col: number): void {

        if (this.selectionMode !== 'SELECT_MODE' && this.selectionMode !== 'SELECT_LOW_MODE') {
            return;
        }

        const _loc = this._getLocation(row, col);
        this.selectLocation(_loc);
    }
    public selectLocation(location: string): void {

        const _itm = this.findItemInGrid(location, this.isPreviewMode);
        if (_itm) {
            const _index = this.selectedBlockPos.indexOf(location);
            if (_index >= 0) {
                this.selectedBlockPos.splice(_index, 1);
            } else {
                this.selectedBlockPos.push(location)
            }
            this.lastSelectedMedication = this.selectedBlockPos.length
                ? this.findItemInGrid(this.selectedBlockPos[this.selectedBlockPos.length - 1], false)
                : null;
        }
    }

    public matrixCellValue(row: number, col: number): string {
        if (col === 0) {
            return row.toString();
        }

        const _itm = this.findItemInGrid(this._getLocation(row, col), this.isPreviewMode);
        return _itm ? _itm.ContainerQuantity.toString() : '';
    }
    public matrixCellClass(row: number, col: number): string {
        if (col === 0) {
            return ''; // skip first column - there is Row Index inside
        }

        const _location: string = this._getLocation(row, col);
        const _itm = this.findItemInGrid(_location, this.isPreviewMode);
        if (_itm === undefined) {
            return '';
        }

        let _class = '';
        if (this.isPreviewMode || _itm.Legend === Legend.SELECTED) {
            _class = 'legend9';
        }
        else if (_itm.Legend === Legend.CARTRIDGE_FULL) {
            _class = 'legend1';
        }
        else if (_itm.Legend === Legend.CARTRIDGE_FULL_LOCKED) {
            _class = 'legend2';
        }
        else if (_itm.Legend === Legend.CONTAINER_ABOVE_CARTRIDGE_EMPTY) {
            _class = 'legend3';
        }
        else if (_itm.Legend === Legend.CONTAINER_ABOVE_CARTRIDGE_FULL) {
            _class = 'legend4';
        }
        else if (_itm.Legend === Legend.CONTAINER_BELOW_CARTRIDGE_EMPTY) {
            _class = 'legend5';
        }
        else if (_itm.Legend === Legend.CONTAINER_BELOW_CARTRIDGE_FULL) {
            _class = 'legend6';
        }
        else if (_itm.Legend === Legend.LOCATION_LOCKED) {
            _class = 'legend7';
        }
        else if (_itm.Legend === Legend.CARTRIDGE_LOCKED) {
            _class = 'legend8';
        }

        if (this.selectedBlockPos.length && !this.isPreviewMode && this.isLocationSelected(_location)) {
            _class += ' selected';
        }
        return _class;
    }

    private findItemInGrid(location: string, isPreview: boolean): IMedicationMatrix {
        return isPreview
            ? this.grid.find((x: IMedicationMatrix) => x.Location == location && this.isLocationSelected(location))
            : this.grid.find((x: IMedicationMatrix) => x.Location == location);
    }
    private isLocationSelected(location: string): boolean {
        return this.selectedBlockPos.indexOf(location) >= 0;
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
