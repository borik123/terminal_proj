﻿import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-med-tote',
    templateUrl: './med-tote.template.html',
    styleUrls: ['./med-tote.component.less']
})
export class MedToteComponent extends BasicPageComponent implements OnInit, OnDestroy {
    totes$ = this.sb.sendAsyncEvent({ commandName: 'GET_MED_TOTES' });
    selectedIndex = -1;
    selectedTote: any;
    dialogRef: BsModalRef = null;

    constructor(public sb: AppSandbox, private modalService: BsModalService) {
        super(sb);
    }

    ngOnInit() {
    }

    toteClicked(index: number, tote: any, tmpl: TemplateRef<any>): void {
        this.selectedIndex = (this.selectedIndex === index) ? -1 : index;
        if (this.selectedIndex === -1) {
            this.selectedTote = undefined;
        } else {
            this.selectedTote = tote;
            this.dialogRef = this.modalService.show(tmpl, {
                ignoreBackdropClick: false,
                class: 'maintenance-loading-dialog'
            });
        }
    }

    ngOnDestroy() {
        if (this.dialogRef != null) {
            this.dialogRef.hide();
            this.dialogRef = null;
        }

        super.ngOnDestroy();
    }
}
