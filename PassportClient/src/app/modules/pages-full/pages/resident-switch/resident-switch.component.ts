﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-resident-switch',
    templateUrl: './resident-switch.template.html',
    styleUrls: ['./resident-switch.component.less']
})
export class ResidentSwitchComponent extends BasicPageComponent {

}
