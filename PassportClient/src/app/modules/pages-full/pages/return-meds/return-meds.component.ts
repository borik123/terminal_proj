﻿import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { ReturnMedication } from '../../../shared/models/returnMed.model';
import { DatePickerModalComponent } from '../../components/date-picker-modal/date-picker-modal.component';

interface Reason {
    UId: string;
    Name: string;
}
interface AdminTime {
    UId: string;
    Name: string;
}
interface ReturnMedicationRow {
    AdminDate: Date;
    AdminTime?: AdminTime;
    Medications?: ReturnMedication[];
}

@Component({
    selector: 'app-return-meds',
    templateUrl: './return-meds.template.html',
    styleUrls: ['./return-meds.component.less']
})
export class ReturnMedsComponent extends BasicPageComponent implements OnInit {
    private modalSubscription: Subscription = new Subscription();

    confirmationState = false;
    returnForm: FormGroup;
    selectedDate: Date = undefined;
    returnMedications: ReturnMedicationRow[] = [];
    adminTimes: AdminTime[] = [
        { UId: '', Name: 'All'},
        { UId: '00000000-0000-0000-0000-000000000000', Name: 'PRN' }
    ];
    reasons: Reason[] = [
        { UId: '00000001-0000-0000-0000-000000000000', Name: 'Order Discontinued' },
        { UId: '00000002-0000-0000-0000-000000000000', Name: 'Discharged' },
        { UId: '00000003-0000-0000-0000-000000000000', Name: 'Deceased' },
        { UId: '00000004-0000-0000-0000-000000000000', Name: 'In Hospital' },
        { UId: '00000005-0000-0000-0000-000000000000', Name: 'ABT Complete' },
        { UId: '00000006-0000-0000-0000-000000000000', Name: 'Already Packaged' }
    ];
    meds: any[] = [
        { MedicationId: 1, MedicationName: '1 dose of Codeine TAB 150MG', SigDescription: 'Q6H', MedID: '040404', Quantity: 1, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 2, ImageSrc: 'med2.png', MedicationName: '1 dose of Simvastatin 20MG', SigDescription: 'Q6H', MedID: '010101', Quantity: 2, IsControlledMed: false, AdminTimeName: 'Night', AdminTimeId: '00000003-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 3, ImageSrc: 'med3.png', MedicationName: '2 dose of Acetamin TAB 300MG', SigDescription: 'Q6H', MedID: '010202', Quantity: 3, IsControlledMed: true, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 4, ImageSrc: 'med4.png', MedicationName: '1 dose of Prilosec TAB 40MG', SigDescription: 'BID', MedID: '010101', Quantity: 4, IsControlledMed: false, AdminTimeName: 'PRN', AdminTimeId: '00000000-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 5, ImageSrc: 'med5.png', MedicationName: '3 dose of Lexarpo TAB 50MG', SigDescription: 'Q6H', MedID: '030301', Quantity: 5, IsControlledMed: false, AdminTimeName: 'Day', AdminTimeId: '00000001-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 6, ImageSrc: 'med1.png', MedicationName: '2 dose of Lexarpo TAB 60MG', SigDescription: 'Q6H', MedID: '040401', Quantity: 4, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 7, ImageSrc: 'med2.png', MedicationName: '1 dose of Acetamin TAB 70MG', SigDescription: 'ACB', MedID: '050501', Quantity: 3, IsControlledMed: true, AdminTimeName: 'Night', AdminTimeId: '00000003-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 8, ImageSrc: 'med3.png', MedicationName: '3 dose of Prilosec TAB 80MG', SigDescription: 'QD', MedID: '070101', Quantity: 2, IsControlledMed: false, AdminTimeName: 'Night', AdminTimeId: '00000003-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 9, ImageSrc: 'med4.png', MedicationName: '1 dose of Codeine TAB 90MG', SigDescription: 'Q6H', MedID: '060101', Quantity: 1, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 10, ImageSrc: 'med1.png', MedicationName: '2 dose of Codeine TAB 10MG', SigDescription: 'BID', MedID: '080101', Quantity: 2, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 11, ImageSrc: 'med2.png', MedicationName: '3 dose of Codeine TAB 11MG', SigDescription: 'BID', MedID: '090101', Quantity: 11, IsControlledMed: true, AdminTimeName: 'PRN', AdminTimeId: '00000000-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 12, ImageSrc: 'med3.png', MedicationName: '1 dose of Codeine TAB 12MG', SigDescription: 'BID', MedID: '010708', Quantity: 10, IsControlledMed: false, AdminTimeName: 'Day', AdminTimeId: '00000001-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 13, ImageSrc: 'med4.png', MedicationName: '3 dose of Codeine TAB 13MG', SigDescription: 'BID', MedID: '010807', Quantity: 9, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 14, ImageSrc: 'med1.png', MedicationName: '2 dose of Codeine TAB 14MG', SigDescription: 'BID', MedID: '010906', Quantity: 8, IsControlledMed: false, AdminTimeName: 'PRN', AdminTimeId: '00000000-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 15, ImageSrc: 'med3.png', MedicationName: '3 dose of Codeine TAB 15MG', SigDescription: 'Q6H', MedID: '010205', Quantity: 7, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 16, ImageSrc: 'med2.png', MedicationName: '1 dose of Codeine TAB 16MG', SigDescription: 'BID', MedID: '010302', Quantity: 6, IsControlledMed: true, AdminTimeName: 'Day', AdminTimeId: '00000001-0000-0000-0000-000000000000', PackagedAt: new Date() },
        { MedicationId: 17, ImageSrc: 'med1.png', MedicationName: '2 dose of Codeine TAB 17MG', SigDescription: 'BID', MedID: '010403', Quantity: 5, IsControlledMed: false, AdminTimeName: 'Evening', AdminTimeId: '00000002-0000-0000-0000-000000000000', PackagedAt: new Date() }
    ];
    selectedMeds: {ControlledBin: any[], ReturnBin: any[]};

    constructor(public sb: AppSandbox, private fb: FormBuilder, private modalService: BsModalService) {
        super(sb);
    }

    ngOnInit() {
        const atSubcription: Subscription = this.sb.adminTimes$.subscribe((at: any[]) => {
            this.adminTimes = [...this.adminTimes, ...at];
            atSubcription.unsubscribe();
        });
        this.sb.sendEvent('GET_ADMIN_TIMES');

        this.returnForm = this.fb.group({
            medRows: this.fb.array([])
        });
    }
    createMedRow(date: Date, reason?: Reason): FormGroup {
        return this.fb.group({
            adminDate: [date, [Validators.required]],
            adminTime: this.adminTimes[0].UId,
            medication: [null, [Validators.required]],
            reason: [reason, [Validators.required]]
        });
    }

    addMedRow(): void {
        const ctrl = this.returnForm.get('medRows') as FormArray;
        const reason = ctrl.length ? (ctrl.controls[0] as FormGroup).controls['reason'].value : null;
        ctrl.push(this.createMedRow(this.selectedDate, reason));
    }
    removeMedRow(i: number): void {
        const ctrl = this.returnForm.get('medRows') as FormArray;
        ctrl.removeAt(i);
    }

    selectDate(action?: string): void {
        if (action) {
            const now = new Date();
            if (action.match(/^[0-9]+d{1}$/gi)) {
                now.setTime(now.getTime() - (86400000 * parseInt(action.replace(/d/gi, ''), 10)));
            }
            this.selectedDate = now;
            this.addMedRow();
        } else {
            // Open Modal Dialog
            const dateDialogRef = this.modalService.show(DatePickerModalComponent, {
                initialState: {
                    datePickerModalVM: this.page.DatePickerModal,
                    assetsFolderPath: this.assetsFolderPath
                },
                ignoreBackdropClick: false,
                class: 'date-picker-dialog'
            });

            // subscribe to modal Close event
            this.modalSubscription = this.modalService.onHidden.subscribe(() => {
                if (dateDialogRef.content.selectedDate) {
                    this.selectedDate = dateDialogRef.content.selectedDate;
                    this.addMedRow();
                }
                this.modalSubscription.unsubscribe();
            });
        }
    }

    goToSubPage3(): void {
        if (!this.returnForm.valid) {
            return;
        }

        const _formValue = this.returnForm.value;
        this.selectedMeds = {
            ControlledBin: _formValue.medRows.filter(o => o.medication.IsControlledMed),
            ReturnBin: _formValue.medRows.filter(o => !o.medication.IsControlledMed)
        };

        this.confirmationState = true;
    }

    get ROWS(): any[] {
        return (this.returnForm.controls['medRows'] as FormArray).controls;
    }
    getAdminTimeText(atId: string): string {
        const _atColl: AdminTime[] = this.adminTimes.filter(at => at.UId === atId);
        return _atColl.length ? _atColl[0].Name : '-none-';
    }
}
