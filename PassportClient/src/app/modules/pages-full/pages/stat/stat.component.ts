﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, RequiredValidator, FormArray , AbstractControl} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Event } from '../../../shared/models/event.model';

@Component({
    selector: 'app-stat',
    templateUrl: './stat.template.html',
    styleUrls: ['./stat.component.less']
})
export class StatComponent extends BasicPageComponent implements OnInit {
    doctors: any[] = [];
    meds: any[] = [];
    statForm: FormGroup;

    constructor(public sb: AppSandbox, private readonly fb: FormBuilder) {
        super(sb);

        this.statForm = this.fb.group({
            rows: this.fb.array([this.addRow()])
        });
    }

    addRow(): FormGroup {
        return this.fb.group({
            doctor: ['', [RequiredValidator]],
            medication: ['', [RequiredValidator]]
        });
    }
    get ROWS(): any[] {
        return (this.statForm.controls['rows'] as FormArray).controls;
    }
    addMedRow(): void {
        const ctrl = this.statForm.get('rows') as FormArray;
        ctrl.push(this.addRow());
    }
    removeMedRow(i: number): void {
        const ctrl = this.statForm.get('rows') as FormArray;
        ctrl.removeAt(i);
    }
    quantityMinus(i: any): void {
        i.get('medication').value.Quantity--;
    }
    quantityPlus(i: any): void {
        i.get('medication').value.Quantity++;
    }
    clearMedication(i: AbstractControl): void {
        i.patchValue({medication: null});
    }

    ngOnInit() {
        const doctorsAsyncEvent: Event = {
            commandName: 'GET_DOCTORS', data: null, type: '', ID: null
        };
        const doctorsSubcription: Subscription = this.sb.sendAsyncEvent(doctorsAsyncEvent).subscribe((data: any[]) => {
            this.doctors = data;
            doctorsSubcription.unsubscribe();
        });

        const medsAsyncEvent: Event = {
            commandName: 'SEARCH_STAT_MEDICATIONS',
            data: JSON.stringify({
                Queries: {
                    'PatientId': this.patient.UId
                }, PageSize: 0, PageNumber: 0
            }),
            type: '', ID: null
        };
        const medsSubcription: Subscription = this.sb.sendAsyncEvent(medsAsyncEvent).subscribe((data: any[]) => {
            this.meds = data;
            medsSubcription.unsubscribe();
        });
    }

    addToPackage(action: string): void {
        if (this.statForm.valid) {

        }
    }
}
