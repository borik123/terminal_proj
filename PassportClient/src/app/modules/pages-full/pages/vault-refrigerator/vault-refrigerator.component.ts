import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ToggleButtonElement } from '../../../shared/models/contentElems.model';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
@Component({
  selector: 'app-vault-refrigerator',
  templateUrl: './vault-refrigerator.template.html',
  styleUrls: ['./vault-refrigerator.component.less']
})
export class VaultRefrigeratorComponent extends BasicPageComponent implements OnInit, OnDestroy {
  @Input() toggle: ToggleButtonElement;

  private localSubscription: Subscription = new Subscription();
  public medRecords: any;
  public gridItems: any;

  constructor(public sb: AppSandbox) {
    super(sb);

    this.toggle = this.page.Select;

    this.gridItems = this.page.GridItems;
    this.localSubscription.add(
      this.sb.vaultRecords$.subscribe(mm => {
        this.medRecords = mm;
      })
    );
  }

  ngOnInit() {
    this.getRecords();
  }

  ngOnDestroy() {
    if (this.localSubscription) {
      this.localSubscription.unsubscribe();
    }
    super.ngOnDestroy();
  }

  private getRecords(): void {
    this.sb.sendEvent('GET_VAULT_REFRIGERATOR_RECORDS');
  }

  actionBtnClicked(btn, index): void {
    btn.Clicked = btn.Clicked ? false : true;
    this.gridItems[index].Blocked = btn.Clicked;
  }

  selectItem(record, index): void {
    if (!this.gridItems[index].Blocked) {
      record.Selected = record.Selected ? false : true;
    }
  }

  removeItem(recordToRemove, index): void {
    if (!this.gridItems[index].Blocked) {
      this.medRecords = this.medRecords.filter(item => item !== recordToRemove);
    }
  }

  filter(key: string): any {
    const result = this.medRecords.filter(x => x.RecordType === key);
    return result;
  }
}
