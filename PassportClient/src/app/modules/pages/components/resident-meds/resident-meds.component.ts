import {Component, Input, ViewChild, ElementRef} from '@angular/core';

interface IMedsDataSource {
  data: string[][];
  headers: string[];
  widths: number[];
}

@Component({
  selector: 'app-resident-meds',
  templateUrl: './resident-meds.template.html',
  styleUrls: ['./resident-meds.component.less']
})
export class ResidentMedsComponent {
  @Input() imageColumnIndex = -1;
  @Input() dataSource: IMedsDataSource = {
    data: [],
    headers: [],
    widths: []
  };

  @ViewChild('scrollHead') headDiv: ElementRef;
  @ViewChild('scrollHeadInner') headInnerDiv: ElementRef;
  @ViewChild('scrollHeadRow') headRow: ElementRef;
  @ViewChild('scrollBodyRow') bodyRow: ElementRef;

  bodyScroll(ev: any): void {
    this.headDiv.nativeElement.scrollLeft = ev.target.scrollLeft;
  }

  updateCellWidth() {
    if (this.bodyRow) {
      const rowWidth: number = this.bodyRow.nativeElement.clientWidth;
      this.headInnerDiv.nativeElement.style.width = `${rowWidth}px`;
      this.headInnerDiv.nativeElement.querySelector('table').style.width = `${rowWidth}px`;

      const hiddenHeaderCells: any[] = this.bodyRow.nativeElement.querySelectorAll('th');
      this.headRow.nativeElement.querySelectorAll('th').forEach((th: any, index) => {
        th.style.width = `${hiddenHeaderCells[index].clientWidth}px`;
      });
    }
  }
}
