import {NgModule} from '@angular/core';
import {SharedModule} from '../../modules/shared/shared.module';
/* COMPONENTS */
import {ResidentMedsComponent} from './components/resident-meds/resident-meds.component';
/* PAGES */
import {IndexComponent} from './pages/index/index.component';
import {IntroComponent} from './pages/intro/intro.component';
import {LogonComponent} from './pages/logon/logon.component';
import {SearchPageComponent} from './pages/search/search.component';
import {PrnPageComponent} from './pages/prn-page/prn-page.component';
import {PrnDispenseComponent} from './pages/prn-dispense/prn-dispense.component';
import {MaintenanceComponent} from './pages/maintenance/maintenance.component';
import {LoadEnvelopesComponent} from './pages/load-envelopes/load-envelopes.component';
import {MechanicalToolsComponent} from './pages/mechanical-tools/mechanical-tools.component';
import {PrinterRibbonComponent} from './pages/printer-ribbon/printer-ribbon.component';
import {ConfPageComponent} from './pages/conf-page/conf-page.component';
import {ChangePasswordComponent} from './pages/change-pass/change-pass.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {FolioComponent} from './pages/folio/folio.component';
import {ResidentScheduleComponent} from './pages/resident-schedule/resident-schedule.component';

@NgModule({
    declarations: [
        IndexComponent,
        IntroComponent,
        LogonComponent,
        SearchPageComponent,
        PrnPageComponent,
        PrnDispenseComponent,
        ProfileComponent,
        MaintenanceComponent,
        LoadEnvelopesComponent,
        MechanicalToolsComponent,
        ChangePasswordComponent,
        FolioComponent,
        ResidentScheduleComponent,
        PrinterRibbonComponent,
        ConfPageComponent,
        ResidentMedsComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        IndexComponent,
        IntroComponent,
        LogonComponent,
        SearchPageComponent,
        PrnPageComponent,
        PrnDispenseComponent,
        ProfileComponent,
        MaintenanceComponent,
        LoadEnvelopesComponent,
        MechanicalToolsComponent,
        PrinterRibbonComponent,
        ConfPageComponent,
        ChangePasswordComponent,
        FolioComponent,
        ResidentScheduleComponent
    ]
})
export class PagesModule {
}
