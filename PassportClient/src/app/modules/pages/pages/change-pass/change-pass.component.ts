﻿import { Component, ViewChild, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { trigger, state, animate, style, transition } from '@angular/animations';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operators';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { AuthenticationResult } from '../../../shared/models/search.model';
import { Command } from '../../../shared/models/command.model';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.template.html',
  styleUrls: ['./change-pass.component.less'],
  animations: [
      trigger('routerTransition', [
          state('void', style({ opacity: '1' })),
          state('*', style({ opacity: '1' })),
          transition(':enter', [ // void => *
              style({ opacity: '0.2' }),
              animate('0.7s ease-in-out', style({ opacity: '1' }))
          ]),
          transition(':leave', [ // * => void
              style({ opacity: '1' }),
              animate('0.7s ease-in-out', style({ opacity: '0.2' }))
          ])
      ])
  ]
})
export class ChangePasswordComponent extends BasicPageComponent implements OnInit, OnDestroy {
  @ViewChild('sound') audio: ElementRef;

  public resetForm: FormGroup;
  public validationError: string;
  public invalidControl: string;

  private dropEffect: Subject<string>;
  private localSubscription: Subscription = new Subscription();

  constructor(public sb: AppSandbox, private readonly _fb: FormBuilder) {
    super(sb);

    this.resetForm = this._fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    });
    this.validationError = '';

    this.dropEffect = new Subject<string>();
    this.dropEffect
      .pipe(debounceTime(500))
      .subscribe(() => this.invalidControl = null);

    this.localSubscription.add(
        this.sb.login$.subscribe(cmd => this.loginHandler(cmd))
    );
    this.localSubscription.add(
        this.sb.gotSolt$.subscribe(cmd => this.gotSoltHandler(cmd))
    );
  }

  ngOnInit() {
      this.sb.clearPassportContext();
  }

  public get oldPassword(): FormControl {
    return this.resetForm.get('oldPassword') as FormControl;
  }
  public get newPassword(): FormControl {
    return this.resetForm.get('newPassword') as FormControl;
  }
  public get repeatPassword(): FormControl {
    return this.resetForm.get('repeatPassword') as FormControl;
  }

  reset(cmdName: string): void {
    if (this.oldPassword.invalid) {
      this.showError('oldPassword', this.page.OldPasswordField.ErrorText.Value);
      return;
    }
    if (this.newPassword.invalid) {
        this.showError('newPassword', this.page.NewPasswordField.ErrorText.Value);
        return;
    }
    if (this.repeatPassword.invalid || this.newPassword.value !== this.repeatPassword.value) {
        this.showError('repeatPassword', this.page.RepeatPasswordField.ErrorText.Value);
        return;
    }

    this.sendClickEvent(cmdName, this.resetForm.value);
  }

  public hasErrors(): boolean {
    return this.validationError.length > 0;
  }

  gotSoltHandler(cmd: Command): void {
      // send LOGIN again to Server
      this.sb.sendEvent(this.page.Login.Action,
          (<any>Object).assign(
              {},
              this.resetForm.value,
              { salt: JSON.parse(cmd.Message) }
          ));
  }
  loginHandler(cmd: Command): void {
    const formVal = this.resetForm.value;
    if (formVal.user === null || formVal.user === undefined) {
        throw new Error('[LOGON PAGE] User has not been selected');
    }

    const _result: AuthenticationResult = JSON.parse(cmd.Message);
    if (_result === AuthenticationResult.Success) {
        // save authorized user to Store
        this.sb.setUser(formVal.user);
        // ask server for Cart items
        this.sb.sendEvent('GET_DISPENSES_BY_USER', formVal.user.UserID);

    } else if (_result === AuthenticationResult.InvalidPassword || _result === AuthenticationResult.InvalidUserName) {
        // show error
        this.showError('password', this.page.PasswordField.ErrorText.Value);
        this.showError('user', this.page.UserNameField.ErrorText.Value);

    } else if (_result === AuthenticationResult.InvalidFingerprint) {
        // show Fingerpring error
        this.showError('fingerprint', this.page.FingerprintField.ErrorText.Value);
    }
  }

  private showError(invalidControl: string, error: string): void {
    this.validationError = error;
    this.invalidControl = invalidControl;
    this.audio.nativeElement.play();
    this.dropEffect.next(invalidControl);
  }

  ngOnDestroy() {
      if (this.localSubscription) {
          this.localSubscription.unsubscribe();
      }
      super.ngOnDestroy();
  }
}
