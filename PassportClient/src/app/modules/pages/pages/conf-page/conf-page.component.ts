﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { filter } from 'rxjs/operators';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-conf-page',
    templateUrl: './conf-page.template.html',
    styleUrls: ['./conf-page.component.less']
})
export class ConfPageComponent extends BasicPageComponent implements OnInit, OnDestroy {
    private localSubscription: Subscription = new Subscription();
    attributes: any[] = [];

    constructor(public sb: AppSandbox) {

        super(sb);

        this.localSubscription.add(
            this.sb.configPageAttributes$.subscribe(at => this.attributes = at)
        );
    }

    ngOnInit() {
        if (this.page.SubPageEventId) {
            // load data from server
            this.sb.sendEvent(this.page.SubPageEventId);
        } else {
            throw new Error(`[CONF-PAGE] Cannot load configuration data because page attribute 'SubPageEventId' is empty`);
        }
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
