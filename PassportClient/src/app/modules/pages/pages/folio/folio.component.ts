﻿import { Component } from '@angular/core';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { ButtonElement } from '../../../shared/models/contentElems.model';

@Component({
    selector: 'app-folio',
    templateUrl: './folio.template.html',
    styleUrls: ['./folio.component.less']
})
export class FolioComponent extends BasicPageComponent {

    constructor(public sb: AppSandbox) {
        super(sb);
    }

    getVisibleButtons(residentStatus: string): ButtonElement[] {
        return this.page.Buttons.filter((btn: ButtonElement) =>
            btn.Action !== 'LOA_RETURN' || (btn.Action === 'LOA_RETURN' && residentStatus.toUpperCase() === 'LOA'));
    }
}
