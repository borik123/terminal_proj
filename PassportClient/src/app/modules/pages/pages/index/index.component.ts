import { Component, OnInit } from '@angular/core';
import { AppSandbox } from '../../../../app.sandbox';

@Component({
  selector: 'app-index',
  template: `
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center">
                <img [src]="getLoadingGifSrc()">
            </div>
        </div>
    </div>
  `
})
export class IndexComponent implements OnInit {
    constructor(private sb: AppSandbox) { }

    ngOnInit() {
        this.sb.sendEvent('LOGON');
    }

    getLoadingGifSrc(): string {
        return `${this.sb.assetsFolderPath}med_load_temp.gif`;
    }
}
