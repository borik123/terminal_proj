﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-intro',
    templateUrl: './intro.template.html',
    styleUrls: ['./intro.component.less']
})
export class IntroComponent extends BasicPageComponent {

}
