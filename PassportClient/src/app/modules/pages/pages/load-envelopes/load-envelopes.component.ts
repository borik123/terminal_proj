﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-load-envelopes',
    templateUrl: './load-envelopes.template.html',
    styleUrls: ['./load-envelopes.component.less']
})
export class LoadEnvelopesComponent extends BasicPageComponent {

}
