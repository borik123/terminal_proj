﻿import { Component, TemplateRef, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { timer } from 'rxjs/observable/timer';
import { Subscription } from 'rxjs/Subscription';
import { tap } from 'rxjs/operators';

import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { MaintenanceDialog } from '../../../shared/models/maintenanceDialog.model';
import { AppSandbox } from '../../../../app.sandbox';

@Component({
    selector: 'app-maintenance',
    templateUrl: './maintenance.template.html',
    styleUrls: ['./maintenance.component.less']
})
export class MaintenanceComponent extends BasicPageComponent implements OnDestroy {
    dialogRef: BsModalRef = null;
    currentDialog: MaintenanceDialog = null;
    private middleDialog: MaintenanceDialog = null;
    private localSubscription: Subscription = new Subscription();

    constructor(public sb: AppSandbox, private modalService: BsModalService) {
        super(sb);

        this.localSubscription.add(
            this.sb.doorOpened$.subscribe(() => this.currentDialog = this.middleDialog)
        );

        this.localSubscription.add(
            this.sb.doorClosed$.subscribe(() => this.doorClosedHandler())
        );
    }

    doorClosedHandler(): void {
        this.currentDialog = this.page.DoneModal;

        // close popup dialog
        timer(3000).subscribe(x => {
            if (this.dialogRef != null) {
                this.dialogRef.hide();
                this.dialogRef = null;
            }
        });
    }

    btnClick(action: string, template: TemplateRef<any>): void {

        const modals = this.page.Modals as Array<MaintenanceDialog>;
        const modalsByAction = modals.filter(m => m.Button.Action === action);

        if (modalsByAction.length) {

            this.sendClickEvent('CONTROLLER_OPEN_DOOR');
            this.middleDialog = modalsByAction[0];

            // open 'Open Door' dialog
            this.currentDialog = this.page.OpenDoorModal;

            this.dialogRef = this.modalService.show(template, {
                ignoreBackdropClick: !this.sb.isEditMode,
                class: 'maintenance-loading-dialog'
            });
        } else {

            this.sendClickEvent(action);
        }
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
