﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-mechanical-tools',
    templateUrl: './mechanical-tools.template.html',
    styleUrls: ['./mechanical-tools.component.less']
})
export class MechanicalToolsComponent extends BasicPageComponent {

}
