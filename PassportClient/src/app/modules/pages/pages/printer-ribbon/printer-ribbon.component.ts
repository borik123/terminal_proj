﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-printer-ribbon',
    templateUrl: './printer-ribbon.template.html',
    styleUrls: ['./printer-ribbon.component.less']
})
export class PrinterRibbonComponent extends BasicPageComponent {

}
