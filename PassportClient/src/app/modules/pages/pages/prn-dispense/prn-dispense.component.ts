﻿import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { timer } from 'rxjs/observable/timer';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { PackagePreparingComponent } from '../../../shared/components/package-preparing/package-preparing.component';
import { PassportContext } from '../../../shared/models/passportContext.model';
import { Notification } from '../../../shared/models/notification.model';

@Component({
    selector: 'app-prn-dispense',
    templateUrl: './prn-dispense.template.html',
    styleUrls: ['./prn-dispense.component.less']
})
export class PrnDispenseComponent extends BasicPageComponent implements OnDestroy {
    modalSubscription: Subscription = new Subscription();
    dialogRef: BsModalRef = null;
    context: PassportContext;
    private localSubscription: Subscription = new Subscription();

    constructor(private modalService: BsModalService, public sb: AppSandbox) {
        super(sb);

        this.localSubscription.add(
            this.sb.context$.subscribe(ctx => this.context = ctx)
        );
    }

    prepare(): void {

        const dispenseData = {
            Packages: this.context.cart.filter(c => c.PackageId == null)
        };

        if (dispenseData.Packages.length === 0) {

            // show notification
            const n: Notification = {
                Id: new Date().getTime(), Date: new Date(),
                Title: '',
                Text: this.page.CartEmptyMessage ? this.page.CartEmptyMessage.Value : 'Basket is empty',
                IsSystem: true, AutoClosed: true,
                Priority: 2 // normal
            };
            this.sb.addNotification(n);

            // stop processing
            return;
        }

        this.dialogRef = this.modalService.show(PackagePreparingComponent, {
            initialState: {
                prepareBtn: this.page.Prepare,
                assetsFolderPath: this.assetsFolderPath
            },
            ignoreBackdropClick: true,
            class: 'pill-loading-dialog'
        });

        this.sendClickEvent('DISPENSE', dispenseData);

        // clear Cart
        dispenseData.Packages.forEach(d => this.sb.removeFromBasket({ residentId: d.Patient.UId }));

        // close modal after a specific amount of time and go to the next page
        const periodInMsec = this.page.TimeoutInSec ? this.page.TimeoutInSec * 1000 : 5000;
        timer(periodInMsec).subscribe(x => {

            // close popup dialog
            if (this.dialogRef != null) {
                this.dialogRef.hide();
                this.dialogRef = null;
            }

            // go next
            this.sendClickEvent(this.page.Dispense.Action);
        });
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
