﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { SearchRequest, IDictionary } from '../../../shared/models/search.model';
import { Event } from '../../../shared/models/event.model';

@Component({
    selector: 'app-prn-page',
    templateUrl: './prn-page.template.html',
    styleUrls: ['./prn-page.component.less']
})
export class PrnPageComponent extends BasicPageComponent implements OnInit, OnDestroy {
    meds$: Observable<any[]>;

    constructor(public sb: AppSandbox) {
        super(sb);
    }

    addToPackage(selection: any[]) {

        this.sb.addToBasket(selection);
        this.sendClickEvent(this.page.AddToPackage.Action);
    }

    ngOnInit() {
        const _queries: IDictionary = {};
        _queries['PatientId'] = this.patient.UId;
        _queries['MedicationName'] = ''; // empty name will return ALL meds

        const searchRequest: SearchRequest = {
            Queries: _queries, PageSize: 0, PageNumber: 0
        };

        const asyncEvent: Event = {
            commandName: 'SEARCH_MEDICATIONS',
            data: JSON.stringify(searchRequest),
            type: '', ID: null
        };
        this.meds$ = this.sb.sendAsyncEvent(asyncEvent);
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }
}
