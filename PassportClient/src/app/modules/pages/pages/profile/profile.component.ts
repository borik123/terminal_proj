﻿import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { SearchRequest, IDictionary } from '../../../shared/models/search.model';
import { Event } from '../../../shared/models/event.model';
import { Medication } from '../../../shared/models/passportContext.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.template.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent extends BasicPageComponent {
    dataSource$: Observable<any>;

    headers: string[] = [
        'Image', 'QTY', 'Med', 'RO Number', 'Split', 'TQW', 'TQD', 'Start Date',
        'End Date', 'Last Packaged', 'Last Changed', 'Entered By', 'SIG Description'
    ];
    widths: number[] = [120, 90, 360, 120, 90, 90, 90, 150, 150, 150, 150, 90, 360];

    constructor(public sb: AppSandbox) {
        super(sb);
    }

    searchMeds(data: {query: string, residentId: string}): void {
        const _queries: IDictionary = {};
        _queries['PatientId'] = data.residentId;
        _queries['MedicationName'] = data.query;

        const searchRequest: SearchRequest = {
            Queries: _queries,
            PageSize: 0, PageNumber: 0
        };

        const event: Event = {
            commandName: 'SEARCH_PROFILE_MEDS',
            data: JSON.stringify(searchRequest),
            type: '', ID: null
        };

        this.dataSource$ = this.sb.sendAsyncEvent(event).pipe(
            map((result: Medication[]) => {
                const gridRows: (string | number)[][] = result.map(med => [
                    med.ImageSrc || 'img_404.png',
                    med.Quantity, med.MedicationName, med.RoNumber, '01',
                    med.TQW, med.TQD, '11/12/2018', '10/12/2018',
                    '09/12/2018', '08/12/2018', '01', '...description...'
                ]);

                return {
                    data: gridRows,
                    headers: this.headers,
                    widths: this.widths
                };
            })
        );
    }
}
