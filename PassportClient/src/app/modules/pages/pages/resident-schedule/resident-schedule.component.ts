﻿import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';
import { SearchRequest, IDictionary } from '../../../shared/models/search.model';
import { Event } from '../../../shared/models/event.model';

@Component({
  selector: 'app-resident-schedule',
  templateUrl: './resident-schedule.template.html',
  styleUrls: ['./resident-schedule.component.less']
})
export class ResidentScheduleComponent extends BasicPageComponent {
    dataSource$: Observable<any>;

    headers: string[] = [
        'Date', 'Admin Time', 'Med Name', 'QTY', 'RO Number', 'Admin By', 'Packaged Time', 'Packaged By',
        'Removed Time', 'Removed By', 'Returned Time', 'Returned Quantity', 'Returned By', 'Returned Reason'
    ];
    widths: number[] = [150, 190, 360, 90, 120, 190, 190, 190, 190, 190, 190, 140, 190, 200];

    constructor(public sb: AppSandbox) {
        super(sb);
    }

    searchMeds(data: {query: string, residentId: string}): void {
        const _queries: IDictionary = {};
        _queries['PatientId'] = data.residentId;
        _queries['MedicationName'] = data.query;

        const searchRequest: SearchRequest = {
            Queries: _queries,
            PageSize: 0, PageNumber: 0
        };

        const event: Event = {
            commandName: 'SEARCH_SCHEDULE',
            data: JSON.stringify(searchRequest),
            type: '', ID: null
        };

        this.dataSource$ = this.sb.sendAsyncEvent(event).pipe(
            map((result: any[]) => {
                const gridRows: any[][] = result.map(row => [
                    new Date(row.Date).toDateString(), new Date(row.AdminTime).toDateString(), row.MedicationName, row.Quantity,
                    row.RoNumber, row.AdminBy, new Date(row.PackagedTime).toDateString(), row.PackagedBy,
                    new Date(row.RemovedTime).toDateString(), row.RemovedBy, new Date(row.ReturnedTime).toDateString(),
                    row.ReturnedQuantity, row.ReturnedBy, row.ReturnedReason
                ]);

                return {
                    data: gridRows,
                    headers: this.headers,
                    widths: this.widths
                };
            })
        );
    }
}
