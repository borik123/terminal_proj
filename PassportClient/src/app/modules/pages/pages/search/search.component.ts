﻿import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { trigger, state, animate, style, transition } from '@angular/animations';
import { interval } from 'rxjs/observable/interval';
import { timer } from 'rxjs/observable/timer';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-search-page',
    templateUrl: './search.template.html',
    styleUrls: ['./search.component.less'],
    animations: [
        trigger('routerTransition', [
            state('void', style({ opacity: '1' })),
            state('*', style({ opacity: '1' })),
            transition(':enter', [ // void => *
                style({ opacity: '0.2' }),
                animate('0.7s ease-in-out', style({ opacity: '1' }))
            ]),
            transition(':leave', [ // * => void
                style({ opacity: '1' }),
                animate('0.7s ease-in-out', style({ opacity: '0.2' }))
            ])
        ])
    ]
})
export class SearchPageComponent extends BasicPageComponent implements OnDestroy {
    public searchForm: FormGroup;
    private localSubscription: Subscription = new Subscription();

    constructor(public sb: AppSandbox, private readonly _fb: FormBuilder) {
        super(sb);

        this.searchForm = this._fb.group({
            resident: ['', Validators.required]
        });

        this.searchForm.valueChanges
            .pipe(
                filter((term: any) => term.resident && term.resident.FullName.length > 0)
            )
            .subscribe(val => {
                this.sendClickEvent('GET_FULL_PATIENT', val.resident.UId);
            });

        this.localSubscription.add(
            this.sb.selectedPatient$.subscribe(patient => {
                this.sb.setPatientDetails(patient);

                // go next
                if (patient.Status === 'Active' || patient.Status === undefined || patient.Status === null || patient.Status === '') {
                    this.sendClickEvent('PRN'); // RESIDENT_SWITCH
                } else {
                    this.sendClickEvent('FOLIO');
                }
            })
        );

        // timer(2000).subscribe(x => this.sb.addNotification({
        //    Id: new Date().getTime(), Date: null, Text: 'You are here', Title: '', AutoClosed: true, Priority: 3, IsSystem: true
        // }));
    }

    public get resident(): FormControl {
        return this.searchForm.get('resident') as FormControl;
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
        super.ngOnDestroy();
    }
}
