import { TextElement, ButtonElement, MediaElement, ToggleButtonElement, InputElement } from '../models/contentElems.model';

export interface IResidentDetails {
    Icon: MediaElement;
    Discharged: TextElement;
    LoaStart: TextElement;
    LoaEnd: TextElement;
    ResidentId: TextElement;
    Room: TextElement;
    Sex: TextElement;
    Age: TextElement;
    Allergies: TextElement;
    LastDose: TextElement;
    LastName: TextElement;
    FirstName: TextElement;
    MedicalRecNo: TextElement;
    AdmitDate: TextElement;
    Station: TextElement;
    Bed: TextElement;
    Male: TextElement;
    Female: TextElement;
    ToggleDetails: ToggleButtonElement;
    Profile: ButtonElement;
    Schedule: ButtonElement;
    SearchField: InputElement;
}
