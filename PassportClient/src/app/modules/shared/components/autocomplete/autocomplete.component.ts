import {Component, Input, Output, OnInit, OnDestroy, forwardRef, TemplateRef, ViewChild, ElementRef, EventEmitter} from '@angular/core';
import {FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {debounceTime, switchMap, takeUntil, skip, map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';
import {BasicSearchComponent} from '../basic-search.component';
import {IDictionary} from '../../models/search.model';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.less'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => AutocompleteComponent), multi: true}
  ]
})
export class AutocompleteComponent extends BasicSearchComponent implements ControlValueAccessor, OnInit, OnDestroy {
  get itemTitle(): string {
    return this._itemTitle;
  }
  @Input()
  set itemTitle(value: string) {
    this._itemTitle = value || '';
  }
  private _itemTitle = '';

  get externalFilters(): IDictionary {
    return this._externalFilters;
  }
  @Input()
  set externalFilters(value: IDictionary) {
    this._externalFilters = value;
    this.filteredItems = this.filterStaticDataSourceInternal(value);
  }
  private _externalFilters: IDictionary = {};

  get dataSource(): string | any[] {
    return this._ds;
  }
  @Input()
  set dataSource(value: string | any[]) {
    this._ds = value;
    if (typeof this.dataSource !== 'string') {
      this.filteredItems = this.filterStaticDataSourceInternal({});
    }
  }
  private _ds: string | any[];

  @Input()
  public placeholder: string;
  @Input()
  public itemId = 'id';
  @Input()
  public selectedItem?: any = null;
  @Input()
  public itemTemplate: TemplateRef<any>;
  @Input()
  public formatTemplate: TemplateRef<any>;
  @Input()
  public iconSrc: string;
  @Input()
  public rightIconSrc: string;
  @Input()
  public searchPropName: string;
  /**
   * Indicates if control should show triangle to open dropdown box.
   **/
  @Input()
  public showDropdownButton?: boolean;
  @Input()
  public zoom = 'no-zoom';
  @Input()
  public pageSize = 0;
  @Input()
  public iconBg = 'transparent';
  @Input()
  public width = '50em';
  @Input()
  public dropdownElementsBeforeScroll = 7;

  @Output() arrowUpClick: EventEmitter<any> = new EventEmitter();

  @Output() arrowDownClick: EventEmitter<any> = new EventEmitter();

  @Output() arrowRightClick: EventEmitter<any> = new EventEmitter();

  @Output() arrowLeftClick: EventEmitter<any> = new EventEmitter();

  @ViewChild('container', {read: ElementRef}) componentContainer;

  @ViewChild('results', { read: ElementRef }) public searchResultElement: ElementRef;

  @ViewChild('dropdownItem', { read: ElementRef }) public dropDownItem: ElementRef;


  public filteredItems: any[];
  public inputControl: FormControl;
  public openDropdown = false;
  public selection = '';
  public activeElement = 0;

  private _hideDropdown = new Subject<boolean>();
  private _propagateChange: (newValue: any) => {};
  private _propagateTouched: any = () => { };
  private _inputSubscription: Subscription = new Subscription();
  private pageNumber = 0;
  private stopSearching: Subject<boolean> = new Subject<boolean>();
  private loading = true;
  public ngOnInit(): void {
    this.inputControl = new FormControl();
    this.showDropdownButton = this.showDropdownButton || false;

    if (typeof this.dataSource !== 'string') {
      this.filteredItems = this.filterStaticDataSourceInternal(this.externalFilters);
    }

    this._inputSubscription = this.inputControl.valueChanges
        .pipe(
            // skip(1), // skip first emission on page Load
            debounceTime(400), // skip all letters that are typed faster than 400ms
            map((term: string) => {
                const _queries: IDictionary = this.externalFilters;
                _queries[this.searchPropName] = term;
                _queries['searchQuery'] = term;
                return _queries; // convert search term to Search queries
            }),
            switchMap((queries: IDictionary) => {
              return typeof this.dataSource === 'string'
                ? this.search(this.dataSource as string, queries, this.pageSize, 0).pipe(takeUntil(this.stopSearching))
                : this.filterStaticDataSource(queries);
            })
        )
        .subscribe(coll => {
            this.setDefaultActiveElement();
            this.filteredItems = coll;
            this.pageNumber = coll.length < this.pageSize ? -1 : 0;
            this.loading = false;
            this.openDropdown = true;
        });

    this._hideDropdown.pipe(debounceTime(300))
      .subscribe(hide => {
        this.setDefaultActiveElement();
        this.openDropdown = !hide;
      });
  }

  filterStaticDataSource(queries: IDictionary): Observable<any[]> {
    return of(this.filterStaticDataSourceInternal(queries));
  }
  filterStaticDataSourceInternal(queries: IDictionary): any[] {
    if (this.dataSource === undefined || this.dataSource === null) {
      return [];
    }

    return (this.dataSource as any[]).filter(x => {
      for (const key in queries) {
        if (queries.hasOwnProperty(key)) {
          if (queries[key] === '') {
            continue;
          } else if (x.hasOwnProperty(key)) {
            const re = new RegExp(queries[key], 'gi');
            if (key === this.searchPropName && !re.test(x[key])) {
              return false;
            } else if (key !== this.searchPropName && x[key] !== queries[key]) {
              return false;
            }
          }
        }
      }
      return true;
    });
  }

  loadMore(ev: any): void {
      if (this.loading || this.pageSize === 0 || this.pageNumber === -1 ||
          ev.target.scrollHeight - ev.target.scrollTop > ev.target.clientHeight) {
          return;
      }
      this.loading = true;
      this.stopSearching.next(); // stop inner subscription from ngOnInit() method

      // build queries
      const _queries: IDictionary = {};
      _queries[this.searchPropName] = this.inputControl.value || '';

      this.search(this.dataSource as string, _queries, this.pageSize, this.pageNumber + 1).pipe(skip(1), takeUntil(this.stopSearching))
        .subscribe(list => {
          this.stopSearching.next();
          if (list.length < this.pageSize || list.length === 0) {
            this.pageNumber = -1;
          } else {
            this.pageNumber = this.pageNumber + 1;
          }
          this.filteredItems = [...this.filteredItems, ...list];
          this.loading = false;
        });
  }

  ngOnDestroy() {

      if (this._inputSubscription) {
          this._inputSubscription.unsubscribe();
      }
  }

  public writeValue(value: any): void {
    this.inputControl.setValue(this.getText(value), { emitEvent: false });
    // this.inputControl.setValue(value);
  }

  public registerOnChange(fn: any): void {
    this._propagateChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this._propagateTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.inputControl.disable();
    } else {
      this.inputControl.enable();
    }
  }

  public hideDropdown(): void {
    this._hideDropdown.next(true);
    this.stopSearching.next();
  }

  public focusDropdown(): void {
    if (typeof this.dataSource !== 'string') {
      this.openDropdown = true;
    }
  }

  public enterClick(): void {
    if (this.openDropdown && this.filteredItems && this.filteredItems.length > 0) {
      this.selectItem(this.filteredItems[this.activeElement]);
    }
  }

  public arrowUp(): void {
    this.arrowUpClick.emit();
    if (this.filteredItems && this.activeElement > 0) {
      this.activeElement --;
      this.moveScroll(true);
    }
  }

  public arrowDown(): void {
    this.arrowDownClick.emit();
    if (this.filteredItems && this.filteredItems.length > this.activeElement + 1) {
      this.activeElement ++;
      this.moveScroll(false);
    }
  }

  public arrowRight(): void {
    this.arrowRightClick.emit();
  }

  public arrowLeft(): void {
    this.arrowLeftClick.emit();
  }

  public selectItem(item: any): void {
    if (this._propagateChange && item !== this.selectedItem) {
        this.selectedItem = item;

        const _selection = this.getText(item);
        this.selection = _selection;
        this.inputControl.setValue(_selection, { emitEvent: false });
        this._propagateChange(item);
    }

    this.hideDropdown();
  }

  public clearSelection(): void {
    this.selectItem(null);
  }

  getText(value: any): string {
    return value !== null && value !== undefined && value !== ''
      ? value[this._itemTitle] as string
      : '';
  }
  trackById(index: number, itm: any): any {
      return itm[this.itemId];
  }

  private moveScroll(up: boolean) {
    if (this.filteredItems.length > this.dropdownElementsBeforeScroll
      && this.searchResultElement && this.searchResultElement.nativeElement) {
      const height = this.getDropDownItemHeight();
      if (up) {
        if (this.filteredItems.length - this.dropdownElementsBeforeScroll > this.activeElement) {
          this.searchResultElement.nativeElement.scrollTop -= height;
        }
      } else {
        if (this.dropdownElementsBeforeScroll <= this.activeElement) {
          this.searchResultElement.nativeElement.scrollTop += height;
        }
      }
    }
  }

  private setDefaultActiveElement() {
    this.activeElement = 0;
    if (this.searchResultElement && this.searchResultElement.nativeElement) {
      this.searchResultElement.nativeElement.scrollTo(0, 0);
    }
  }

  private getDropDownItemHeight() {
    if (this.dropDownItem && this.dropDownItem.nativeElement) {
      return this.dropDownItem.nativeElement.offsetHeight + 1.241;
    }
    return 0;
  }
}
