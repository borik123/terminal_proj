﻿import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppSandbox } from '../../../app.sandbox';
import { SearchRequest, IDictionary } from '../models/search.model';
import { Event } from '../models/event.model';

@Component({
    selector: 'app-search-component',
    template: ''
})
export class BasicSearchComponent {

    constructor(public sb: AppSandbox) { }

    search(cmdName: string, query: IDictionary, pageSize: number = 0, pageNumber: number = 0): Observable<any[]> {

        const searchRequest: SearchRequest = {
            Queries: query,
            PageSize: pageSize,
            PageNumber: pageNumber
        };

        const event: Event = {
            commandName: cmdName,
            data: JSON.stringify(searchRequest),
            type: '', ID: null
        };

        return this.sb.sendAsyncEvent(event);
    }
}
