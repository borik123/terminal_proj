﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET } from "@angular/router";
import { filter, distinctUntilChanged, map, tap, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { Breadcrumb } from '../../models/breadcrumb.model';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './breadcrumb.template.html',
    styleUrls: ['./breadcrumb.component.less']
})
export class BreadcrumbComponent implements OnInit {
    breadcrumbs$: Observable<Breadcrumb[]>;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router)
    { }

    ngOnInit() {

        this.breadcrumbs$ = this.router.events
            .pipe(
                startWith(new NavigationEnd(0, '', '')), // to show breadcrumb on a first page
                filter(event => event instanceof NavigationEnd),
                distinctUntilChanged(),
                map(event => this.buildBreadCrumb(this.activatedRoute.root))
            );
    }

    buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: Array<Breadcrumb> = []): Array<Breadcrumb> {

        if (route.routeConfig == null) {
            // we are on the root path
            return route.firstChild
                ? this.buildBreadCrumb(route.firstChild, url, breadcrumbs)
                : breadcrumbs;
        }
        
        const label = route.routeConfig.data ? route.routeConfig.data['breadcrumb'] : 'Page';
        const path = route.routeConfig.path;

        const nextUrl = `${url}${path}/`;
        const breadcrumb = {
            label: label,
            url: nextUrl
        };

        const newBreadcrumbs = [...breadcrumbs, breadcrumb];
        
        if (route.firstChild) {

            //If we are not on our current path yet,  there will be more children to look after, to build our breadcumb
            return route.routeConfig.data == undefined
                ? this.buildBreadCrumb(route.firstChild, url, breadcrumbs) // skip route
                : this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs); // go deeper
        }

        return newBreadcrumbs;
    }

}
