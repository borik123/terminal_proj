﻿import { Component, Input, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../../app.sandbox';
import { PassportContext } from '../../models/passportContext.model';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.template.html',
    styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnDestroy, OnInit {
    assetsFolderPath: string = this.sb.assetsFolderPath;
    @Input() isClosed: boolean;

    @Output() toggle = new EventEmitter();
    @Output() removeMedication = new EventEmitter();
    @Output() dispense = new EventEmitter();

    private localSubscription: Subscription = new Subscription();
    context: PassportContext;
    cartVM: any;

    constructor(private sb: AppSandbox) {

        this.localSubscription.add(
            this.sb.context$.subscribe(x => this.context = x)
        );
        this.localSubscription.add(
            this.sb.basketVM$.subscribe(vm => this.cartVM = vm)
        );
    }

    ngOnInit() {
        // ask Basket VM from server for Master layout
        this.sb.sendEvent('BASKET_VM');
    }

    get cartItems(): any[] {

        // group by cart items by Resident and State
        let groupedCart: any[] = [];
        this.context.cart.forEach((itm: any) => {
            const itmIndex = groupedCart.findIndex(el => el.Patient.UId === itm.Patient.UId && el.Status === itm.Status);
            if (itmIndex >= 0) {
                groupedCart[itmIndex].Items.push(itm);
                groupedCart[itmIndex].Total += itm.Items.length;
            } else {

                groupedCart.push({
                    Patient: itm.Patient,
                    Status: itm.Status,
                    Items: [itm],
                    Total: itm.Items.length
                });
            }
        });

        return groupedCart;
    }

    get medTotalCount(): number {

        return this.context.cart.reduce((total, val) => { return total + val.Items.length }, 0);
    }

    ngOnDestroy() {
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
    }
}
