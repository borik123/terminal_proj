﻿import {Component, Input} from '@angular/core';

@Component({
  selector: 'highlight',
  template: '<span>{{head}}</span><b style="color: #000">{{body}}</b><span>{{tail}}</span>'
})
export class HighlightComponent{
  get text(): any {
    return this._text;
  }

  @Input('text')
  set text(value: any) {
    const v = value || '';
    if (v === this._text) {
      return;
    }

    this._text = value;
    this.updateParts();
  }

  get selection(): string {
    return this._selection;
  }

  @Input('selection')
  set selection(value: string) {
    const v = value || '';
    if (this._selection === v) {
      return;
    }

    this._selection = v;
    this._selectionLowerCase = this._selection.toLowerCase();
    this.updateParts();
  }

  private _text: any;

  public head: string;
  public body: string;
  public tail: string;

  private _selection: string;

  private _selectionLowerCase: string;

  constructor() {
    this._text = '';
    this._selection = '';
    this.head = '';
    this.body = '';
    this.tail = '';
  }

  private updateParts(): void {
    const idx = this._text.toLowerCase().indexOf(this._selectionLowerCase);
    if (idx < 0) {
        this.head = this._text;
        this.body = '';
        this.tail = '';
        return;
    }

    this.head = idx > 0 ? this._text.substr(0, idx) : '';

    if (idx >= 0) {
      this.body = this._text.substr(idx, this._selection.length);
      this.tail = this._text.substr(idx + this._selection.length);
      return;
    }

    this.body = '';
    this.tail = '';
  }
}
