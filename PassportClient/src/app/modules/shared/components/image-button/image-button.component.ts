﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ButtonElement } from '../../models/contentElems.model';

@Component({
    selector: 'app-image-button',
    templateUrl: './image-button.template.html',
    styleUrls: ['./image-button.component.less']
})
export class ImageButtonComponent {
    @Input() btn: ButtonElement;
    @Input() assetsFolderPath: string;
    @Input() style: any = {};
    @Input() disabled: false;

    @Output() onClick = new EventEmitter();

    onClickInt(action: string): void {
        if (!this.disabled) {
            this.onClick.emit(action);
        }
    }
}
