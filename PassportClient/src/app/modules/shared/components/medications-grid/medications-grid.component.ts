import {Component, Input, Output, EventEmitter, ElementRef, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {trigger, state, style, transition, animate} from '@angular/animations';
import {debounceTime, distinctUntilChanged, switchMap, startWith, takeUntil, take, skip, map} from 'rxjs/operators';
import {PopoverConfig} from 'ngx-bootstrap/popover';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import {AppSandbox} from '../../../../app.sandbox';
import {Medication} from '../../models/passportContext.model';

export function getPopoverConfig(): PopoverConfig {
  return (<any>Object).assign(new PopoverConfig(), {
    placement: 'bottom',
    triggers: 'mouseenter:mouseleave'
  });
}

@Component({
  selector: 'app-medications-grid',
  templateUrl: './medications-grid.template.html',
  styleUrls: ['./medications-grid.component.less'],
  animations: [
    trigger('rowDetails', [
      state('true', style({
        height: '*'
      })),
      state('false', style({
        height: '82px'
      })),
      transition('true <=> false', animate('700ms ease-in-out'))
    ])
  ],
  providers: [{provide: PopoverConfig, useFactory: getPopoverConfig}]
})
export class MedicationsGridComponent {
  ds: Medication[] = [];
  medicationList: Medication[] = [];
  medsSearchForm: FormGroup;

  @Input() assetsFolderPath: string;
  @Input() medsVM: any;
  @Input() addToPackageBtn: any;
  @Input()
  set dataSource(val: Medication[]) {
      this.ds = val;
      this.medicationList = this.filterStaticDataSourceInternal(this.medsSearchForm.value.query || '');
      this.spinnerService.hide();
  }

  // handler
  @Output() addToPackage = new EventEmitter();

  constructor(public sb: AppSandbox, private readonly _fb: FormBuilder, private spinnerService: Ng4LoadingSpinnerService) {
    this.medsSearchForm = this._fb.group({
        query: ''
    });

    this.spinnerService.show();
    this.medsSearchForm.valueChanges
        .pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term => this.filterDataSource(term.query || ''))
        )
        .subscribe(list => {
            this.medicationList = list;
            this.spinnerService.hide();
        });
  }

  filterDataSource(term: string): Observable<Medication[]> {
    return of(this.filterStaticDataSourceInternal(term));
  }
  filterStaticDataSourceInternal(term: string): Medication[] {
    if (this.ds == null || this.ds === undefined) {
      return [];
    }

    const re = new RegExp(term, 'gi');
    return this.ds.filter(m => re.test(m.MedicationName));
  }

  clearSelectedMedications(type?: string): void {
    // TO-DO: add filter by Type
    this.selectedMedications.forEach(v => v.selected = false);
  }

  addToPackageInt() {
    const _selected = this.selectedMedications;
    if (_selected.length === 0) {
        return; // need select anything
    }

    this.addToPackage.emit(_selected);
  }

  scrollToPrn(): void {
  }

  getMedicationClassName(med: Medication): string {
      const now = new Date();
      let expirationStartDate = now;
      expirationStartDate.setTime(expirationStartDate.getTime() - (2 * 24 * 60 * 60 * 1000)); // minus 2 days

      if (med.SigTimeCount <= 0) {
          return 'gray';
      } else if (med.MedExpirationDate > expirationStartDate) {
          return 'red';
      } else if (med.DosageStartDate > now) {
          return 'grey';
      } else {
          return 'general-med-row';
      }
  }

  replaceImageMarkers(input: string): string {
      return input
          ? input.replace(/::{/g, '<img src="' + this.assetsFolderPath).replace(/}::/g, '" />')
          : '';
  }

  expandMedRow(obj: any): void {
    obj.itm.expanded = !obj.itm.expanded;
    obj.event.stopPropagation(); // expand, BUT do not select
  }
  selectMedRow(cell: any): void {
      if (cell.SigTimeCount > 0) {
          // save prev.value
          const prevSelectedStatus: boolean = cell.selected;

          // deselect all meds with the same MedID
          this.selectedMedications.filter(m => m.MedID === cell.MedID).forEach(m => m.selected = false);

          cell.selected = !prevSelectedStatus;
      }
  }

  get selectedMedications() {
    return this.medicationList.filter(m => m.selected);
  }
  get selectedMedicationsCount() {
      return this.selectedMedications.length;
  }
}
