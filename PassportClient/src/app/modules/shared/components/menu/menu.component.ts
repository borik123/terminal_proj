﻿import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.template.html',
    styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {
    menu: any[] = [];
    isOpened = false;

    @ViewChild('pop') pop: any;

    @Input() isClosed: boolean;
    @Input() menuItems: any;

    @Output() goToPage = new EventEmitter();

    ngOnInit() {

        // ask Menu items from server
        this.goToPage.emit('GET_TOP_MENU');
    }

    goTo(path: string) {
        // hide menu
        this.pop._popover.hide();
        // go to page
        this.goToPage.emit(path);
    }

    onShown() {
        this.isOpened = true;
    }
    onHidden() {
        this.isOpened = false;
    }
}
