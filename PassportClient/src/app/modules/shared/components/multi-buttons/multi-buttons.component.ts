﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ButtonElement } from '../../models/contentElems.model';

@Component({
    selector: 'app-multi-buttons',
    templateUrl: './multi-buttons.template.html',
    styleUrls: ['./multi-buttons.component.less']
})
export class MultiButtonsComponent {
  @Input() buttons: ButtonElement[] = [];
  @Input() assetsFolderPath = '';

  @Output() clickHandler = new EventEmitter();

  getAsset(assetName: string): string {
    return this.assetsFolderPath + assetName;
  }
}
