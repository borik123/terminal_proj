﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes} from '@angular/animations';

import { Notification } from '../../models/notification.model';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.template.html',
    styleUrls: ['./notifications.component.less'],
    animations: [
        trigger('newNotification', [
            state('yes', style({
                textShadow: '4px 5px 12px rgba(229, 244, 0, 1)'
            })),
            state('no', style({
                textShadow: 'none'
            })),
            transition('yes => no', animate('700ms ease-in-out')),
            transition('no => yes', animate('700ms ease-in-out'))
        ])
    ]
})
export class NotificationsComponent {
    tooltipVisible: boolean = false;
    notificationState: string = 'no';

    @Input() iconPath: string;
    @Input() removeIconPath: string;

    private _notifications: Notification[] = [];
    get notifications(): Notification[] {
        return this._notifications;
    }
    @Input() set notifications(value: Notification[]) {
        if (value.length > this._notifications.length) {

            this.notificationState = 'yes';

            setTimeout(this.clearNewNotificationStyle.bind(this), 2500);
        }

        this._notifications = value;
    }

    @Output() remove = new EventEmitter();

    constructor() { }

    toggleTooltip(): void {

        this.tooltipVisible = !this.tooltipVisible;
    }

    clearNewNotificationStyle() {
        
        this.notificationState = 'no';
    }

    removeInternal(obj: any): void {

        this.remove.emit(obj.item.Id);
        obj.event.stopPropagation();
    }

    trackById(index, notification) {

        return notification.Id;
    }
}
