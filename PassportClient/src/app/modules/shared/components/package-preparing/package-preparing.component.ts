﻿import { Component } from '@angular/core';

@Component({
    selector: 'app-package-preparing',
    templateUrl: './package-preparing.template.html',
    styleUrls: ['./package-preparing.component.less']
})
export class PackagePreparingComponent {
    prepareBtn: any;
    assetsFolderPath: string;
}
