﻿import { Component, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { BsModalService } from 'ngx-bootstrap/modal';

import { Notification } from '../../models/notification.model';
import { ExitModalComponent } from '../../pages/exit-modal/exit-modal.component';

import { AppSandbox } from '../../../../app.sandbox';
import { PassportContext } from '../../models/passportContext.model';

@Component({
    selector: 'app-page-header',
    templateUrl: './page-header.template.html',
    styleUrls: ['./page-header.component.less']
})
export class PageHeaderComponent implements OnDestroy, OnInit {
    notifications$: Observable<Notification[]> = this.sb.notifications$;
    menuItems$: Observable<any> = this.sb.topMenu$;
    assetsFolderPath: string = this.sb.assetsFolderPath;

    @Output() goHome = new EventEmitter();
    @Output() goToPage = new EventEmitter();
    @Output() refreshPage = new EventEmitter();
    @Output() exit = new EventEmitter();
    @Output() removeNotification = new EventEmitter();

    private modalSubscription: Subscription = new Subscription();
    private localSubscription: Subscription = new Subscription();
    context: PassportContext;
    header: any;

    constructor(private modalService: BsModalService, private sb: AppSandbox) {

        this.localSubscription.add(
            this.sb.context$.subscribe(x => this.context = x)
        );
        this.localSubscription.add(
            this.sb.headerVM$.subscribe(h => this.header = h)
        );
    }

    ngOnInit() {
        // ask Header VM from server for Master layout
        this.sb.sendEvent('HEADER_VM');
    }

    openExitModal(cmdName: string) {

        // show Modal dialog
        let dialogRef = this.modalService.show(ExitModalComponent, {
            initialState: {
                bound: this.header.LogoutDialog,
                assetsFolderPath: this.assetsFolderPath,
                medsInCart: this.context.cart.filter(c => c.PackageId == null)
                                .reduce((total, val) => { return total + val.Items.length }, 0)
            },
            class: 'exit-modal-dialog'
        });

        // subscribe to modal Close event
        this.modalSubscription = this.modalService.onHidden.subscribe(result => {
            if (dialogRef.content.needLogout) {
                this.exit.emit(cmdName);
            }
            this.modalSubscription.unsubscribe();
        });
    }

    ngOnDestroy() {
        if (this.modalSubscription) {
            this.modalSubscription.unsubscribe();
        }
        if (this.localSubscription) {
            this.localSubscription.unsubscribe();
        }
    }
}
