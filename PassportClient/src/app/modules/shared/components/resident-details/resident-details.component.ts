import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { debounceTime, distinctUntilChanged, startWith, map } from 'rxjs/operators';

import { Resident } from '../../../shared/models/passportContext.model';
import { IResidentDetails } from '../../../shared/VMs/IResidentDetails.model';

@Component({
  selector: 'app-resident-details',
  templateUrl: './resident-details.template.html',
  styleUrls: ['./resident-details.component.less'],
  animations: [
      trigger('details', [
          state('opened', style({
              height: '*'
          })),
          state('closed', style({
              height: '68px'
          })),
          transition('opened <=> closed', animate('700ms ease-in-out'))
      ])
  ]
})
export class ResidentDetailsComponent {
  searchForm: FormGroup;

  @Input() detailsState = 'closed';
  @Input() assetsFolderPath: string;
  @Input() currentResident: Resident;
  @Input() userDetails: IResidentDetails;
  @Input() searchMode = false;

  @Output() sendEvent = new EventEmitter();
  @Output() searchHandler = new EventEmitter();

  constructor(private readonly _fb: FormBuilder) {
    this.searchForm = this._fb.group({
        query: ['']
    });

    this.searchForm.valueChanges
        .pipe(
            startWith(''),
            debounceTime(500),
            distinctUntilChanged(),
            map((term: any) => term.query || '')
        )
        .subscribe(query => {
            this.searchHandler.emit(query);
        });
  }

  toggleDetailsState() {
    this.detailsState = this.detailsState === 'opened' ? 'closed' : 'opened';
  }

  public getAsset(assetName: string): string {
    return this.assetsFolderPath + assetName;
  }
}
