﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { timer } from 'rxjs/observable/timer';

import { AppSandbox } from '../../../../app.sandbox';
import { Notification } from '../../models/notification.model';

@Component({
    selector: 'app-system-notifications',
    templateUrl: './system-notifications.template.html',
    styleUrls: ['./system-notifications.component.less']
})
export class SystemNotificationsComponent {
    currentIndex = -1;
    lastShownMsgId: number = null;

    @Output() show = new EventEmitter();
    @Output() hide = new EventEmitter();

    private _notifications: Notification[] = [];
    get notifications(): Notification[] {
        return this._notifications;
    }
    @Input() set notifications(value: Notification[]) {
        if (value.length > this._notifications.length) {

            this._notifications = value;

            this.showInt();
        }
    }

    constructor(private sb: AppSandbox) {}

    closeNotification() {
        this.lastShownMsgId = this.notifications[this.currentIndex].Id;
        this.hide.emit();

        timer(1000).subscribe(() => this.showInt());
    }

    getIconSrc(): string {
        return this.sb.assetsFolderPath + 'MASSAGE_RED_ICON1.png';
    }

    private showInt(): void {

        if (this.currentIndex === -1 ||
            (this.notifications[this.currentIndex].Id === this.lastShownMsgId && this.currentIndex < this.notifications.length - 1)) {
            this.currentIndex++;
            this.show.emit();

            if (this.notifications[this.currentIndex].AutoClosed) {
                timer(4000).subscribe(() => {
                    this.lastShownMsgId = this.notifications[this.currentIndex].Id;
                    this.hide.emit();

                    timer(1000).subscribe(() => this.showInt());
                });
            }
        }
    }
}
