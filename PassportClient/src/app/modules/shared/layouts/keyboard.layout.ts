﻿import {keyboardIcons, IKeyboardLayouts, keyboardLayouts, KeyboardClassKey} from "@ngx-material-keyboard/core";

export const customIcons = (
  <any>Object).assign(
  {},
  keyboardIcons,
  {
      'Alt': 'language',
      'ArrowUp': 'arrow_drop_up',
      'ArrowDown': 'arrow_drop_down',
      'ArrowLeft': 'arrow_left',
      'ArrowRight': 'arrow_right'
  });

export const customLayouts: IKeyboardLayouts = (<any>Object).assign(
    {},
    keyboardLayouts,
    {
        'English': {
            'name': 'English',
            'keys': [
                [
                    ['1', '!'],
                    ['2', '@'],
                    ['3', '#'],
                    ['4', '$'],
                    ['5', '%'],
                    ['6', '^'],
                    ['7', '&'],
                    ['8', '*'],
                    ['9', '('],
                    ['0', ')'],
                    ['-', '_'],
                    ['=', '+'],
                    [KeyboardClassKey.Bksp, KeyboardClassKey.Bksp]
                ],
                [
                    [KeyboardClassKey.Tab, KeyboardClassKey.Tab],
                    ['q', 'Q'],
                    ['w', 'W'],
                    ['e', 'E'],
                    ['r', 'R'],
                    ['t', 'T'],
                    ['y', 'Y'],
                    ['u', 'U'],
                    ['i', 'I'],
                    ['o', 'O'],
                    ['p', 'P'],
                    ['[', '{'],
                    [']', '}'],
                    ['\\', '|']
                ],
                [
                    [KeyboardClassKey.Caps, KeyboardClassKey.Caps],
                    ['a', 'A'],
                    ['s', 'S'],
                    ['d', 'D'],
                    ['f', 'F'],
                    ['g', 'G'],
                    ['h', 'H'],
                    ['j', 'J'],
                    ['k', 'K'],
                    ['l', 'L'],
                    [';', ':'],
                    ['\'', '"'],
                    [KeyboardClassKey.Enter, KeyboardClassKey.Enter],
                ],
                [
                    [KeyboardClassKey.Shift, KeyboardClassKey.Shift],
                    ['z', 'Z'],
                    ['x', 'X'],
                    ['c', 'C'],
                    ['v', 'V'],
                    ['b', 'B'],
                    ['n', 'N'],
                    ['m', 'M'],
                    [',', ','],
                    ['.', '.'],
                    ['/', '?'],
                    ['ArrowUp', 'ArrowUp', 'ArrowUp', 'ArrowUp'],
                    [KeyboardClassKey.Alt, KeyboardClassKey.Alt]
                ],
                [
                    [KeyboardClassKey.Space, KeyboardClassKey.Space],
                    ['ArrowLeft', 'ArrowLeft'],
                    ['ArrowDown', 'ArrowDown'],
                    ['ArrowRight', 'ArrowRight']
                ]
            ],
            'lang': ['en-only']
        }
    }
);
