import { Component, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Subscription } from 'rxjs/Subscription';
import { timer } from 'rxjs/observable/timer';

import { AppSandbox } from '../../../app.sandbox';
import { PassportContext } from '../models/passportContext.model';

import { PackagePreparingComponent } from '../components/package-preparing/package-preparing.component';

@Component({
    selector: 'app-master-layout',
    templateUrl: './master.template.html',
    styleUrls: ['./master.component.less'],
    animations: [
        trigger('slideInOut', [
            state('in', style({
                transform: 'translateX(0)'
            })),
            state('out', style({
                transform: 'translateX(600px)'
            })),
            state('hidden', style({
                transform: 'translateX(850px)'
            })),
            transition('in => out', animate('700ms ease-in-out')),
            transition('out => in', animate(1400, keyframes([
                style({ transform: 'translateX(600px)', offset: 0 }),
                style({ transform: 'translateX(720px)', offset: 0.7 }),
                style({ transform: 'translateX(0)', offset: 1.0 })
            ]))),
            transition('in => hidden', animate('700ms ease-in-out')),
            transition('out => hidden', animate('700ms ease-in-out')),
            transition('hidden => in', animate('700ms ease-in-out')),
        ])
    ]
})
export class MasterLayoutComponent implements OnDestroy {
    page$: any = this.sb.page$;
    assetsFolderPath: string = this.sb.assetsFolderPath;
    previousPage: any;
    cartState = 'hidden';
    spinnerImg = '<img src="assets/loading-3.gif" />';

    dialogRef: BsModalRef = null;
    subscription: Subscription = new Subscription();
    context: PassportContext = null;

    constructor(public sb: AppSandbox, private modalService: BsModalService) {
        this.subscription.add(
            this.sb.previousPage$.subscribe(st => this.previousPage = st)
        );
        this.subscription.add(
            this.sb.context$.subscribe(st => this.contextChangedHandler(st))
        );
    }

    contextChangedHandler(newContext: PassportContext): void {
        this.context = newContext;

        // show/hide Cart
        if (newContext.cart.length === 0) {
            this.cartState = 'hidden';
        } else if (newContext.cart.length > 0 && this.cartState === 'hidden') {
            this.cartState = 'out';
        }
    }

    toggleCart() {
        this.cartState = this.cartState === 'out' ? 'in' : 'out';
    }

    goHome(cmdName: string): void {
        this.addEvent(cmdName);
    }
    refreshPage(): void {
        // NOW it's not refresh - it's GO BACK
        if (this.previousPage != null) {
            // GO TO previous Page
            this.addEvent(this.previousPage.EventId);
        }
    }
    exit(cmdName: string): void {
        this.sb.clearPassportContext();
        this.addEvent(cmdName);
    }
    removeNotification(notificationId: number): void {
        this.sb.deleteNotification(notificationId);
    }
    removeMedication(data: {obj: any, evId: string}): void {
        const {packageId, packageItemId} = data.obj;

        if (packageId != null) {

            // remove package (or package item) from server
            this.addEvent('UPDATE_PACKAGE', JSON.stringify({
                UserId: this.context.currentUser.UserID,
                TransactionID: packageId,
                PackageItemId: packageItemId
            }));

        } else {

            this.sb.removeFromBasket(data.obj);

            // go Home if no items in a Cart
            if (data.evId === 'DISPENSION' && this.context.cart.filter(c => c.PackageId == null).length === 0) {
                // go Home
                this.addEvent('SWITCH');

                if (this.cartState !== 'hidden') {
                    this.cartState = 'out';
                }
            }
        }
    }

    dispense(data: any) {
        const {cartVM, residentId, created} = data;

        this.dialogRef = this.modalService.show(PackagePreparingComponent, {
            initialState: {
                prepareBtn: cartVM.Prepare,
                assetsFolderPath: this.assetsFolderPath
            },
            ignoreBackdropClick: true,
            class: 'pill-loading-dialog'
        });

        // send data to server
        const dispenseData = {
            Packages: this.context.cart
                .filter(c => c.PackageId === null && c.Patient.UId === residentId && (!created || c.Created === created))
        };
        this.sb.sendEventToServer({ commandName: 'DISPENSE', type: 'onClick', ID: null, data: JSON.stringify(dispenseData) });

        // close cart
        this.cartState = 'out';

        // remove from Cart
        this.sb.removeFromBasket({ residentId: residentId, created: created });

        // TO-DO :: replace with interval from Server
        // close dialog and go next after SOME time
        timer(5000).subscribe(x => {

            // close popup dialog
            if (this.dialogRef != null) {
                this.dialogRef.hide();
                this.dialogRef = null;
            }

            // go next
            this.addEvent(cartVM.Dispense.Action);
        });

    }

    addEvent(cmdName: string, cmdData: string = null): void {
        this.sb.sendEventToServer({ commandName: cmdName, type: 'onClick', ID: null, data: cmdData });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.dialogRef != null) {
            this.dialogRef.hide();
            this.dialogRef = null;
        }
    }
}
