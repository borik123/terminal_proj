import { Component } from '@angular/core';

@Component({
  selector: 'app-simple-layout',
  template: `
      <div class="w-100">
        <router-outlet></router-outlet>
      </div>
    `,
  styleUrls: ['./simple.component.less']
})
export class SimpleLayoutComponent {

}
