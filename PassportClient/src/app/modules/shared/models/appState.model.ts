﻿import { Command } from './command.model';
import { Notification } from './notification.model';
import { PassportContext } from './passportContext.model';

export interface AppState {
    lastCommand: Command;
    navHistory: any[];
    notifications: Notification[];
    passportContext: PassportContext;
}
