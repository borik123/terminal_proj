﻿export enum MessageType {
    Default = 0,
    Connection = 1,
    ViewModel = 2,
    Peripheral = 3
}

export interface IBound {
    messageId: string;
    messageContent: string;
    messageDetails: string;
    messageType: MessageType;

    sendEvent(eventData: any): void;
}