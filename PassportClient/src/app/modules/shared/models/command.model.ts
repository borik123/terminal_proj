﻿import { MessageType } from './bound.model';

export interface Command {
    Id: string;
    Message: string;
    Details: string;
    Type: MessageType;
}
