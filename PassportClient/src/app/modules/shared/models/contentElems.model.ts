﻿export interface TextElement {
    Key: string;
    Value: string;
}

export interface MediaElement {
    Key: string;
    Value: string;
}

export interface ControlElement {
    Text: TextElement;
    Enabled?: boolean;
    Image?: MediaElement;
}

export interface InputElement extends ControlElement {
    Placeholder?: TextElement;
    ErrorText?: TextElement;
    ValidationPattern?: TextElement;
}

export interface ButtonElement extends ControlElement {
    Action: string;
    ActiveImage?: MediaElement;
}

export interface ToggleButtonElement extends ButtonElement {
    Checked?: boolean;
    UncheckedText?: TextElement;
    UncheckedImage?: MediaElement;
}
