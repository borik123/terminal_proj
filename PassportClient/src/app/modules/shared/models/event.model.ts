﻿export interface Event {
    commandName: string;
    type?: string;
    ID?: string;
    data?: any;
}
