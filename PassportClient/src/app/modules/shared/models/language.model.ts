﻿export enum LangDirection {
    Ltr = 0,
    Rtl = 1
}

export interface Language {
    LanguageID: number;
    Key: string;
    Direction: LangDirection;
}