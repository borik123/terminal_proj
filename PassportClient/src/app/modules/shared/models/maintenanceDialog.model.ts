﻿import {TextElement, ButtonElement, MediaElement} from './contentElems.model';

export interface MaintenanceDialog {
    Header: TextElement,
    Button: ButtonElement,
    Background: MediaElement
}
