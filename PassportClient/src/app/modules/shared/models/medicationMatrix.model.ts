﻿export enum Legend {
    SELECTED = 0,
    CARTRIDGE_FULL = 1,
    CARTRIDGE_FULL_LOCKED = 2,
    CONTAINER_ABOVE_CARTRIDGE_EMPTY = 3,
    CONTAINER_ABOVE_CARTRIDGE_FULL = 4,
    CONTAINER_BELOW_CARTRIDGE_EMPTY = 5,
    CONTAINER_BELOW_CARTRIDGE_FULL = 6,
    LOCATION_LOCKED = 7,
    CARTRIDGE_LOCKED = 8
}

export interface IMedication {
    MedID: string,
    NDC: string,
    GenericName: string,
    BrandName: string,
    ExpirationDate: Date,
    ImageSrc?: string
}

export interface IMedicationMatrix {
    Location: string,
    ContainerMedication: IMedication,
    ContainerExpirationDate: Date,
    ContainerFillDate: Date,
    ContainerQuantity: number,
    ContainerTransNum: number,
    ContainerLotNumber: string,
    CartridgeMedication: IMedication,
    CartridgeExpirationDate: Date,
    CartridgeFillDate: Date,
    CartridgeQuantity: number,
    CartridgeTransNum: number,
    CartridgeLotNumber: string,
    Legend: Legend
}
