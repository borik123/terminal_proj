export enum NotificationPriority {
    LOWEST = 0,
    LOW = 1,
    NORMAL = 2,
    HIGH = 3,
    HIGHEST = 4
}

export interface Notification {
    Id: number;
    Text: string;
    Title: string;
    Date: Date;
    IsSystem: boolean;
    AutoClosed: boolean;
    Priority: NotificationPriority;
}
