﻿export interface User {
    UserID: string;
    FullName: string;
}

export interface ResidentLoa {
    LoaStart?: Date;
    LoaEnd?: Date;
    LoaStartAdminTimeUId?: string;
    LoaEndAdminTimeUId?: string;
}

export interface Resident {
    UId: string;
    PID: string;
    FullName: string;
    FirstName?: string;
    LastName?: string;
    StationID?: string;
    StationName?: string;
    RoomID?: string;
    RoomName?: string;
    BedID?: string;
    BedName?: string;
    Sex: number;
    Age: number;
    Allergies: string[];
    LastDose: string;
    Status?: string;
    BirthDate: Date;
    Discharged?: Date;
    LOA?: ResidentLoa;
    AdmitDate?: Date;
    MedicalRecNo?: string;
    TransactionDetails?: any;
}

export interface PatientPackageItem {
    ItemId?: string;
    // ProfileId: string;
    SigId: string;
    SigTimeId?: string;
    MedicationId?: string;
    MedicationName: string;
    Quantity: number;
}

export interface Medication extends PatientPackageItem {
    PatientId: string;
    ImageSrc: string;
    SigCode: string;
    SigDescription: string;
    SigUId: string;
    SigTimeId: string;
    SigTimeCount: number;
    SeqIndex: number;
    Type: string;
    State?: string;
    RxNumber: number;
    RoNumber?: number;
    QuantityPerDose?: number;
    SplitNumber?: number;
    TQW?: number;
    TQD?: number;
    TQR?: number;
    DosageToday?: number;
    SigTimeAsString: string;
    MedExpirationDate?: Date;
    MedID: string;
    DosageStartDate?: Date;
    LastGiven?: number;
    AdminTimes?: number[];
    TransactionTimes?: Date[];
    selected?: boolean;
    expanded: boolean;
}

export interface CartElem {
    PackageId?: string;
    Patient: Resident;
    Status?: number;
    AuthorId: string;
    Items: PatientPackageItem[];
    Created?: number;
}

export interface PassportContext {
    currentUser: User;
    currentResident: Resident;
    cart: CartElem[];
}
