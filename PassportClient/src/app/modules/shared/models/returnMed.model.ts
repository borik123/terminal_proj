﻿import { Medication } from './passportContext.model';

export interface ReturnMedication extends Medication {
    AdminTimeName: string;
    ReturnReason: string;
    IsControlledMed?: boolean;
}
