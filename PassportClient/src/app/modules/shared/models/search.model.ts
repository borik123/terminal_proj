﻿export interface IDictionary {
    [index: string]: string;
}

export enum AuthenticationResult {
    Success = 1,
    InvalidUserName = 2,
    InvalidPassword = 3,
    InvalidFingerprint = 4
}

export interface SearchRequest {
    Queries: IDictionary;
    PageSize: number;
    PageNumber: number;
}

export interface SearchResult {
    Records: any[];
}
