﻿export interface IBed {
    UId: string;
    Name: string;
}

export interface IRoom {
    UId: string;
    Name: string;
    Beds: IBed[];
}

export interface IStation {
    UId: string;
    Name: string;
    Rooms: IRoom[];
}
