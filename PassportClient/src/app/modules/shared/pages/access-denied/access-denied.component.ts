﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-access-denied',
    templateUrl: './access-denied.template.html',
    styleUrls: ['./access-denied.component.less']
})
export class AccessDeniedComponent extends BasicPageComponent {
}
