﻿import { Component, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { AppSandbox } from '../../../app.sandbox';
import { PassportContext, Resident } from '../models/passportContext.model';
import { IEditorOptions, IEditorType } from '../../edit-mode/models/editorOptions.model';

@Component({
    selector: 'app-basic-page',
    template: ''
})
export class BasicPageComponent implements OnDestroy {
    page: any;
    patient: Resident;
    assetsFolderPath: string = this.sb.assetsFolderPath;
    context$: Observable<PassportContext> = this.sb.context$;

    private subscription: Subscription = new Subscription();

    constructor(public sb: AppSandbox) {
        this.subscription.add(
            this.sb.page$.subscribe(state => this.page = state)
        );
        this.subscription.add(
            this.sb.context$.subscribe(ctx => this.patient = ctx.currentResident)
        );
    }

    replaceImageMarkers(input: string): string {
        return input ?
            input.replace(/::{/g, '<img src="' + this.assetsFolderPath).replace(/}::/g, '" />')
            : '';
    }

    sendClickEvent(cmdName: string, data: any = null): void {
        this.sb.sendEvent(cmdName, data);
    }

    updateModel() {
        // send 'this.page' to Server to SAVE it
        const boundObj = (<any>window).bound;
        if (boundObj !== undefined && typeof boundObj.savePageVM === 'function') {

            boundObj.savePageVM(JSON.stringify(this.page));
        }
    }

    public getEditorProps(ePath: string, eType: IEditorType, ePos: string = 'rt'): IEditorOptions {
        return {
            ctrl: this.page,
            path: ePath,
            enabled: this.sb.isEditMode,
            type: eType,
            pos: ePos
        };
    }

    public getAsset(assetName: string): string {
        return this.assetsFolderPath + assetName;
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
