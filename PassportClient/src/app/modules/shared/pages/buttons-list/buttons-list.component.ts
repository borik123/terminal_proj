﻿import { Component } from '@angular/core';

import { BasicPageComponent } from '../../../shared/pages/basic-page';

@Component({
    selector: 'app-buttons-list',
    templateUrl: './buttons-list.template.html',
    styleUrls: ['./buttons-list.component.less']
})
export class ButtonsListComponent extends BasicPageComponent {

}
