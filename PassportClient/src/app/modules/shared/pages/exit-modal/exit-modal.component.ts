﻿import { Component } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
    selector: 'app-exit-modal',
    templateUrl: './exit-modal.template.html',
    styleUrls: ['./exit-modal.component.less']
})
export class ExitModalComponent {
    needLogout = false;
    bound: any;
    assetsFolderPath: string;
    medsInCart = 0;

    constructor(public bsModalRef: BsModalRef) { }

    get notificationText(): string {

        return this.bound.BasketNotification && this.medsInCart > 0
            ? this.bound.BasketNotification.Value.replace('[count]', this.medsInCart)
            : null;
    }

    yesCliked(): void {

        this.needLogout = true;
        this.closeModalDialog();
    }
    noClicked(): void {

        this.needLogout = false;
        this.closeModalDialog();
    }
    closeModalDialog(): void {

        // close modal dialog
        this.bsModalRef.hide();
        this.bsModalRef = null;
    }
}
