import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
/* MODULES */
import {MatButtonModule} from '@angular/material';
import {MatKeyboardModule} from '@ngx-material-keyboard/core';
import {MatInputModule} from '@angular/material/input';
import {Ng4LoadingSpinnerModule} from 'ng4-loading-spinner';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {InlineEditorModule} from '../edit-mode/edit-mode.module';
/* LAYOUTS */
import {SimpleLayoutComponent} from './layouts/simple.component';
import {MasterLayoutComponent} from './layouts/master.component';
import {keyboardLayouts, MAT_KEYBOARD_LAYOUTS, MAT_KEYBOARD_ICONS} from '@ngx-material-keyboard/core';
import {customIcons, customLayouts} from './layouts/keyboard.layout';
/* COMPONENTS */
import {BasicSearchComponent} from './components/basic-search.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {NotificationsComponent} from './components/notifications/notifications.component';
import {SystemNotificationsComponent} from './components/system-notifications/system-notifications.component';
import {CartComponent} from './components/cart/cart.component';
import {AutocompleteComponent} from './components/autocomplete/autocomplete.component';
import {HighlightComponent} from './components/highlight/highlight.component';
import {PackagePreparingComponent} from './components/package-preparing/package-preparing.component';
import {BreadcrumbComponent} from './components/breadcrumb/breadcrumb.component';
import {ImageButtonComponent} from './components/image-button/image-button.component';
import {MenuComponent} from './components/menu/menu.component';
import {ResidentDetailsComponent} from './components/resident-details/resident-details.component';
import {MultiButtonsComponent} from './components/multi-buttons/multi-buttons.component';
import {MedicationsGridComponent} from './components/medications-grid/medications-grid.component';
/* PAGES */
import {BasicPageComponent} from './pages/basic-page';
import {ExitModalComponent} from './pages/exit-modal/exit-modal.component';
import {ButtonsListComponent} from './pages/buttons-list/buttons-list.component';
import {AccessDeniedComponent} from './pages/access-denied/access-denied.component';

@NgModule({
  declarations: [
    BasicPageComponent,
    ButtonsListComponent,
    AccessDeniedComponent,
    ExitModalComponent,
    BasicSearchComponent,
    SimpleLayoutComponent,
    MasterLayoutComponent,
    PageHeaderComponent,
    NotificationsComponent,
    SystemNotificationsComponent,
    CartComponent,
    AutocompleteComponent,
    HighlightComponent,
    PackagePreparingComponent,
    BreadcrumbComponent,
    MenuComponent,
    ImageButtonComponent,
    ResidentDetailsComponent,
    MultiButtonsComponent,
    MedicationsGridComponent
  ],
  imports: [
    PopoverModule,
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    Ng4LoadingSpinnerModule,
    MatInputModule,
    MatButtonModule,
    MatKeyboardModule,
    FormsModule
  ],
  providers: [
    {provide: MAT_KEYBOARD_ICONS, useValue: customIcons},
    {provide: MAT_KEYBOARD_LAYOUTS, useValue: customLayouts}
  ],
  exports: [
      BasicSearchComponent,
      BasicPageComponent,
      ButtonsListComponent,
      AccessDeniedComponent,
      ExitModalComponent,
      SimpleLayoutComponent,
      MasterLayoutComponent,
      PageHeaderComponent,
      NotificationsComponent,
      SystemNotificationsComponent,
      CartComponent,
      AutocompleteComponent,
      HighlightComponent,
      PackagePreparingComponent,
      BreadcrumbComponent,
      MenuComponent,
      ImageButtonComponent,
      ResidentDetailsComponent,
      PopoverModule,
      CommonModule,
      RouterModule,
      ReactiveFormsModule,
      Ng4LoadingSpinnerModule,
      MatInputModule,
      MatButtonModule,
      MatKeyboardModule,
      FormsModule,
      InlineEditorModule,
      MultiButtonsComponent,
      MedicationsGridComponent
  ],
  entryComponents: [PackagePreparingComponent, ExitModalComponent]
})
export class SharedModule {
}
