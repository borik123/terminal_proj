﻿import { Command } from '../modules/shared/models/command.model';
import * as actions from '../actions/command.actions';

export function commandReducer(state: Command, action: any): Command {
    
    switch (action.type) {
        case actions.CommandActionTypes.RECEIVE_COMMAND:
            return action.payload;
        default:
            return state;
    }
}