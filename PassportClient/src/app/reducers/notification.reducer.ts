﻿import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { AppState } from '../modules/shared/models/appState.model';
import { Notification } from '../modules/shared/models/notification.model';
import * as actions from '../actions/notification.actions';

export function notificationReducer(state: Notification[] = [], action: any): Notification[] {
    switch (action.type) {
        case actions.NotificationActionTypes.ADD_NOTIFICATION:
            return [...state, action.payload];
        case actions.NotificationActionTypes.DELETE_NOTIFICATION:
            let removedNotificationIndex = (<any>state).findIndex(x => x.Id == action.payload);
            return [
                ...state.slice(0, removedNotificationIndex),
                ...state.slice(removedNotificationIndex + 1)];
        default:
            return state;
    }
}

//export const getClientState = createFeatureSelector<AppState>('PassportClient');

//export const getNotifications = createSelector(
//    getClientState,
//    (state: AppState) => state.notifications
//);
export const getNotifications = (state: AppState) => state.notifications;
export const getSystemNotifications = createSelector(
    getNotifications,
    notifications => {
        return notifications.filter(_ => _.IsSystem);
    }
);
