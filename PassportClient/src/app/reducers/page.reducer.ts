﻿import { createFeatureSelector, createSelector, MemoizedSelector, select } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { pipe } from 'rxjs';

import * as actions from '../actions/page.actions';
import { AppState } from '../modules/shared/models/appState.model';
import { Command } from '../modules/shared/models/command.model';
import { MessageType } from '../modules/shared/models/bound.model';

const EMPTY_PAGE = {
    EventId: 'index'
};

export function pageReducer(state: any[] = [EMPTY_PAGE], action: any): any[] {
    switch (action.type) {
        case actions.PageActionTypes.ADD_PAGE:
            if (state.length > 1 && action.payload.EventId === state[state.length - 2].EventId) {
                // it's go Back
                return [...state.slice(0, state.length - 2), action.payload];
            } else if (action.payload.EventId === 'INTRO') {
                // go to Intro
                return [action.payload];
            } else {
                // just add new Page
                return [...state, action.payload];
            }
        case actions.PageActionTypes.GO_BACK:
            return state.slice(0, state.length - 1);
        case actions.PageActionTypes.GO_HOME:
            return state.filter(x => x.EventId !== 'index').slice(0, 2);
        case actions.PageActionTypes.CLEAR_HISTORY:
            return state.filter(x => x.EventId !== 'index').slice(0, 1);
        default:
            return state;
    }
}

export const getLastCommand = (state: AppState) => state.lastCommand;
export const getLastSearchCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command.Type === MessageType.Default)
);
export const getLastMenuCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'GET_TOP_MENU')
);
export const getStationsCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'GET_STATIONS')
);
export const getDoctorsCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'GET_DOCTORS')
);
export const getStatMedsCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'SEARCH_STAT_MEDICATIONS')
);
export const getAdminTimesCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'GET_ADMIN_TIMES')
);
export const getLastHeaderVmCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'HEADER_VM')
);
export const getLastBasketVmCommand = pipe(
    select(getLastCommand),
    filter((command: Command) => command !== null && command !== undefined && command.Id === 'BASKET_VM')
);
export const getNavHistory = (state: AppState) => state.navHistory;
export const getLastPage = createSelector(
    getNavHistory,
    history => {
        return history.length ? history.slice(history.length - 1)[0] : null;
    }
);
export const getPreviousPage = createSelector(
    getNavHistory,
    history => {
        return history.length > 1 ? history.slice(history.length - 2, history.length - 1)[0] : null;
    }
);
