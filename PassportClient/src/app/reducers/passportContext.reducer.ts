﻿import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { AppState } from '../modules/shared/models/appState.model';
import { PassportContext, CartElem, PatientPackageItem } from '../modules/shared/models/passportContext.model';
import * as actions from '../actions/passportContext.actions';

const EMPTY_CONTEXT = {
    currentUser: undefined,
    currentResident: undefined,
    // currentResident: {
    //     'Status': '', 'PID': '0004', 'UId': '00000002',
    //     'FullName': 'Tallulah Hurst', 'Sex': 77, 'Age': 43, 'Allergies': [], 'BirthDate': new Date(1983, 12, 9),
    //     'LastDose': 'Ondasetron 4MG, 4 hours ago', 'StationID': '0001', 'StationName': 'Station 1',
    //     'RoomID': '01', 'RoomName': '101', 'BedID': '005', 'BedName': '11',
    //     'LOA': {'LoaStart': new Date(2018, 6, 17), 'LoaEnd': new Date(2018, 6, 18)}
    // },
    cart: []
};

export function passportContextReducer(state: PassportContext = EMPTY_CONTEXT, action: any): PassportContext {
    switch (action.type) {
        case actions.PassportContextActionTypes.LOGIN_USER:
            return (<any>Object).assign({}, state, { currentUser: action.payload });
        case actions.PassportContextActionTypes.LOGOUT_USER:
            return (<any>Object).assign({}, state, { currentUser: null });

        case actions.PassportContextActionTypes.SET_RESIDENT:
            return (<any>Object).assign({}, state, { currentResident: action.payload });
        case actions.PassportContextActionTypes.CLEAR_RESIDENT:
            return (<any>Object).assign({}, state, { currentResident: null });

        case actions.PassportContextActionTypes.ADD_TO_CART:
            return (<any>Object).assign({}, state, {
                cart: [...state.cart, {
                    Patient: state.currentResident,
                    Items: action.payload,
                    AuthorId: state.currentUser.UserID,
                    Created: new Date().getTime()
                }]
            });

        case actions.PassportContextActionTypes.ADD_EXISTING_TO_CART:
            return (<any>Object).assign(
                {},
                state,
                { cart: [...state.cart.filter(c => c.PackageId == null), ...action.payload] }
            );

        case actions.PassportContextActionTypes.REMOVE_FROM_CART:
            const {residentId, medsId, created} = action.payload; // destructuring
            const removedElementIndex = state.cart.findIndex((x: CartElem) =>
                x.Patient.UId === residentId && x.PackageId == null && (!created || x.Created === created));
            const _elem = state.cart[removedElementIndex];
            if (medsId === undefined || _elem.Items.length === 1) {
                // remove the whole package
                return (<any>Object).assign({}, state, {
                    cart: [
                        ...state.cart.slice(0, removedElementIndex),
                        ...state.cart.slice(removedElementIndex + 1)
                    ]
                });
            } else {
                // remove single MED from package
                const removedMedsIndex = _elem.Items.findIndex((x: PatientPackageItem) => x.MedicationId === medsId);
                return (<any>Object).assign({}, state, {
                    cart: [
                        ...state.cart.slice(0, removedElementIndex),
                        (<any>Object).assign({}, _elem, {
                            Items: [
                                ..._elem.Items.slice(0, removedMedsIndex),
                                ..._elem.Items.slice(removedMedsIndex + 1)
                            ]
                        }),
                        ...state.cart.slice(removedElementIndex + 1)]
                });
            }
        case actions.PassportContextActionTypes.CLEAR_CONTEXT:
            return EMPTY_CONTEXT;
        default:
            return state;
    }
}

export const getPassportContext = (state: AppState) => state.passportContext;
