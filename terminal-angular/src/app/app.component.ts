import { Component } from '@angular/core';
import { SharedService } from './_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';


  constructor(private _sharedService: SharedService) {
    _sharedService.changeEmitted$.subscribe(
      text => {
        console.log(text);
      });
  }
}
