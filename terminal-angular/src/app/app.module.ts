import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Ng2CompleterModule } from 'ng2-completer';
import { FormsModule } from '@angular/forms';
import { NgxSmartModalModule } from 'ngx-smart-modal';

import { ChangePasswordComponent } from './change-password';
import { MedRunStatusComponent } from './med-run-status';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { SharedService } from './_services';
import { AgGridModule } from 'ag-grid-angular';
@NgModule({
  declarations: [
    AppComponent,
    ChangePasswordComponent,
    MedRunStatusComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    routing,
    BrowserAnimationsModule,
    Ng2CompleterModule,
    FormsModule,
    NgxSmartModalModule.forRoot(),
    AgGridModule.withComponents([])
  ],
  providers: [
    SharedService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
