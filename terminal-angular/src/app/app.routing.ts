import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from './change-password';
import { MedRunStatusComponent } from './med-run-status';


const appRoutes: Routes = [
  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'med-run', component: MedRunStatusComponent },
  // otherwise redirect to home
  { path: '**', redirectTo: 'change-password' }
];

export const routing = RouterModule.forRoot(appRoutes);
