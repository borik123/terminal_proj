import {Component} from '@angular/core';
import {SharedService} from '../_services/';
import {PageInfo} from '../_models/';
import { CompleterService, CompleterData, CompleterItem } from 'ng2-completer';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password-component.html'
})
export class ChangePasswordComponent {

  code: string;
  errorMsg: boolean;
  private pageName = 'Change Password';

  protected searchStr: string;
  protected captain: string;
  protected dataService: CompleterData;
  protected searchData = [
    { color: 'red', value: '1' },
    { color: 'green', value: '2' },
    { color: 'blue', value: '3' },
    { color: 'cyan', value: '4' },
    { color: 'magenta', value: '5' },
    { color: 'yellow', value: '6' },
    { color: 'black', value: '7' }
  ];



  constructor(private completerService: CompleterService,
              private _sharedService: SharedService,
              public ngxSmartModalService: NgxSmartModalService) {
    const pageInfo = new PageInfo();
    pageInfo.Name = this.pageName;
    this._sharedService.emitChange(pageInfo);

    this.dataService = completerService.local(this.searchData, 'color', 'color');
  }

  protected onSelected(item: CompleterItem) {
    if (item) {
      // this.ngxSmartModalService.getModal('myModal').open();
    }
  }
  protected onResetClick() {
    this.ngxSmartModalService.getModal('myModal').open();
  }
  protected sendCode() {
    this.errorMsg = true;
    console.log(this.code);
  }
  protected onClose() {
    this.errorMsg = false;
    this.code = '';
  }
}
