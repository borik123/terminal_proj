import {Component} from '@angular/core';
import {SharedService} from '../_services/';
import {PageInfo} from '../_models/';

@Component({
  selector: 'app-med-run',
  templateUrl: './med-run-status-component.html'
})
export class MedRunStatusComponent {

  private pageName = 'Med Run Status';

  constructor(private _sharedService: SharedService) {
    const pageInfo = new PageInfo();
    pageInfo.Name = this.pageName;
    this._sharedService.emitChange(pageInfo);
  }

  columnDefs = [
    {headerName: 'Med Run ID', field: 'id' },
    {headerName: 'Scheduled Start Date', field: 'startDate' },
    {headerName: 'Packaging Status', field: 'pacagingStatus'},
    {headerName: 'Totes Needed', field: 'totesNeeded' },
    {headerName: 'Scheduled On', field: 'scheduledOn' },
    {headerName: 'Set By', field: 'setBy'},
    {headerName: 'Med Run Name', field: 'medRunName' },
    {headerName: 'Status', field: 'status' },
    {headerName: 'Scheduled On', field: 'scheduledOn1'},
    {headerName: 'Created Date Time', field: 'createdDateTime' },
    {headerName: 'Complete Date Time', field: 'completeDateTime' }
  ];

  rowData = [
    { id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },
    { id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },{ id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },{ id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },{ id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },{ id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },{ id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' },{ id: 'test', startDate: 'test', pacagingStatus: 'test', totesNeeded: 'test', scheduledOn: 'test',
      setBy: 'test', medRunName: 'test',  status: 'test', scheduledOn1: 'test', createdDateTime: 'test',
      completeDateTime: '100' }
  ];
}
